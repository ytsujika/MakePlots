//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 21 18:02:02 2023 by ROOT version 6.24/08
// from TTree B/B
// found on file: /home/ytsujika/AnalysisBphys/run/Rootfile/BPHY22-1.root
//////////////////////////////////////////////////////////

#ifndef BTreeAnalize_h
#define BTreeAnalize_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class BTreeAnalize {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Definition of histogram

  // Decay mode : B0 -> D* mu nu
  TH1F *h_Distance;
  TH1F *h_Raw_MET;
  TH1F *h_Rec_MET;
  TH2F *h_Rec_MET_xy;
  TH1F *h_MET_difference;
  TH2F *h_MET_difference_xy;
  TH1F *h_B_Delta_Mass;
  TH1F *h_B_Full_Mass;
  TH1F *h_Full_Mass;
  TH1F *h_B_Mass1;
  TH1F *h_B_pT1;
  TH1F *h_Nu_pT1;
  TH2F *h_B_EP1;
  TH1F *h_B_Mass2;
  TH1F *h_B_pT2;
  TH1F *h_Nu_pT2;
  TH2F *h_B_EP2;
  TH2F *h_B_Mass_2D;

  TH1F *h_n_Pair;
  TH1F *h_n_Pair_SF;
  TH1F *h_n_Pair_OF;

  TH1F *h_B_Mass_All;
  TH1F *h_B_Mass_S;
  TH1F *h_B_Mass_B;
  TH1F *h_B_Mass;
  TH1F *h_Delta_Mass_All;
  TH1F *h_Delta_Mass_S;
  TH1F *h_Delta_Mass_B;
  TH1F *h_Delta_Mass;
  TH2F *h_BM_vs_DM_All;
  TH2F *h_BM_vs_DM_S;
  TH2F *h_BM_vs_DM_B;
  TH2F *h_BM_vs_DM;
  TH1F *h_W_Mass_All;
  TH1F *h_W_Mass_S;
  TH1F *h_W_Mass_B;
  TH1F *h_W_Mass;
  TH1F *h_DeltaPhi_All;
  TH1F *h_DeltaPhi_S;
  TH1F *h_DeltaPhi_B;
  TH1F *h_DeltaPhi;

  TH1F *h_BDT_score_All;
  TH1F *h_BDT_score_S;
  TH1F *h_BDT_score_B;
  TH2F *h_BDT_score_2D_All;
  TH2F *h_BDT_score_2D_S;
  TH2F *h_BDT_score_2D_B;
  
  TH2F *h_Delta_Mass_2D_All;
  TH2F *h_Delta_Mass_2D_S;
  TH2F *h_Delta_Mass_2D_B;
  
  TH1F *h_Delta_Mass_SF_S;
  TH1F *h_Delta_Mass_SF_B;
  TH1F *h_Delta_Mass_SF;
  TH1F *h_Delta_Mass_OF_S;
  TH1F *h_Delta_Mass_OF_B;
  TH1F *h_Delta_Mass_OF;

  TH1F *h_Mass_Difference_SF;
  TH1F *h_Mass_Difference_OF;

  TH1F *h_Visible_Mass_SF;
  TH1F *h_Visible_Mass_SF_SS;
  TH1F *h_Visible_Mass_SF_OS;
  TH1F *h_Visible_Mass_OF;
  TH1F *h_Visible_Mass_OF_SS;
  TH1F *h_Visible_Mass_OF_OS;
  TH2F *h_Visible_Mass_Combination_SF;
  TH2F *h_Visible_Mass_Combination_SF_SS;
  TH2F *h_Visible_Mass_Combination_SF_OS;
  TH2F *h_Visible_Mass_Combination_OF;
  TH2F *h_Visible_Mass_Combination_OF_SS;
  TH2F *h_Visible_Mass_Combination_OF_OS;

  TH1F *h_2Mu_Mass_SF;
  TH1F *h_2Mu_Mass_SF_SS;
  TH1F *h_2Mu_Mass_SF_OS;
  TH1F *h_2Mu_Mass_OF;
  TH1F *h_2Mu_Mass_OF_SS;
  TH1F *h_2Mu_Mass_OF_OS;
  TH2F *h_2Mu_Mass_Combination_SF;
  TH2F *h_2Mu_Mass_Combination_SF_SS;
  TH2F *h_2Mu_Mass_Combination_SF_OS;
  TH2F *h_2Mu_Mass_Combination_OF;
  TH2F *h_2Mu_Mass_Combination_OF_SS;
  TH2F *h_2Mu_Mass_Combination_OF_OS;

  TH1F *h_Delta_Mass_Difference_SF;
  TH1F *h_Delta_Mass_Difference_SF_SS;
  TH1F *h_Delta_Mass_Difference_SF_OS;
  TH1F *h_Delta_Mass_Difference_OF;
  TH1F *h_Delta_Mass_Difference_OF_SS;
  TH1F *h_Delta_Mass_Difference_OF_OS;

  //Fitting function
  TF1 *Function_Delta_Mass_SF;
  TF1 *Function_Delta_Mass_SF_S;
  TF1 *Function_Delta_Mass_SF_B;
  TF1 *Function_Delta_Mass_OF;
  TF1 *Function_Delta_Mass_OF_S;
  TF1 *Function_Delta_Mass_OF_B;

  TF1 *Function_Mass_Difference_SF;
  TF1 *Function_Mass_Difference_SF_S;
  TF1 *Function_Mass_Difference_SF_B;
  TF1 *Function_Mass_Difference_OF;
  TF1 *Function_Mass_Difference_OF_S;
  TF1 *Function_Mass_Difference_OF_B;

  // Correlation in MC sample
  TH1F *h_Delta_Time_SF_S;
  TH1F *h_Delta_Time_SF_B;
  TH1F *h_Delta_Time_SF_B_Unscaled;
  TH1F *h_Delta_Time_SF_O;
  TH1F *h_Delta_Time_SF;
  TH1F *h_Delta_Time_OF_S;
  TH1F *h_Delta_Time_OF_B;
  TH1F *h_Delta_Time_OF_B_Unscaled;
  TH1F *h_Delta_Time_OF_O;
  TH1F *h_Delta_Time_OF;
  TH1F *h_Correlation;
  TH1F *h_Significance;

  // Declaration of leaf types
  Int_t           event_num;
  Int_t           jet_num;
  Bool_t          PassGRL;
  Int_t           n_Track;
  vector<float>   *Track_d0;
  vector<bool>    *isMatch;
  vector<bool>    *isMatch_Pass;
  vector<bool>    *isPassed;
  vector<bool>    *isPassedTight;
  Bool_t          TriggerPass;
  vector<bool>    *TriggerType;
  Int_t           n_B;
  Int_t           n_B_Truth;
  Int_t           n_Dstar1;
  Int_t           n_Dstar1_Pass;
  Int_t           n_Dstar1_Pass_Tight;
  Int_t           n_Dstar1_Match;
  Int_t           n_Dstar1_Match_Pass;
  Int_t           n_Dstar1_Truth;
  Int_t           n_Dstar1_Truth_HighPt;
  Int_t           n_Dstar2;
  Int_t           n_Dstar2_Pass;
  Int_t           n_Dstar2_Pass_Tight;
  Int_t           n_Dstar2_Match;
  Int_t           n_Dstar2_Match_Pass;
  Int_t           n_Dstar2_Truth;
  Int_t           n_Dstar2_Truth_HighPt;
  Int_t           n_Mu_Truth;
  Int_t           n_MET;
  Int_t           n_MET_Truth;
  vector<int>     *n_PV;
  vector<int>     *n_RPV;
  Float_t         MET_px;
  Float_t         MET_py;
  Float_t         MET_sum;
  Float_t         Rec_MET_px;
  Float_t         Rec_MET_py;
  Float_t         Rec_MET_sum;
  Float_t         Rec_MET_Mu_px;
  Float_t         Rec_MET_Mu_py;
  Float_t         Rec_MET_Mu_sum;
  Float_t         Track_MET_px;
  Float_t         Track_MET_py;
  Float_t         Track_MET_sum;
  Float_t         Truth_MET_px;
  Float_t         Truth_MET_py;
  Float_t         Truth_MET_sum;
  vector<int>     *DecayType;
  vector<float>   *BDT_score;
  vector<int>     *B_flavor;
  vector<float>   *B_nPart;
  vector<float>   *B_mass;
  vector<float>   *B_mass_err;
  vector<float>   *B_eta;
  vector<float>   *B_phi;
  vector<float>   *B_px;
  vector<float>   *B_py;
  vector<float>   *B_pz;
  vector<float>   *B_E;
  vector<float>   *B_pT;
  vector<float>   *B_chi2;
  vector<float>   *B_nDoF;
  vector<float>   *B_x;
  vector<float>   *B_y;
  vector<float>   *B_z;
  vector<float>   *Visible_mass;
  vector<float>   *Visible_pT;
  vector<float>   *Visible_eta;
  vector<float>   *Visible_phi;
  vector<float>   *PiD0_mass;
  vector<float>   *D0Pi_mass;
  vector<float>   *D0Pi_pT;
  vector<float>   *D0Pi_eta;
  vector<float>   *D0Pi_phi;
  vector<float>   *PV_x;
  vector<float>   *PV_y;
  vector<float>   *PV_z;
  vector<float>   *RPV_x;
  vector<float>   *RPV_y;
  vector<float>   *RPV_z;
  vector<float>   *LxyMaxSumPt2;
  vector<float>   *LxyErrMaxSumPt2;
  vector<float>   *A0MaxSumPt2;
  vector<float>   *A0ErrMaxSumPt2;
  vector<float>   *A0xyMaxSumPt2;
  vector<float>   *A0xyErrMaxSumPt2;
  vector<float>   *Z0MaxSumPt2;
  vector<float>   *Z0ErrMaxSumPt2;
  vector<float>   *TauInvMassPVMaxSumPt2;
  vector<float>   *TauErrInvMassPVMaxSumPt2;
  vector<float>   *TauConstMassPVMaxSumPt2;
  vector<float>   *TauErrConstMassPVMaxSumPt2;
  vector<int>     *n_MuPi;
  vector<float>   *MuPi_mass;
  vector<float>   *MuPi_pT;
  vector<float>   *MuPi_eta;
  vector<float>   *MuPi_phi;
  vector<float>   *MuPi_chi2;
  vector<float>   *MuPi_nDoF;
  vector<float>   *MuPiAft_mass;
  vector<float>   *MuPiLxyMaxSumPt2;
  vector<double>  *Pi_s_mass;
  vector<double>  *Pi_s_pT;
  vector<double>  *Pi_s_charge;
  vector<double>  *Pi_s_eta;
  vector<double>  *Pi_s_phi;
  vector<double>  *Pi_s_theta_star;
  vector<float>   *Pi_s_d0;
  vector<float>   *Pi_s_z0;
  vector<float>   *Pi_s_qOverP;
  vector<int>     *n_Mu;
  vector<int>     *Mu_quality;
  vector<bool>    *Mu_PassIso;
  vector<double>  *Mu_mass;
  vector<double>  *Mu_pT;
  vector<double>  *Mu_charge;
  vector<double>  *Mu_eta;
  vector<double>  *Mu_phi;
  vector<float>   *Mu_d0;
  vector<float>   *Mu_z0;
  vector<float>   *Mu_qOverP;
  vector<float>   *Mu_chi2;
  vector<float>   *Mu_nDoF;
  vector<float>   *Mu_Chi2_B;
  vector<float>   *Mu_nDoF_B;
  vector<float>   *D0_mass;
  vector<float>   *D0_mass_err;
  vector<float>   *D0_pT;
  vector<float>   *D0_pT_err;
  vector<float>   *D0_Lxy;
  vector<float>   *D0_Lxy_err;
  vector<float>   *D0_Tau;
  vector<float>   *D0_Tau_err;
  vector<float>   *D0_chi2;
  vector<float>   *D0_nDoF;
  vector<bool>    *D0_flag;
  vector<float>   *KPi_mass;
  vector<float>   *KPi_pT;
  vector<float>   *KPi_eta;
  vector<float>   *KPi_phi;
  vector<double>  *Pi1_mass;
  vector<double>  *Pi1_pT;
  vector<double>  *Pi1_charge;
  vector<double>  *Pi1_eta;
  vector<double>  *Pi1_phi;
  vector<double>  *Pi1_theta_star;
  vector<float>   *Pi1_d0;
  vector<float>   *Pi1_z0;
  vector<float>   *Pi1_qOverP;
  vector<double>  *Pi2_mass;
  vector<double>  *Pi2_pT;
  vector<double>  *Pi2_charge;
  vector<double>  *Pi2_eta;
  vector<double>  *Pi2_phi;
  vector<double>  *Pi2_theta_star;
  vector<float>   *Pi2_d0;
  vector<float>   *Pi2_z0;
  vector<float>   *Pi2_qOverP;
  vector<double>  *Pi3_mass;
  vector<double>  *Pi3_pT;
  vector<double>  *Pi3_charge;
  vector<double>  *Pi3_eta;
  vector<double>  *Pi3_phi;
  vector<double>  *Pi3_theta_star;
  vector<float>   *Pi3_d0;
  vector<float>   *Pi3_z0;
  vector<float>   *Pi3_qOverP;
  vector<double>  *K_mass;
  vector<double>  *K_pT;
  vector<double>  *K_charge;
  vector<double>  *K_eta;
  vector<double>  *K_phi;
  vector<double>  *K_theta_star;
  vector<float>   *K_d0;
  vector<float>   *K_z0;
  vector<float>   *K_qOverP;

  // List of branches
  TBranch        *b_event_num;   //!
  TBranch        *b_jet_num;   //!
  TBranch        *b_PassGRL;   //!
  TBranch        *b_n_Track;   //!
  TBranch        *b_Track_d0;   //!
  TBranch        *b_isMatch;   //!
  TBranch        *b_isMatch_Pass;   //!
  TBranch        *b_isPassed;   //!
  TBranch        *b_isPassedTight;   //!
  TBranch        *b_TriggerPass;   //!
  TBranch        *b_TriggerType;   //!
  TBranch        *b_n_B;   //!
  TBranch        *b_n_B_Truth;   //!
  TBranch        *b_n_Dstar1;   //!
  TBranch        *b_n_Dstar1_Pass;   //!
  TBranch        *b_n_Dstar1_Pass_Tight;   //!
  TBranch        *b_n_Dstar1_Match;   //!
  TBranch        *b_n_Dstar1_Match_Pass;   //!
  TBranch        *b_n_Dstar1_Truth;   //!
  TBranch        *b_n_Dstar1_Truth_HighPt;   //!
  TBranch        *b_n_Dstar2;   //!
  TBranch        *b_n_Dstar2_Pass;   //!
  TBranch        *b_n_Dstar2_Pass_Tight;   //!
  TBranch        *b_n_Dstar2_Match;   //!
  TBranch        *b_n_Dstar2_Match_Pass;   //!
  TBranch        *b_n_Dstar2_Truth;   //!
  TBranch        *b_n_Dstar2_Truth_HighPt;   //!
  TBranch        *b_n_Mu_Truth;   //!
  TBranch        *b_n_MET;   //!
  TBranch        *b_n_MET_Truth;   //!
  TBranch        *b_n_PV;   //!
  TBranch        *b_n_RPV;   //!
  TBranch        *b_MET_px;   //!
  TBranch        *b_MET_py;   //!
  TBranch        *b_MET_sum;   //!
  TBranch        *b_Rec_MET_px;   //!
  TBranch        *b_Rec_MET_py;   //!
  TBranch        *b_Rec_MET_sum;   //!
  TBranch        *b_Rec_MET_Mu_px;   //!
  TBranch        *b_Rec_MET_Mu_py;   //!
  TBranch        *b_Rec_MET_Mu_sum;   //!
  TBranch        *b_Track_MET_px;   //!
  TBranch        *b_Track_MET_py;   //!
  TBranch        *b_Track_MET_sum;   //!
  TBranch        *b_Truth_MET_px;   //!
  TBranch        *b_Truth_MET_py;   //!
  TBranch        *b_Truth_MET_sum;   //!
  TBranch        *b_DecayType;   //!
  TBranch        *b_BDT_score;   //!
  TBranch        *b_B_flavor;   //!
  TBranch        *b_B_nPart;   //!
  TBranch        *b_B_mass;   //!
  TBranch        *b_B_mass_err;   //!
  TBranch        *b_B_eta;   //!
  TBranch        *b_B_phi;   //!
  TBranch        *b_B_px;   //!
  TBranch        *b_B_py;   //!
  TBranch        *b_B_pz;   //!
  TBranch        *b_B_E;   //!
  TBranch        *b_B_pT;   //!
  TBranch        *b_B_chi2;   //!
  TBranch        *b_B_nDoF;   //!
  TBranch        *b_B_x;   //!
  TBranch        *b_B_y;   //!
  TBranch        *b_B_z;   //!
  TBranch        *b_Visible_mass;   //!
  TBranch        *b_Visible_pT;   //!
  TBranch        *b_Visible_eta;   //!
  TBranch        *b_Visible_phi;   //!
  TBranch        *b_PiD0_mass;   //!
  TBranch        *b_D0Pi_mass;   //!
  TBranch        *b_D0Pi_pT;   //!
  TBranch        *b_D0Pi_eta;   //!
  TBranch        *b_D0Pi_phi;   //!
  TBranch        *b_PV_x;   //!
  TBranch        *b_PV_y;   //!
  TBranch        *b_PV_z;   //!
  TBranch        *b_RPV_x;   //!
  TBranch        *b_RPV_y;   //!
  TBranch        *b_RPV_z;   //!
  TBranch        *b_LxyMaxSumPt2;   //!
  TBranch        *b_LxyErrMaxSumPt2;   //!
  TBranch        *b_A0MaxSumPt2;   //!
  TBranch        *b_A0ErrMaxSumPt2;   //!
  TBranch        *b_A0xyMaxSumPt2;   //!
  TBranch        *b_A0xyErrMaxSumPt2;   //!
  TBranch        *b_Z0MaxSumPt2;   //!
  TBranch        *b_Z0ErrMaxSumPt2;   //!
  TBranch        *b_TauInvMassPVMaxSumPt2;   //!
  TBranch        *b_TauErrInvMassPVMaxSumPt2;   //!
  TBranch        *b_TauConstMassPVMaxSumPt2;   //!
  TBranch        *b_TauErrConstMassPVMaxSumPt2;   //!
  TBranch        *b_n_MuPi;   //!
  TBranch        *b_MuPi_mass;   //!
  TBranch        *b_MuPi_pT;   //!
  TBranch        *b_MuPi_eta;   //!
  TBranch        *b_MuPi_phi;   //!
  TBranch        *b_MuPi_chi2;   //!
  TBranch        *b_MuPi_nDoF;   //!
  TBranch        *b_MuPiAft_mass;   //!
  TBranch        *b_MuPiLxyMaxSumPt2;   //!
  TBranch        *b_Pi_s_mass;   //!
  TBranch        *b_Pi_s_pT;   //!
  TBranch        *b_Pi_s_charge;   //!
  TBranch        *b_Pi_s_eta;   //!
  TBranch        *b_Pi_s_phi;   //!
  TBranch        *b_Pi_s_theta_star;   //!
  TBranch        *b_Pi_s_d0;   //!
  TBranch        *b_Pi_s_z0;   //!
  TBranch        *b_Pi_s_qOverP;   //!
  TBranch        *b_n_Mu;   //!
  TBranch        *b_Mu_quality;   //!
  TBranch        *b_Mu_PassIso;   //!
  TBranch        *b_Mu_mass;   //!
  TBranch        *b_Mu_pT;   //!
  TBranch        *b_Mu_charge;   //!
  TBranch        *b_Mu_eta;   //!
  TBranch        *b_Mu_phi;   //!
  TBranch        *b_Mu_d0;   //!
  TBranch        *b_Mu_z0;   //!
  TBranch        *b_Mu_qOverP;   //!
  TBranch        *b_Mu_chi2;   //!
  TBranch        *b_Mu_nDoF;   //!
  TBranch        *b_Mu_Chi2_B;   //!
  TBranch        *b_Mu_nDoF_B;   //!
  TBranch        *b_D0_mass;   //!
  TBranch        *b_D0_mass_err;   //!
  TBranch        *b_D0_pT;   //!
  TBranch        *b_D0_pT_err;   //!
  TBranch        *b_D0_Lxy;   //!
  TBranch        *b_D0_Lxy_err;   //!
  TBranch        *b_D0_Tau;   //!
  TBranch        *b_D0_Tau_err;   //!
  TBranch        *b_D0_chi2;   //!
  TBranch        *b_D0_nDoF;   //!
  TBranch        *b_D0_flag;   //!
  TBranch        *b_KPi_mass;   //!
  TBranch        *b_KPi_pT;   //!
  TBranch        *b_KPi_eta;   //!
  TBranch        *b_KPi_phi;   //!
  TBranch        *b_Pi1_mass;   //!
  TBranch        *b_Pi1_pT;   //!
  TBranch        *b_Pi1_charge;   //!
  TBranch        *b_Pi1_eta;   //!
  TBranch        *b_Pi1_phi;   //!
  TBranch        *b_Pi1_theta_star;   //!
  TBranch        *b_Pi1_d0;   //!
  TBranch        *b_Pi1_z0;   //!
  TBranch        *b_Pi1_qOverP;   //!
  TBranch        *b_Pi2_mass;   //!
  TBranch        *b_Pi2_pT;   //!
  TBranch        *b_Pi2_charge;   //!
  TBranch        *b_Pi2_eta;   //!
  TBranch        *b_Pi2_phi;   //!
  TBranch        *b_Pi2_theta_star;   //!
  TBranch        *b_Pi2_d0;   //!
  TBranch        *b_Pi2_z0;   //!
  TBranch        *b_Pi2_qOverP;   //!
  TBranch        *b_Pi3_mass;   //!
  TBranch        *b_Pi3_pT;   //!
  TBranch        *b_Pi3_charge;   //!
  TBranch        *b_Pi3_eta;   //!
  TBranch        *b_Pi3_phi;   //!
  TBranch        *b_Pi3_theta_star;   //!
  TBranch        *b_Pi3_d0;   //!
  TBranch        *b_Pi3_z0;   //!
  TBranch        *b_Pi3_qOverP;   //!
  TBranch        *b_K_mass;   //!
  TBranch        *b_K_pT;   //!
  TBranch        *b_K_charge;   //!
  TBranch        *b_K_eta;   //!
  TBranch        *b_K_phi;   //!
  TBranch        *b_K_theta_star;   //!
  TBranch        *b_K_d0;   //!
  TBranch        *b_K_z0;   //!
  TBranch        *b_K_qOverP;   //!

  BTreeAnalize(const int& isMC, const int& isAdd, const string& infname, const string& outfname, const float& width1, const float& width2, TTree *tree=0);
  virtual ~BTreeAnalize();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Def_histo(const float& width1, const float& width2);
  virtual void     Loop(const int& isMC, const int& isAdd, const string& outfname, const float& width1, const float& width2);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
  virtual Double_t FitFunction(double x, double par0, double par1, double par2);
};

#endif

#ifdef BTreeAnalize_cxx
BTreeAnalize::BTreeAnalize(const int& isMC, const int& isAdd, const string& infname, const string& outfname, const float& width1, const float& width2, TTree *tree) : fChain(0) 
{
  string infile(infname);
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(infile.c_str());
    if (!f || !f->IsOpen()) {
      f = new TFile(infile.c_str());
    }
    f->GetObject("B",tree);

  }
  Init(tree);
  Def_histo(width1, width2);
  Loop(isMC, isAdd, outfname, width1, width2);
}

BTreeAnalize::~BTreeAnalize()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t BTreeAnalize::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t BTreeAnalize::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void BTreeAnalize::Def_histo(const float& width1, const float& width2)
{
  Int_t Mass_bin = (Int_t)30./width1;
  Float_t Time_bin_end = 15.*width2+0.25;
  Float_t Significance_bin_start = 0.25;
  Float_t Significance_bin_end = Significance_bin_start + width2*7.;

  // Decay mode : B0 -> D* mu nu
  h_Distance=new TH1F("h_Distance", "", 40, -10, 10);
  h_Distance->SetXTitle("s^{2}");
  h_Distance->SetYTitle("Entries / 0.5");

  h_Raw_MET=new TH1F("h_Raw_MET", "", 100, 0, 100);
  h_Raw_MET->SetXTitle("E^{miss}_{T} [GeV]");
  h_Raw_MET->SetYTitle("Entries / 0.5 GeV");

  h_Rec_MET=new TH1F("h_Rec_MET", "", 100, 0, 100);
  h_Rec_MET->SetXTitle("E^{miss}_{T} [GeV]");
  h_Rec_MET->SetYTitle("Entries / 0.5 GeV");

  h_Rec_MET_xy=new TH2F("h_Rec_MET_xy", "", 100, -10, 10, 100, -10, 10);
  h_Rec_MET_xy->SetXTitle("p^{miss}_{x} [GeV]");
  h_Rec_MET_xy->SetYTitle("p^{miss}_{y} [GeV]");

  h_MET_difference=new TH1F("h_MET_difference", "", 100, -50, 50);
  h_MET_difference->SetXTitle("#DeltaE^{miss}_{T} [GeV]");
  h_MET_difference->SetYTitle("Entries / 0.5 GeV");

  h_MET_difference_xy=new TH2F("h_MET_difference_xy", "", 100, -10, 10, 100, -10, 10);
  h_MET_difference_xy->SetXTitle("#Deltap^{miss}_{x} [GeV]");
  h_MET_difference_xy->SetYTitle("#Deltap^{miss}_{y} [GeV]");

  h_B_Full_Mass=new TH1F("h_B_Full_Mass", "", 100, 0, 100);
  h_B_Full_Mass->SetXTitle("m_{B} [GeV]");
  h_B_Full_Mass->SetYTitle("Entries / 0.4 GeV");

  h_Full_Mass=new TH1F("h_Full_Mass", "", 100, 0, 100);
  h_Full_Mass->SetXTitle("m_{2B^{0}} [GeV]");
  h_Full_Mass->SetYTitle("Entries / 2 GeV");

  h_B_Mass1=new TH1F("h_B_Mass1", "", 50, 0, 20);
  h_B_Mass1->SetXTitle("m_{B} [GeV]");
  h_B_Mass1->SetYTitle("Entries / 0.4 GeV");

  h_B_pT1=new TH1F("h_B_pT1", "", 100, 0, 50);
  h_B_pT1->SetXTitle("p_{T} [GeV]");
  h_B_pT1->SetYTitle("Entries / 0.5 GeV");

  h_Nu_pT1=new TH1F("h_Nu_pT1", "", 100, 0, 50);
  h_Nu_pT1->SetXTitle("p_{T} [GeV]");
  h_Nu_pT1->SetYTitle("Entries / 0.5 GeV");

  h_B_EP1=new TH2F("h_B_EP1", "", 50, -4, 4, 50, -3, 3);
  h_B_EP1->SetXTitle("#phi_{B} / 0.16");
  h_B_EP1->SetYTitle("#eta_{B} / 0.12");

  h_B_Mass2=new TH1F("h_B_Mass2", "", 50, 0, 20);
  h_B_Mass2->SetXTitle("m_{B} [GeV]");
  h_B_Mass2->SetYTitle("Entries / 0.4 GeV");

  h_B_pT2=new TH1F("h_B_pT2", "", 100, 0, 50);
  h_B_pT2->SetXTitle("p_{T} [GeV]");
  h_B_pT2->SetYTitle("Entries / 0.5 GeV");

  h_Nu_pT2=new TH1F("h_Nu_pT2", "", 100, 0, 50);
  h_Nu_pT2->SetXTitle("p_{T} [GeV]");
  h_Nu_pT2->SetYTitle("Entries / 0.5 GeV");

  h_B_EP2=new TH2F("h_B_EP2", "", 50, -4, 4, 50, -3, 3);
  h_B_EP2->SetXTitle("#phi_{B} / 0.16");
  h_B_EP2->SetYTitle("#eta_{B} / 0.12");

  h_B_Mass_2D=new TH2F("h_B_Mass_2D", "", 100, 0, 20, 100, 0, 20);
  h_B_Mass_2D->SetXTitle("m_{B1} / 0.2 GeV");
  h_B_Mass_2D->SetYTitle("m_{B2} / 0.2 GeV");

  h_n_Pair=new TH1F("h_n_Pair", "", 10, 0, 10);
  h_n_Pair->SetXTitle("n_{B pair}");
  h_n_Pair->SetYTitle("Entries");

  h_n_Pair_SF=new TH1F("h_n_Pair_SF", "", 10, 0, 10);
  h_n_Pair_SF->SetXTitle("n_{B pair}");
  h_n_Pair_SF->SetYTitle("Entries");

  h_n_Pair_OF=new TH1F("h_n_Pair_OF", "", 10, 0, 10);
  h_n_Pair_OF->SetXTitle("n_{B pair}");
  h_n_Pair_OF->SetYTitle("Entries");

  h_B_Delta_Mass=new TH1F("h_B_Delta_Mass", "", 50, 0, 50);
  h_B_Delta_Mass->SetXTitle("#Deltam_{B} [GeV]");
  h_B_Delta_Mass->SetYTitle("Entries / 1 GeV");

  h_B_Mass_All=new TH1F("h_B_Mass_All", "", 50, 0, 20);
  h_B_Mass_All->SetXTitle("m_{B} [GeV]");
  h_B_Mass_All->SetYTitle("Entries / 0.4 GeV");

  h_B_Mass_S=new TH1F("h_B_Mass_S", "", 50, 0, 20);
  h_B_Mass_S->SetXTitle("m_{B} [GeV]");
  h_B_Mass_S->SetYTitle("Entries / 0.4 GeV");

  h_B_Mass_B=new TH1F("h_B_Mass_B", "", 50, 0, 20);
  h_B_Mass_B->SetXTitle("m_{B} [GeV]");
  h_B_Mass_B->SetYTitle("Entries / 0.4 GeV");

  h_B_Mass=new TH1F("h_B_Mass", "", 50, 0, 20);
  h_B_Mass->SetXTitle("m_{B} [GeV]");
  h_B_Mass->SetYTitle("Entries / 0.4 GeV");

  h_Delta_Mass_All=new TH1F("h_Delta_Mass_All", "", Mass_bin, 135, 165);
  h_Delta_Mass_All->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_All->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass_S=new TH1F("h_Delta_Mass_S", "", Mass_bin, 135, 165);
  h_Delta_Mass_S->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_S->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass_B=new TH1F("h_Delta_Mass_B", "", Mass_bin, 135, 165);
  h_Delta_Mass_B->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_B->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass=new TH1F("h_Delta_Mass", "", Mass_bin, 135, 165);
  h_Delta_Mass->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass->SetYTitle("Entries / 0.5 MeV");

  h_BM_vs_DM_All=new TH2F("h_BM_vs_DM_All", "", 100, 0, 20, Mass_bin, 135, 165);
  h_BM_vs_DM_All->SetXTitle("m_{B} [GeV]");
  h_BM_vs_DM_All->SetYTitle("m_{D*}-m_{D^{0}} [MeV]");

  h_BM_vs_DM_S=new TH2F("h_BM_vs_DM_S", "", 100, 0, 20, Mass_bin, 135, 165);
  h_BM_vs_DM_S->SetXTitle("m_{B} [GeV]");
  h_BM_vs_DM_S->SetYTitle("m_{D*}-m_{D^{0}} [MeV]");

  h_BM_vs_DM_B=new TH2F("h_BM_vs_DM_B", "", 100, 0, 20, Mass_bin, 135, 165);
  h_BM_vs_DM_B->SetXTitle("m_{B} [GeV]");
  h_BM_vs_DM_B->SetYTitle("m_{D*}-m_{D^{0}} [MeV]");

  h_BM_vs_DM=new TH2F("h_BM_vs_DM", "", 100, 0, 20, Mass_bin, 135, 165);
  h_BM_vs_DM->SetXTitle("m_{B} [GeV]");
  h_BM_vs_DM->SetYTitle("m_{D*}-m_{D^{0}} [MeV]");

  h_W_Mass_All=new TH1F("h_W_Mass_All", "", 100, 0, 20);
  h_W_Mass_All->SetXTitle("m_{W} [GeV]");
  h_W_Mass_All->SetYTitle("Entries / 0.4 GeV");

  h_W_Mass_S=new TH1F("h_W_Mass_S", "", 100, 0, 20);
  h_W_Mass_S->SetXTitle("m_{W} [GeV]");
  h_W_Mass_S->SetYTitle("Entries / 0.4 GeV");

  h_W_Mass_B=new TH1F("h_W_Mass_B", "", 100, 0, 20);
  h_W_Mass_B->SetXTitle("m_{W} [GeV]");
  h_W_Mass_B->SetYTitle("Entries / 0.4 GeV");

  h_W_Mass=new TH1F("h_W_Mass", "", 100, 0, 20);
  h_W_Mass->SetXTitle("m_{W} [GeV]");
  h_W_Mass->SetYTitle("Entries / 0.4 GeV");

  h_DeltaPhi_All=new TH1F("h_DeltaPhi_All", "", 64, -3.2, 3.2);
  h_DeltaPhi_All->SetXTitle("#Delta#phi(#mu#nu)");
  h_DeltaPhi_All->SetYTitle("Entries / 0.1");

  h_DeltaPhi_S=new TH1F("h_DeltaPhi_S", "", 64, -3.2, 3.2);
  h_DeltaPhi_S->SetXTitle("#Delta#phi(#mu#nu)");
  h_DeltaPhi_S->SetYTitle("Entries / 0.1");

  h_DeltaPhi_B=new TH1F("h_DeltaPhi_B", "", 64, -3.2, 3.2);
  h_DeltaPhi_B->SetXTitle("#Delta#phi(#mu#nu)");
  h_DeltaPhi_B->SetYTitle("Entries / 0.1");

  h_DeltaPhi=new TH1F("h_DeltaPhi", "", 64, -3.2, 3.2);
  h_DeltaPhi->SetXTitle("#Delta#phi(#mu#nu)");
  h_DeltaPhi->SetYTitle("Entries / 0.1");

  h_BDT_score_All=new TH1F("h_BDT_score_All", "", 80, -0.8, 0.8);
  h_BDT_score_All->SetXTitle("BDT score");
  h_BDT_score_All->SetYTitle("Entries / 0.2");

  h_BDT_score_S=new TH1F("h_BDT_score_S", "", 80, -0.8, 0.8);
  h_BDT_score_S->SetXTitle("BDT score");
  h_BDT_score_S->SetYTitle("Entries / 0.2");

  h_BDT_score_B=new TH1F("h_BDT_score_B", "", 80, -0.8, 0.8);
  h_BDT_score_B->SetXTitle("BDT score");
  h_BDT_score_B->SetYTitle("Entries / 0.2");

  h_BDT_score_2D_All=new TH2F("h_BDT_score_2D_All", "", 80, -0.8, 0.8, 80, -0.8, 0.8);
  h_BDT_score_2D_All->SetXTitle("BDT score1");
  h_BDT_score_2D_All->SetYTitle("BDT score2");

  h_BDT_score_2D_S=new TH2F("h_BDT_score_2D_S", "", 80, -0.8, 0.8, 80, -0.8, 0.8);
  h_BDT_score_2D_S->SetXTitle("BDT score1");
  h_BDT_score_2D_S->SetYTitle("BDT score2");

  h_BDT_score_2D_B=new TH2F("h_BDT_score_2D_B", "", 80, -0.8, 0.8, 80, -0.8, 0.8);
  h_BDT_score_2D_B->SetXTitle("BDT score1");
  h_BDT_score_2D_B->SetYTitle("BDT score2");

  h_Delta_Mass_2D_All=new TH2F("h_Delta_Mass_2D_All", "", Mass_bin, 135, 165, Mass_bin, 135, 165);
  h_Delta_Mass_2D_All->SetXTitle("#Deltam_{1} [MeV]");
  h_Delta_Mass_2D_All->SetYTitle("#Deltam_{2} [MeV]");

  h_Delta_Mass_2D_S=new TH2F("h_Delta_Mass_2D_S", "", Mass_bin, 135, 165, Mass_bin, 135, 165);
  h_Delta_Mass_2D_S->SetXTitle("#Deltam_{1} [MeV]");
  h_Delta_Mass_2D_S->SetYTitle("#Deltam_{2} [MeV]");

  h_Delta_Mass_2D_B=new TH2F("h_Delta_Mass_2D_B", "", Mass_bin, 135, 165, Mass_bin, 135, 165);
  h_Delta_Mass_2D_B->SetXTitle("#Deltam_{1} [MeV]");
  h_Delta_Mass_2D_B->SetYTitle("#Deltam_{2} [MeV]");

  h_Delta_Mass_SF_S=new TH1F("h_Delta_Mass_SF_S", "", Mass_bin, 135, 165);
  h_Delta_Mass_SF_S->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_SF_S->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass_SF_B=new TH1F("h_Delta_Mass_SF_B", "", Mass_bin, 135, 165);
  h_Delta_Mass_SF_B->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_SF_B->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass_SF=new TH1F("h_Delta_Mass_SF", "", Mass_bin, 135, 165);
  h_Delta_Mass_SF->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_SF->SetYTitle("Entries / 0.5 MeV");

  h_Mass_Difference_SF=new TH1F("h_Mass_Difference_SF", "", Mass_bin, 135, 165);
  h_Mass_Difference_SF->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Mass_Difference_SF->SetYTitle("Entries / 0.5 MeV");

  h_Visible_Mass_SF=new TH1F("h_Visible_Mass_SF", "", 50, 0, 20);
  h_Visible_Mass_SF->SetXTitle("m_{B_{1}} + m_{B_{2}} [GeV]");
  h_Visible_Mass_SF->SetYTitle("Entries / 0.4 GeV");

  h_Visible_Mass_SF_SS=new TH1F("h_Visible_Mass_SF_SS", "", 50, 0, 20);
  h_Visible_Mass_SF_SS->SetXTitle("m_{B_{1}} + m_{B_{2}} [GeV]");
  h_Visible_Mass_SF_SS->SetYTitle("Entries / 0.4 GeV");

  h_Visible_Mass_SF_OS=new TH1F("h_Visible_Mass_SF_OS", "", 50, 0, 20);
  h_Visible_Mass_SF_OS->SetXTitle("m_{B_{1}} + m_{B_{2}} [GeV]");
  h_Visible_Mass_SF_OS->SetYTitle("Entries / 0.4 GeV");

  h_Visible_Mass_Combination_SF=new TH2F("h_Visible_Mass_Combination_SF", "", 50, 0, 10, 50, 0, 10);
  h_Visible_Mass_Combination_SF->SetXTitle("m_{B_{1}} [GeV]");
  h_Visible_Mass_Combination_SF->SetYTitle("m_{B_{2}} [GeV]");

  h_Visible_Mass_Combination_SF_SS=new TH2F("h_Visible_Mass_Combination_SF_SS", "", 50, 0, 10, 50, 0, 10);
  h_Visible_Mass_Combination_SF_SS->SetXTitle("m_{B_{1}} [GeV]");
  h_Visible_Mass_Combination_SF_SS->SetYTitle("m_{B_{2}} [GeV]");

  h_Visible_Mass_Combination_SF_OS=new TH2F("h_Visible_Mass_Combination_SF_OS", "", 50, 0, 10, 50, 0, 10);
  h_Visible_Mass_Combination_SF_OS->SetXTitle("m_{B_{1}} [GeV]");
  h_Visible_Mass_Combination_SF_OS->SetYTitle("m_{B_{2}} [GeV]");

  h_2Mu_Mass_SF=new TH1F("h_2Mu_Mass_SF", "", 50, 0, 10);
  h_2Mu_Mass_SF->SetXTitle("m_{#mu_{1}} + m_{#mu_{2}} [GeV]");
  h_2Mu_Mass_SF->SetYTitle("Entries / 0.2 GeV");

  h_2Mu_Mass_SF_SS=new TH1F("h_2Mu_Mass_SF_SS", "", 50, 0, 10);
  h_2Mu_Mass_SF_SS->SetXTitle("m_{#mu_{1}} + m_{#mu_{2}} [GeV]");
  h_2Mu_Mass_SF_SS->SetYTitle("Entries / 0.2 GeV");

  h_2Mu_Mass_SF_OS=new TH1F("h_2Mu_Mass_SF_OS", "", 50, 0, 10);
  h_2Mu_Mass_SF_OS->SetXTitle("m_{#mu_{1}} + m_{#mu_{2}} [GeV]");
  h_2Mu_Mass_SF_OS->SetYTitle("Entries / 0.2 GeV");

  h_2Mu_Mass_Combination_SF=new TH2F("h_2Mu_Mass_Combination_SF", "", 50, 0, 10, 50, 0, 10);
  h_2Mu_Mass_Combination_SF->SetXTitle("m_{#mu_{1}} [GeV]");
  h_2Mu_Mass_Combination_SF->SetYTitle("m_{#mu_{2}} [GeV]");

  h_2Mu_Mass_Combination_SF_SS=new TH2F("h_2Mu_Mass_Combination_SF_SS", "", 50, 0, 10, 50, 0, 10);
  h_2Mu_Mass_Combination_SF_SS->SetXTitle("m_{#mu_{1}} [GeV]");
  h_2Mu_Mass_Combination_SF_SS->SetYTitle("m_{#mu_{2}} [GeV]");

  h_2Mu_Mass_Combination_SF_OS=new TH2F("h_2Mu_Mass_Combination_SF_OS", "", 50, 0, 10, 50, 0, 10);
  h_2Mu_Mass_Combination_SF_OS->SetXTitle("m_{#mu_{1}} [GeV]");
  h_2Mu_Mass_Combination_SF_OS->SetYTitle("m_{#mu_{2}} [GeV]");

  h_Delta_Mass_OF_S=new TH1F("h_Delta_Mass_OF_S", "", Mass_bin, 135, 165);
  h_Delta_Mass_OF_S->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_OF_S->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass_OF_B=new TH1F("h_Delta_Mass_OF_B", "", Mass_bin, 135, 165);
  h_Delta_Mass_OF_B->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_OF_B->SetYTitle("Entries / 0.5 MeV");

  h_Delta_Mass_OF=new TH1F("h_Delta_Mass_OF", "", Mass_bin, 135, 165);
  h_Delta_Mass_OF->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Delta_Mass_OF->SetYTitle("Entries / 0.5 MeV");

  h_Mass_Difference_OF=new TH1F("h_Mass_Difference_OF", "", Mass_bin, 135, 165);
  h_Mass_Difference_OF->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
  h_Mass_Difference_OF->SetYTitle("Entries / 0.5 MeV");

  h_Visible_Mass_OF=new TH1F("h_Visible_Mass_OF", "", 50, 0, 20);
  h_Visible_Mass_OF->SetXTitle("m_{B_{1}} + m_{B_{2}} [GeV]");
  h_Visible_Mass_OF->SetYTitle("Entries / 0.4 GeV");

  h_Visible_Mass_OF_SS=new TH1F("h_Visible_Mass_OF_SS", "", 50, 0, 20);
  h_Visible_Mass_OF_SS->SetXTitle("m_{B_{1}} + m_{B_{2}} [GeV]");
  h_Visible_Mass_OF_SS->SetYTitle("Entries / 0.4 GeV");

  h_Visible_Mass_OF_OS=new TH1F("h_Visible_Mass_OF_OS", "", 50, 0, 20);
  h_Visible_Mass_OF_OS->SetXTitle("m_{B_{1}} + m_{B_{2}} [GeV]");
  h_Visible_Mass_OF_OS->SetYTitle("Entries / 0.4 GeV");

  h_Visible_Mass_Combination_OF=new TH2F("h_Visible_Mass_Combination_OF", "", 50, 0, 10, 50, 0, 10);
  h_Visible_Mass_Combination_OF->SetXTitle("m_{B_{1}} [GeV]");
  h_Visible_Mass_Combination_OF->SetYTitle("m_{B_{2}} [GeV]");

  h_Visible_Mass_Combination_OF_SS=new TH2F("h_Visible_Mass_Combination_OF_SS", "", 50, 0, 10, 50, 0, 10);
  h_Visible_Mass_Combination_OF_SS->SetXTitle("m_{B_{1}} [GeV]");
  h_Visible_Mass_Combination_OF_SS->SetYTitle("m_{B_{2}} [GeV]");

  h_Visible_Mass_Combination_OF_OS=new TH2F("h_Visible_Mass_Combination_OF_OS", "", 50, 0, 10, 50, 0, 10);
  h_Visible_Mass_Combination_OF_OS->SetXTitle("m_{B_{1}} [GeV]");
  h_Visible_Mass_Combination_OF_OS->SetYTitle("m_{B_{2}} [GeV]");

  h_2Mu_Mass_OF=new TH1F("h_2Mu_Mass_OF", "", 50, 0, 10);
  h_2Mu_Mass_OF->SetXTitle("m_{#mu_{1}} + m_{#mu_{2}} [GeV]");
  h_2Mu_Mass_OF->SetYTitle("Entries / 0.2 GeV");

  h_2Mu_Mass_OF_SS=new TH1F("h_2Mu_Mass_OF_SS", "", 50, 0, 10);
  h_2Mu_Mass_OF_SS->SetXTitle("m_{#mu_{1}} + m_{#mu_{2}} [GeV]");
  h_2Mu_Mass_OF_SS->SetYTitle("Entries / 0.2 GeV");

  h_2Mu_Mass_OF_OS=new TH1F("h_2Mu_Mass_OF_OS", "", 50, 0, 10);
  h_2Mu_Mass_OF_OS->SetXTitle("m_{#mu_{1}} + m_{#mu_{2}} [GeV]");
  h_2Mu_Mass_OF_OS->SetYTitle("Entries / 0.2 GeV");

  h_2Mu_Mass_Combination_OF=new TH2F("h_2Mu_Mass_Combination_OF", "", 50, 0, 10, 50, 0, 10);
  h_2Mu_Mass_Combination_OF->SetXTitle("m_{#mu_{1}} [GeV]");
  h_2Mu_Mass_Combination_OF->SetYTitle("m_{#mu_{2}} [GeV]");

  h_2Mu_Mass_Combination_OF_SS=new TH2F("h_2Mu_Mass_Combination_OF_SS", "", 50, 0, 10, 50, 0, 10);
  h_2Mu_Mass_Combination_OF_SS->SetXTitle("m_{#mu_{1}} [GeV]");
  h_2Mu_Mass_Combination_OF_SS->SetYTitle("m_{#mu_{2}} [GeV]");

  h_2Mu_Mass_Combination_OF_OS=new TH2F("h_2Mu_Mass_Combination_OF_OS", "", 50, 0, 10, 50, 0, 10);
  h_2Mu_Mass_Combination_OF_OS->SetXTitle("m_{#mu_{1}} [GeV]");
  h_2Mu_Mass_Combination_OF_OS->SetYTitle("m_{#mu_{2}} [GeV]");

  h_Delta_Mass_Difference_SF=new TH1F("h_Delta_Mass_Difference_SF", "", 30, 0, 30);
  h_Delta_Mass_Difference_SF->SetXTitle("|#Deltam_{B_{1}} - #Deltam_{B_{2}}| [MeV]");
  h_Delta_Mass_Difference_SF->SetYTitle("Entries / 1 MeV");

  h_Delta_Mass_Difference_SF_SS=new TH1F("h_Delta_Mass_Difference_SF_SS", "", 30, 0, 30);
  h_Delta_Mass_Difference_SF_SS->SetXTitle("|#Deltam_{B_{1}} - #Deltam_{B_{2}}| [MeV]");
  h_Delta_Mass_Difference_SF_SS->SetYTitle("Entries / 1 MeV");

  h_Delta_Mass_Difference_SF_OS=new TH1F("h_Delta_Mass_Difference_SF_OS", "", 30, 0, 30);
  h_Delta_Mass_Difference_SF_OS->SetXTitle("|#Deltam_{B_{1}} - #Deltam_{B_{2}}| [MeV]");
  h_Delta_Mass_Difference_SF_OS->SetYTitle("Entries / 1 MeV");

  h_Delta_Mass_Difference_OF=new TH1F("h_Delta_Mass_Difference_OF", "", 30, 0, 30);
  h_Delta_Mass_Difference_OF->SetXTitle("|#Deltam_{B_{1}} - #Deltam_{B_{2}}| [MeV]");
  h_Delta_Mass_Difference_OF->SetYTitle("Entries / 1 MeV");

  h_Delta_Mass_Difference_OF_SS=new TH1F("h_Delta_Mass_Difference_OF_SS", "", 30, 0, 30);
  h_Delta_Mass_Difference_OF_SS->SetXTitle("|#Deltam_{B_{1}} - #Deltam_{B_{2}}| [MeV]");
  h_Delta_Mass_Difference_OF_SS->SetYTitle("Entries / 1 MeV");

  h_Delta_Mass_Difference_OF_OS=new TH1F("h_Delta_Mass_Difference_OF_OS", "", 30, 0, 30);
  h_Delta_Mass_Difference_OF_OS->SetXTitle("|#Deltam_{B_{1}} - #Deltam_{B_{2}}| [MeV]");
  h_Delta_Mass_Difference_OF_OS->SetYTitle("Entries / 1 MeV");

  // Correlation in MC sample
  h_Delta_Time_SF_S=new TH1F("h_Delta_Time_SF_S", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_SF_S->SetXTitle("#Deltat [ps]");
  h_Delta_Time_SF_S->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_SF_B=new TH1F("h_Delta_Time_SF_B", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_SF_B->SetXTitle("#Deltat [ps]");
  h_Delta_Time_SF_B->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_SF_B_Unscaled=new TH1F("h_Delta_Time_SF_B_Unscaled", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_SF_B_Unscaled->SetXTitle("#Deltat [ps]");
  h_Delta_Time_SF_B_Unscaled->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_SF_O=new TH1F("h_Delta_Time_SF_O", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_SF_O->SetXTitle("#Deltat [ps]");
  h_Delta_Time_SF_O->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_SF=new TH1F("h_Delta_Time_SF", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_SF->SetXTitle("#Deltat [ps]");
  h_Delta_Time_SF->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_OF_S=new TH1F("h_Delta_Time_OF_S", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_OF_S->SetXTitle("#Deltat [ps]");
  h_Delta_Time_OF_S->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_OF_B=new TH1F("h_Delta_Time_OF_B", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_OF_B->SetXTitle("#Deltat [ps]");
  h_Delta_Time_OF_B->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_OF_B_Unscaled=new TH1F("h_Delta_Time_OF_B_Unscaled", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_OF_B_Unscaled->SetXTitle("#Deltat [ps]");
  h_Delta_Time_OF_B_Unscaled->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_OF_O=new TH1F("h_Delta_Time_OF_O", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_OF_O->SetXTitle("#Deltat [ps]");
  h_Delta_Time_OF_O->SetYTitle("Entries / 0.5 ps");

  h_Delta_Time_OF=new TH1F("h_Delta_Time_OF", "", 15, 0.25, Time_bin_end);
  h_Delta_Time_OF->SetXTitle("#Deltat [ps]");
  h_Delta_Time_OF->SetYTitle("Entries / 0.5 ps");

  h_Correlation=new TH1F("h_Correlation", "", 15, 0.25, Time_bin_end);
  h_Correlation->SetXTitle("#Deltat [ps]");
  h_Correlation->SetYTitle("Correlation / 0.5 ps");

  h_Significance=new TH1F("h_Significance", "", 7, Significance_bin_start, Significance_bin_end);
  h_Significance->SetXTitle("#Deltat [ps]");
  h_Significance->SetYTitle("|S| / 0.5 ps");

  return;
}
void BTreeAnalize::Init(TTree *tree)
{
  // Set object pointer
  Track_d0 = 0;
  isMatch = 0;
  isMatch_Pass = 0;
  isPassed = 0;
  isPassedTight = 0;
  TriggerType = 0;
  n_PV = 0;
  n_RPV = 0;
  DecayType = 0;
  BDT_score = 0;
  B_flavor = 0;
  B_nPart = 0;
  B_mass = 0;
  B_mass_err = 0;
  B_eta = 0;
  B_phi = 0;
  B_px = 0;
  B_py = 0;
  B_pz = 0;
  B_E = 0;
  B_pT = 0;
  B_chi2 = 0;
  B_nDoF = 0;
  B_x = 0;
  B_y = 0;
  B_z = 0;
  Visible_mass = 0;
  Visible_pT = 0;
  Visible_eta = 0;
  Visible_phi = 0;
  PiD0_mass = 0;
  D0Pi_mass = 0;
  D0Pi_pT = 0;
  D0Pi_eta = 0;
  D0Pi_phi = 0;
  PV_x = 0;
  PV_y = 0;
  PV_z = 0;
  RPV_x = 0;
  RPV_y = 0;
  RPV_z = 0;
  LxyMaxSumPt2 = 0;
  LxyErrMaxSumPt2 = 0;
  A0MaxSumPt2 = 0;
  A0ErrMaxSumPt2 = 0;
  A0xyMaxSumPt2 = 0;
  A0xyErrMaxSumPt2 = 0;
  Z0MaxSumPt2 = 0;
  Z0ErrMaxSumPt2 = 0;
  TauInvMassPVMaxSumPt2 = 0;
  TauErrInvMassPVMaxSumPt2 = 0;
  TauConstMassPVMaxSumPt2 = 0;
  TauErrConstMassPVMaxSumPt2 = 0;
  n_MuPi = 0;
  MuPi_mass = 0;
  MuPi_pT = 0;
  MuPi_eta = 0;
  MuPi_phi = 0;
  MuPi_chi2 = 0;
  MuPi_nDoF = 0;
  MuPiAft_mass = 0;
  MuPiLxyMaxSumPt2 = 0;
  Pi_s_mass = 0;
  Pi_s_pT = 0;
  Pi_s_charge = 0;
  Pi_s_eta = 0;
  Pi_s_phi = 0;
  Pi_s_theta_star = 0;
  Pi_s_d0 = 0;
  Pi_s_z0 = 0;
  Pi_s_qOverP = 0;
  n_Mu = 0;
  Mu_quality = 0;
  Mu_PassIso = 0;
  Mu_mass = 0;
  Mu_pT = 0;
  Mu_charge = 0;
  Mu_eta = 0;
  Mu_phi = 0;
  Mu_d0 = 0;
  Mu_z0 = 0;
  Mu_qOverP = 0;
  Mu_chi2 = 0;
  Mu_nDoF = 0;
  Mu_Chi2_B = 0;
  Mu_nDoF_B = 0;
  D0_mass = 0;
  D0_mass_err = 0;
  D0_pT = 0;
  D0_pT_err = 0;
  D0_Lxy = 0;
  D0_Lxy_err = 0;
  D0_Tau = 0;
  D0_Tau_err = 0;
  D0_chi2 = 0;
  D0_nDoF = 0;
  D0_flag = 0;
  KPi_mass = 0;
  KPi_pT = 0;
  KPi_eta = 0;
  KPi_phi = 0;
  Pi1_mass = 0;
  Pi1_pT = 0;
  Pi1_charge = 0;
  Pi1_eta = 0;
  Pi1_phi = 0;
  Pi1_theta_star = 0;
  Pi1_d0 = 0;
  Pi1_z0 = 0;
  Pi1_qOverP = 0;
  Pi2_mass = 0;
  Pi2_pT = 0;
  Pi2_charge = 0;
  Pi2_eta = 0;
  Pi2_phi = 0;
  Pi2_theta_star = 0;
  Pi2_d0 = 0;
  Pi2_z0 = 0;
  Pi2_qOverP = 0;
  Pi3_mass = 0;
  Pi3_pT = 0;
  Pi3_charge = 0;
  Pi3_eta = 0;
  Pi3_phi = 0;
  Pi3_theta_star = 0;
  Pi3_d0 = 0;
  Pi3_z0 = 0;
  Pi3_qOverP = 0;
  K_mass = 0;
  K_pT = 0;
  K_charge = 0;
  K_eta = 0;
  K_phi = 0;
  K_theta_star = 0;
  K_d0 = 0;
  K_z0 = 0;
  K_qOverP = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("event_num", &event_num, &b_event_num);
  fChain->SetBranchAddress("jet_num", &jet_num, &b_jet_num);
  fChain->SetBranchAddress("PassGRL", &PassGRL, &b_PassGRL);
  fChain->SetBranchAddress("n_Track", &n_Track, &b_n_Track);
  fChain->SetBranchAddress("Track_d0", &Track_d0, &b_Track_d0);
  fChain->SetBranchAddress("isMatch", &isMatch, &b_isMatch);
  fChain->SetBranchAddress("isMatch_Pass", &isMatch_Pass, &b_isMatch_Pass);
  fChain->SetBranchAddress("isPassed", &isPassed, &b_isPassed);
  fChain->SetBranchAddress("isPassedTight", &isPassedTight, &b_isPassedTight);
  fChain->SetBranchAddress("TriggerPass", &TriggerPass, &b_TriggerPass);
  fChain->SetBranchAddress("TriggerType", &TriggerType, &b_TriggerType);
  fChain->SetBranchAddress("n_B", &n_B, &b_n_B);
  fChain->SetBranchAddress("n_B_Truth", &n_B_Truth, &b_n_B_Truth);
  fChain->SetBranchAddress("n_Dstar1", &n_Dstar1, &b_n_Dstar1);
  fChain->SetBranchAddress("n_Dstar1_Pass", &n_Dstar1_Pass, &b_n_Dstar1_Pass);
  fChain->SetBranchAddress("n_Dstar1_Pass_Tight", &n_Dstar1_Pass_Tight, &b_n_Dstar1_Pass_Tight);
  fChain->SetBranchAddress("n_Dstar1_Match", &n_Dstar1_Match, &b_n_Dstar1_Match);
  fChain->SetBranchAddress("n_Dstar1_Match_Pass", &n_Dstar1_Match_Pass, &b_n_Dstar1_Match_Pass);
  fChain->SetBranchAddress("n_Dstar1_Truth", &n_Dstar1_Truth, &b_n_Dstar1_Truth);
  fChain->SetBranchAddress("n_Dstar1_Truth_HighPt", &n_Dstar1_Truth_HighPt, &b_n_Dstar1_Truth_HighPt);
  fChain->SetBranchAddress("n_Dstar2", &n_Dstar2, &b_n_Dstar2);
  fChain->SetBranchAddress("n_Dstar2_Pass", &n_Dstar2_Pass, &b_n_Dstar2_Pass);
  fChain->SetBranchAddress("n_Dstar2_Pass_Tight", &n_Dstar2_Pass_Tight, &b_n_Dstar2_Pass_Tight);
  fChain->SetBranchAddress("n_Dstar2_Match", &n_Dstar2_Match, &b_n_Dstar2_Match);
  fChain->SetBranchAddress("n_Dstar2_Match_Pass", &n_Dstar2_Match_Pass, &b_n_Dstar2_Match_Pass);
  fChain->SetBranchAddress("n_Dstar2_Truth", &n_Dstar2_Truth, &b_n_Dstar2_Truth);
  fChain->SetBranchAddress("n_Dstar2_Truth_HighPt", &n_Dstar2_Truth_HighPt, &b_n_Dstar2_Truth_HighPt);
  fChain->SetBranchAddress("n_Mu_Truth", &n_Mu_Truth, &b_n_Mu_Truth);
  fChain->SetBranchAddress("n_MET", &n_MET, &b_n_MET);
  fChain->SetBranchAddress("n_MET_Truth", &n_MET_Truth, &b_n_MET_Truth);
  fChain->SetBranchAddress("n_PV", &n_PV, &b_n_PV);
  fChain->SetBranchAddress("n_RPV", &n_RPV, &b_n_RPV);
  fChain->SetBranchAddress("MET_px", &MET_px, &b_MET_px);
  fChain->SetBranchAddress("MET_py", &MET_py, &b_MET_py);
  fChain->SetBranchAddress("MET_sum", &MET_sum, &b_MET_sum);
  fChain->SetBranchAddress("Rec_MET_px", &Rec_MET_px, &b_Rec_MET_px);
  fChain->SetBranchAddress("Rec_MET_py", &Rec_MET_py, &b_Rec_MET_py);
  fChain->SetBranchAddress("Rec_MET_sum", &Rec_MET_sum, &b_Rec_MET_sum);
  fChain->SetBranchAddress("Rec_MET_Mu_px", &Rec_MET_Mu_px, &b_Rec_MET_Mu_px);
  fChain->SetBranchAddress("Rec_MET_Mu_py", &Rec_MET_Mu_py, &b_Rec_MET_Mu_py);
  fChain->SetBranchAddress("Rec_MET_Mu_sum", &Rec_MET_Mu_sum, &b_Rec_MET_Mu_sum);
  fChain->SetBranchAddress("Track_MET_px", &Track_MET_px, &b_Track_MET_px);
  fChain->SetBranchAddress("Track_MET_py", &Track_MET_py, &b_Track_MET_py);
  fChain->SetBranchAddress("Track_MET_sum", &Track_MET_sum, &b_Track_MET_sum);
  fChain->SetBranchAddress("Truth_MET_px", &Truth_MET_px, &b_Truth_MET_px);
  fChain->SetBranchAddress("Truth_MET_py", &Truth_MET_py, &b_Truth_MET_py);
  fChain->SetBranchAddress("Truth_MET_sum", &Truth_MET_sum, &b_Truth_MET_sum);
  fChain->SetBranchAddress("DecayType", &DecayType, &b_DecayType);
  fChain->SetBranchAddress("BDT_score", &BDT_score, &b_BDT_score);
  fChain->SetBranchAddress("B_flavor", &B_flavor, &b_B_flavor);
  fChain->SetBranchAddress("B_nPart", &B_nPart, &b_B_nPart);
  fChain->SetBranchAddress("B_mass", &B_mass, &b_B_mass);
  fChain->SetBranchAddress("B_mass_err", &B_mass_err, &b_B_mass_err);
  fChain->SetBranchAddress("B_eta", &B_eta, &b_B_eta);
  fChain->SetBranchAddress("B_phi", &B_phi, &b_B_phi);
  fChain->SetBranchAddress("B_px", &B_px, &b_B_px);
  fChain->SetBranchAddress("B_py", &B_py, &b_B_py);
  fChain->SetBranchAddress("B_pz", &B_pz, &b_B_pz);
  fChain->SetBranchAddress("B_E", &B_E, &b_B_E);
  fChain->SetBranchAddress("B_pT", &B_pT, &b_B_pT);
  fChain->SetBranchAddress("B_chi2", &B_chi2, &b_B_chi2);
  fChain->SetBranchAddress("B_nDoF", &B_nDoF, &b_B_nDoF);
  fChain->SetBranchAddress("B_x", &B_x, &b_B_x);
  fChain->SetBranchAddress("B_y", &B_y, &b_B_y);
  fChain->SetBranchAddress("B_z", &B_z, &b_B_z);
  fChain->SetBranchAddress("Visible_mass", &Visible_mass, &b_Visible_mass);
  fChain->SetBranchAddress("Visible_pT", &Visible_pT, &b_Visible_pT);
  fChain->SetBranchAddress("Visible_eta", &Visible_eta, &b_Visible_eta);
  fChain->SetBranchAddress("Visible_phi", &Visible_phi, &b_Visible_phi);
  fChain->SetBranchAddress("PiD0_mass", &PiD0_mass, &b_PiD0_mass);
  fChain->SetBranchAddress("D0Pi_mass", &D0Pi_mass, &b_D0Pi_mass);
  fChain->SetBranchAddress("D0Pi_pT", &D0Pi_pT, &b_D0Pi_pT);
  fChain->SetBranchAddress("D0Pi_eta", &D0Pi_eta, &b_D0Pi_eta);
  fChain->SetBranchAddress("D0Pi_phi", &D0Pi_phi, &b_D0Pi_phi);
  fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
  fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
  fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
  fChain->SetBranchAddress("RPV_x", &RPV_x, &b_RPV_x);
  fChain->SetBranchAddress("RPV_y", &RPV_y, &b_RPV_y);
  fChain->SetBranchAddress("RPV_z", &RPV_z, &b_RPV_z);
  fChain->SetBranchAddress("LxyMaxSumPt2", &LxyMaxSumPt2, &b_LxyMaxSumPt2);
  fChain->SetBranchAddress("LxyErrMaxSumPt2", &LxyErrMaxSumPt2, &b_LxyErrMaxSumPt2);
  fChain->SetBranchAddress("A0MaxSumPt2", &A0MaxSumPt2, &b_A0MaxSumPt2);
  fChain->SetBranchAddress("A0ErrMaxSumPt2", &A0ErrMaxSumPt2, &b_A0ErrMaxSumPt2);
  fChain->SetBranchAddress("A0xyMaxSumPt2", &A0xyMaxSumPt2, &b_A0xyMaxSumPt2);
  fChain->SetBranchAddress("A0xyErrMaxSumPt2", &A0xyErrMaxSumPt2, &b_A0xyErrMaxSumPt2);
  fChain->SetBranchAddress("Z0MaxSumPt2", &Z0MaxSumPt2, &b_Z0MaxSumPt2);
  fChain->SetBranchAddress("Z0ErrMaxSumPt2", &Z0ErrMaxSumPt2, &b_Z0ErrMaxSumPt2);
  fChain->SetBranchAddress("TauInvMassPVMaxSumPt2", &TauInvMassPVMaxSumPt2, &b_TauInvMassPVMaxSumPt2);
  fChain->SetBranchAddress("TauErrInvMassPVMaxSumPt2", &TauErrInvMassPVMaxSumPt2, &b_TauErrInvMassPVMaxSumPt2);
  fChain->SetBranchAddress("TauConstMassPVMaxSumPt2", &TauConstMassPVMaxSumPt2, &b_TauConstMassPVMaxSumPt2);
  fChain->SetBranchAddress("TauErrConstMassPVMaxSumPt2", &TauErrConstMassPVMaxSumPt2, &b_TauErrConstMassPVMaxSumPt2);
  fChain->SetBranchAddress("n_MuPi", &n_MuPi, &b_n_MuPi);
  fChain->SetBranchAddress("MuPi_mass", &MuPi_mass, &b_MuPi_mass);
  fChain->SetBranchAddress("MuPi_pT", &MuPi_pT, &b_MuPi_pT);
  fChain->SetBranchAddress("MuPi_eta", &MuPi_eta, &b_MuPi_eta);
  fChain->SetBranchAddress("MuPi_phi", &MuPi_phi, &b_MuPi_phi);
  fChain->SetBranchAddress("MuPi_chi2", &MuPi_chi2, &b_MuPi_chi2);
  fChain->SetBranchAddress("MuPi_nDoF", &MuPi_nDoF, &b_MuPi_nDoF);
  fChain->SetBranchAddress("MuPiAft_mass", &MuPiAft_mass, &b_MuPiAft_mass);
  fChain->SetBranchAddress("MuPiLxyMaxSumPt2", &MuPiLxyMaxSumPt2, &b_MuPiLxyMaxSumPt2);
  fChain->SetBranchAddress("Pi_s_mass", &Pi_s_mass, &b_Pi_s_mass);
  fChain->SetBranchAddress("Pi_s_pT", &Pi_s_pT, &b_Pi_s_pT);
  fChain->SetBranchAddress("Pi_s_charge", &Pi_s_charge, &b_Pi_s_charge);
  fChain->SetBranchAddress("Pi_s_eta", &Pi_s_eta, &b_Pi_s_eta);
  fChain->SetBranchAddress("Pi_s_phi", &Pi_s_phi, &b_Pi_s_phi);
  fChain->SetBranchAddress("Pi_s_theta_star", &Pi_s_theta_star, &b_Pi_s_theta_star);
  fChain->SetBranchAddress("Pi_s_d0", &Pi_s_d0, &b_Pi_s_d0);
  fChain->SetBranchAddress("Pi_s_z0", &Pi_s_z0, &b_Pi_s_z0);
  fChain->SetBranchAddress("Pi_s_qOverP", &Pi_s_qOverP, &b_Pi_s_qOverP);
  fChain->SetBranchAddress("n_Mu", &n_Mu, &b_n_Mu);
  fChain->SetBranchAddress("Mu_quality", &Mu_quality, &b_Mu_quality);
  fChain->SetBranchAddress("Mu_PassIso", &Mu_PassIso, &b_Mu_PassIso);
  fChain->SetBranchAddress("Mu_mass", &Mu_mass, &b_Mu_mass);
  fChain->SetBranchAddress("Mu_pT", &Mu_pT, &b_Mu_pT);
  fChain->SetBranchAddress("Mu_charge", &Mu_charge, &b_Mu_charge);
  fChain->SetBranchAddress("Mu_eta", &Mu_eta, &b_Mu_eta);
  fChain->SetBranchAddress("Mu_phi", &Mu_phi, &b_Mu_phi);
  fChain->SetBranchAddress("Mu_d0", &Mu_d0, &b_Mu_d0);
  fChain->SetBranchAddress("Mu_z0", &Mu_z0, &b_Mu_z0);
  fChain->SetBranchAddress("Mu_qOverP", &Mu_qOverP, &b_Mu_qOverP);
  fChain->SetBranchAddress("Mu_chi2", &Mu_chi2, &b_Mu_chi2);
  fChain->SetBranchAddress("Mu_nDoF", &Mu_nDoF, &b_Mu_nDoF);
  fChain->SetBranchAddress("Mu_Chi2_B", &Mu_Chi2_B, &b_Mu_Chi2_B);
  fChain->SetBranchAddress("Mu_nDoF_B", &Mu_nDoF_B, &b_Mu_nDoF_B);
  fChain->SetBranchAddress("D0_mass", &D0_mass, &b_D0_mass);
  fChain->SetBranchAddress("D0_mass_err", &D0_mass_err, &b_D0_mass_err);
  fChain->SetBranchAddress("D0_pT", &D0_pT, &b_D0_pT);
  fChain->SetBranchAddress("D0_pT_err", &D0_pT_err, &b_D0_pT_err);
  fChain->SetBranchAddress("D0_Lxy", &D0_Lxy, &b_D0_Lxy);
  fChain->SetBranchAddress("D0_Lxy_err", &D0_Lxy_err, &b_D0_Lxy_err);
  fChain->SetBranchAddress("D0_Tau", &D0_Tau, &b_D0_Tau);
  fChain->SetBranchAddress("D0_Tau_err", &D0_Tau_err, &b_D0_Tau_err);
  fChain->SetBranchAddress("D0_chi2", &D0_chi2, &b_D0_chi2);
  fChain->SetBranchAddress("D0_nDoF", &D0_nDoF, &b_D0_nDoF);
  fChain->SetBranchAddress("D0_flag", &D0_flag, &b_D0_flag);
  fChain->SetBranchAddress("KPi_mass", &KPi_mass, &b_KPi_mass);
  fChain->SetBranchAddress("KPi_pT", &KPi_pT, &b_KPi_pT);
  fChain->SetBranchAddress("KPi_eta", &KPi_eta, &b_KPi_eta);
  fChain->SetBranchAddress("KPi_phi", &KPi_phi, &b_KPi_phi);
  fChain->SetBranchAddress("Pi1_mass", &Pi1_mass, &b_Pi1_mass);
  fChain->SetBranchAddress("Pi1_pT", &Pi1_pT, &b_Pi1_pT);
  fChain->SetBranchAddress("Pi1_charge", &Pi1_charge, &b_Pi1_charge);
  fChain->SetBranchAddress("Pi1_eta", &Pi1_eta, &b_Pi1_eta);
  fChain->SetBranchAddress("Pi1_phi", &Pi1_phi, &b_Pi1_phi);
  fChain->SetBranchAddress("Pi1_theta_star", &Pi1_theta_star, &b_Pi1_theta_star);
  fChain->SetBranchAddress("Pi1_d0", &Pi1_d0, &b_Pi1_d0);
  fChain->SetBranchAddress("Pi1_z0", &Pi1_z0, &b_Pi1_z0);
  fChain->SetBranchAddress("Pi1_qOverP", &Pi1_qOverP, &b_Pi1_qOverP);
  fChain->SetBranchAddress("Pi2_mass", &Pi2_mass, &b_Pi2_mass);
  fChain->SetBranchAddress("Pi2_pT", &Pi2_pT, &b_Pi2_pT);
  fChain->SetBranchAddress("Pi2_charge", &Pi2_charge, &b_Pi2_charge);
  fChain->SetBranchAddress("Pi2_eta", &Pi2_eta, &b_Pi2_eta);
  fChain->SetBranchAddress("Pi2_phi", &Pi2_phi, &b_Pi2_phi);
  fChain->SetBranchAddress("Pi2_theta_star", &Pi2_theta_star, &b_Pi2_theta_star);
  fChain->SetBranchAddress("Pi2_d0", &Pi2_d0, &b_Pi2_d0);
  fChain->SetBranchAddress("Pi2_z0", &Pi2_z0, &b_Pi2_z0);
  fChain->SetBranchAddress("Pi2_qOverP", &Pi2_qOverP, &b_Pi2_qOverP);
  fChain->SetBranchAddress("Pi3_mass", &Pi3_mass, &b_Pi3_mass);
  fChain->SetBranchAddress("Pi3_pT", &Pi3_pT, &b_Pi3_pT);
  fChain->SetBranchAddress("Pi3_charge", &Pi3_charge, &b_Pi3_charge);
  fChain->SetBranchAddress("Pi3_eta", &Pi3_eta, &b_Pi3_eta);
  fChain->SetBranchAddress("Pi3_phi", &Pi3_phi, &b_Pi3_phi);
  fChain->SetBranchAddress("Pi3_theta_star", &Pi3_theta_star, &b_Pi3_theta_star);
  fChain->SetBranchAddress("Pi3_d0", &Pi3_d0, &b_Pi3_d0);
  fChain->SetBranchAddress("Pi3_z0", &Pi3_z0, &b_Pi3_z0);
  fChain->SetBranchAddress("Pi3_qOverP", &Pi3_qOverP, &b_Pi3_qOverP);
  fChain->SetBranchAddress("K_mass", &K_mass, &b_K_mass);
  fChain->SetBranchAddress("K_pT", &K_pT, &b_K_pT);
  fChain->SetBranchAddress("K_charge", &K_charge, &b_K_charge);
  fChain->SetBranchAddress("K_eta", &K_eta, &b_K_eta);
  fChain->SetBranchAddress("K_phi", &K_phi, &b_K_phi);
  fChain->SetBranchAddress("K_theta_star", &K_theta_star, &b_K_theta_star);
  fChain->SetBranchAddress("K_d0", &K_d0, &b_K_d0);
  fChain->SetBranchAddress("K_z0", &K_z0, &b_K_z0);
  fChain->SetBranchAddress("K_qOverP", &K_qOverP, &b_K_qOverP);
  Notify();
}

Bool_t BTreeAnalize::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void BTreeAnalize::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t BTreeAnalize::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}

// Define a function with three parameters.
Double_t  BTreeAnalize::FitFunction(double x, double par0, double par1, double par2) {
  double arg = 0;
  if (par2!=0) arg = TMath::Abs((x - par1)/par2);
  double FitVal = par0*TMath::Exp(-0.5*TMath::Power(arg, 1.+1./(1+0.5*arg)));
  return FitVal;
}

// Define a function with three parameters.
/*Double_t  BTreeAnalize::FitFunction(double x, double par0, double par1, double par2) {
  double arg = 0;
  if (par[2]!=0) arg = TMath::Abs((x[0] - par[1])/par[2]);
  double FitVal = par[0]*TMath::Exp(-0.5*TMath::Power(arg, 1.+1./(1+0.5*arg)));
  return FitVal;
  }*/

#endif // #ifdef BTreeAnalize_cxx

