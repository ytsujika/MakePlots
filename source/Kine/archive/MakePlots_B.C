#define MakePlots_B_cxx
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void MakePlots_B(const int& isMC, const string& inname, const string& outname1, const string& outname2)
{

   string infile(inname);
   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(infile.c_str());
   if (!f || !f->IsOpen()) {
     f = new TFile(infile.c_str());
   }

   TH1::SetDefaultSumw2();

   // Definition of histogram
   // Decay mode : B0 -> D* mu nu 
   TH1F *h_Delta_Mass_SF_S[15];
   TH1F *h_Delta_Mass_SF_B[15];
   TH1F *h_Delta_Mass_SF[15];
   TH1F *h_Delta_Mass_OF_S[15];
   TH1F *h_Delta_Mass_OF_B[15];
   TH1F *h_Delta_Mass_OF[15];
   TH1F *h_Mass_Difference_SF_S[5][4];
   TH1F *h_Mass_Difference_SF_B[5][4];
   TH1F *h_Mass_Difference_SF[5][4];
   TH1F *h_Mass_Difference_OF_S[5][4];
   TH1F *h_Mass_Difference_OF_B[5][4];
   TH1F *h_Mass_Difference_OF[5][4];

   //Fitting function
   TF1 *Function_Delta_Mass_SF[15];
   TF1 *Function_Delta_Mass_SF_S[15];
   TF1 *Function_Delta_Mass_SF_B[15];
   TF1 *Function_Delta_Mass_OF[15];
   TF1 *Function_Delta_Mass_OF_S[15];
   TF1 *Function_Delta_Mass_OF_B[15];
   TF1 *Function_Mass_Difference_SF[5][4];
   TF1 *Function_Mass_Difference_SF_S[5][4];
   TF1 *Function_Mass_Difference_SF_B[5][4];
   TF1 *Function_Mass_Difference_OF[5][4];
   TF1 *Function_Mass_Difference_OF_S[5][4];
   TF1 *Function_Mass_Difference_OF_B[5][4];

   // Correlation in MC sample
   TH1F *h_Delta_Time_SF;
   TH1F *h_Delta_Time_OF;
   TH1F *h_Correlation_All;
   TH1F *h_Time_Difference_SF[5];
   TH1F *h_Time_Difference_OF[5];
   TH1F *h_Correlation[5];
   TH1F *h_Significance;


   // Decay mode : B0 -> D* mu nu
   for(int i=0;i<15;i++){
     std::stringstream ssa;
     ssa<<"h_Delta_Mass_SF_S_"<<std::setw(2)<<std::setfill('0')<<i;
     h_Delta_Mass_SF_S[i]=(TH1F*)f->Get(ssa.str().c_str());
     h_Delta_Mass_SF_S[i]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
     h_Delta_Mass_SF_S[i]->SetYTitle("Entries / 0.5 MeV");

     std::stringstream ssb;
     ssb<<"h_Delta_Mass_SF_B_"<<std::setw(2)<<std::setfill('0')<<i;
     h_Delta_Mass_SF_B[i]=(TH1F*)f->Get(ssb.str().c_str());
     h_Delta_Mass_SF_B[i]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
     h_Delta_Mass_SF_B[i]->SetYTitle("Entries / 0.5 MeV");

     std::stringstream ssc;
     ssc<<"h_Delta_Mass_SF_"<<std::setw(2)<<std::setfill('0')<<i;
     h_Delta_Mass_SF[i]=(TH1F*)f->Get(ssc.str().c_str());
     h_Delta_Mass_SF[i]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
     h_Delta_Mass_SF[i]->SetYTitle("Entries / 0.5 MeV");

     std::stringstream ssd;
     ssd<<"h_Delta_Mass_OF_S_"<<std::setw(2)<<std::setfill('0')<<i;
     h_Delta_Mass_OF_S[i]=(TH1F*)f->Get(ssd.str().c_str());
     h_Delta_Mass_OF_S[i]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
     h_Delta_Mass_OF_S[i]->SetYTitle("Entries / 0.5 MeV");

     std::stringstream sse;
     sse<<"h_Delta_Mass_OF_B_"<<std::setw(2)<<std::setfill('0')<<i;
     h_Delta_Mass_OF_B[i]=(TH1F*)f->Get(sse.str().c_str());
     h_Delta_Mass_OF_B[i]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
     h_Delta_Mass_OF_B[i]->SetYTitle("Entries / 0.5 MeV");

     std::stringstream ssf;
     ssf<<"h_Delta_Mass_OF_"<<std::setw(2)<<std::setfill('0')<<i;
     h_Delta_Mass_OF[i]=(TH1F*)f->Get(ssf.str().c_str());
     h_Delta_Mass_OF[i]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
     h_Delta_Mass_OF[i]->SetYTitle("Entries / 0.5 MeV");
   }

   for(int i=0;i<5;i++){
     for(int j=0;j<4;j++){
       std::stringstream ss1;
       ss1<<"h_Mass_Difference_SF_S_Bin"<<i+1<<"_Term"<<j+1;
       h_Mass_Difference_SF_S[i][j]=(TH1F*)f->Get(ss1.str().c_str());
       h_Mass_Difference_SF_S[i][j]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
       h_Mass_Difference_SF_S[i][j]->SetYTitle("Entries / 0.5 MeV");

       std::stringstream ss2;
       ss2<<"h_Mass_Difference_SF_B_Bin"<<i+1<<"_Term"<<j+1;
       h_Mass_Difference_SF_B[i][j]=(TH1F*)f->Get(ss2.str().c_str());
       h_Mass_Difference_SF_B[i][j]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
       h_Mass_Difference_SF_B[i][j]->SetYTitle("Entries / 0.5 MeV");

       std::stringstream ss3;
       ss3<<"h_Mass_Difference_SF_Bin"<<i+1<<"_Term"<<j+1;
       h_Mass_Difference_SF[i][j]=(TH1F*)f->Get(ss3.str().c_str());
       h_Mass_Difference_SF[i][j]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
       h_Mass_Difference_SF[i][j]->SetYTitle("Entries / 0.5 MeV"); 

       std::stringstream ss4;
       ss4<<"h_Mass_Difference_OF_S_Bin"<<i+1<<"_Term"<<j+1;
       h_Mass_Difference_OF_S[i][j]=(TH1F*)f->Get(ss4.str().c_str());
       h_Mass_Difference_OF_S[i][j]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
       h_Mass_Difference_OF_S[i][j]->SetYTitle("Entries / 0.5 MeV");

       std::stringstream ss5;
       ss5<<"h_Mass_Difference_OF_B_Bin"<<i+1<<"_Term"<<j+1;
       h_Mass_Difference_OF_B[i][j]=(TH1F*)f->Get(ss5.str().c_str());
       h_Mass_Difference_OF_B[i][j]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
       h_Mass_Difference_OF_B[i][j]->SetYTitle("Entries / 0.5 MeV");

       std::stringstream ss6;
       ss6<<"h_Mass_Difference_OF_Bin"<<i+1<<"_Term"<<j+1;
       h_Mass_Difference_OF[i][j]=(TH1F*)f->Get(ss6.str().c_str());
       h_Mass_Difference_OF[i][j]->SetXTitle("m_{D*}-m_{D^{0}} [MeV]");
       h_Mass_Difference_OF[i][j]->SetYTitle("Entries / 0.5 MeV");
     }
   }

   for(int i=0;i<15;i++){
     std::stringstream ssa;
     ssa<<"Function_Delta_Mass_SF_S_"<<std::setw(2)<<std::setfill('0')<<i;
     Function_Delta_Mass_SF_S[i]=(TF1*)f->Get(ssa.str().c_str());

     std::stringstream ssb;
     ssb<<"Function_Delta_Mass_SF_B_"<<std::setw(2)<<std::setfill('0')<<i;
     Function_Delta_Mass_SF_B[i]=(TF1*)f->Get(ssb.str().c_str());

     std::stringstream ssc;
     ssc<<"Function_Delta_Mass_SF_"<<std::setw(2)<<std::setfill('0')<<i;
     Function_Delta_Mass_SF[i]=(TF1*)f->Get(ssc.str().c_str());

     std::stringstream ssd;
     ssd<<"Function_Delta_Mass_OF_S_"<<std::setw(2)<<std::setfill('0')<<i;
     Function_Delta_Mass_OF_S[i]=(TF1*)f->Get(ssd.str().c_str());

     std::stringstream sse;
     sse<<"Function_Delta_Mass_OF_B_"<<std::setw(2)<<std::setfill('0')<<i;
     Function_Delta_Mass_OF_B[i]=(TF1*)f->Get(sse.str().c_str());

     std::stringstream ssf;
     ssf<<"Function_Delta_Mass_OF_"<<std::setw(2)<<std::setfill('0')<<i;
     Function_Delta_Mass_OF[i]=(TF1*)f->Get(ssf.str().c_str());
   }

   for(int i=0;i<5;i++){
     for(int j=0;j<4;j++){
       std::stringstream ss1;
       ss1<<"Function_Mass_Difference_SF_S_Bin"<<i+1<<"_Term"<<j+1;
       Function_Mass_Difference_SF_S[i][j]=(TF1*)f->Get(ss1.str().c_str());

       std::stringstream ss2;
       ss2<<"Function_Mass_Difference_SF_B_Bin"<<i+1<<"_Term"<<j+1;
       Function_Mass_Difference_SF_B[i][j]=(TF1*)f->Get(ss2.str().c_str());

       std::stringstream ss3;
       ss3<<"Function_Mass_Difference_SF_Bin"<<i+1<<"_Term"<<j+1;
       Function_Mass_Difference_SF[i][j]=(TF1*)f->Get(ss3.str().c_str());

       std::stringstream ss4;
       ss4<<"Function_Mass_Difference_OF_S_Bin"<<i+1<<"_Term"<<j+1;
       Function_Mass_Difference_OF_S[i][j]=(TF1*)f->Get(ss4.str().c_str());

       std::stringstream ss5;
       ss5<<"Function_Mass_Difference_OF_B_Bin"<<i+1<<"_Term"<<j+1;
       Function_Mass_Difference_OF_B[i][j]=(TF1*)f->Get(ss5.str().c_str());

       std::stringstream ss6;
       ss6<<"Function_Mass_Difference_OF_Bin"<<i+1<<"_Term"<<j+1;
       Function_Mass_Difference_OF[i][j]=(TF1*)f->Get(ss6.str().c_str());
     }
   }

   // Correlation in MC sample
   h_Delta_Time_SF=(TH1F*)f->Get("h_Delta_Time_SF");
   h_Delta_Time_SF->SetXTitle("#Deltat [ps]");
   h_Delta_Time_SF->SetYTitle("Entries / 0.5 ps");
   h_Delta_Time_OF=(TH1F*)f->Get("h_Delta_Time_OF");
   h_Delta_Time_OF->SetXTitle("#Deltat [ps]");
   h_Delta_Time_OF->SetYTitle("Entries / 0.5 ps");

   h_Correlation_All=(TH1F*)f->Get("h_Correlation_All");
   h_Correlation_All->SetXTitle("#Deltat [ps]");
   h_Correlation_All->SetYTitle("Correlation / 0.5 ps");

   for(int i=0;i<5;i++){
     std::stringstream ss1;
     ss1<<"h_Time_Difference_SF_Bin"<<i+1;
     h_Time_Difference_SF[i]=(TH1F*)f->Get(ss1.str().c_str());
     h_Time_Difference_SF[i]->SetXTitle("Term");
     h_Time_Difference_SF[i]->SetYTitle("Entries");

     std::stringstream ss2;
     ss2<<"h_Time_Difference_OF_Bin"<<i+1;
     h_Time_Difference_OF[i]=(TH1F*)f->Get(ss2.str().c_str());
     h_Time_Difference_OF[i]->SetXTitle("Term");
     h_Time_Difference_OF[i]->SetYTitle("Entries");

     std::stringstream ss;
     ss<<"h_Correlation_Bin"<<i+1;
     h_Correlation[i]=(TH1F*)f->Get(ss.str().c_str());
     h_Correlation[i]->SetXTitle("Term");
     h_Correlation[i]->SetYTitle("Correlation");
   }

   h_Significance=(TH1F*)f->Get("h_Significance");
   h_Significance->SetXTitle("#Deltat [ps]");
   h_Significance->SetYTitle("|S| / 0.5 ps");


   // Make Plots
   int color[8]=
     {400, 632, 600, 800, 432, 616, 416, 0}; //Yel, Red, Cya, Blu, Ora, Gre, Mag, Gre, Whi

   TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
   l.SetNDC();
   l.SetTextFont(72);
   l.SetTextColor(kBlack);
   l.SetTextSize(0.04); 
   TLatex p; 
   p.SetNDC();
   p.SetTextFont(42);
   p.SetTextSize(0.04); 
   p.SetTextColor(kBlack);
   TLatex n1; 
   n1.SetNDC();
   n1.SetTextFont(42);
   n1.SetTextSize(0.032); 
   n1.SetTextColor(kBlack);
   std::stringstream ss;
   if(isMC)
     ss<<"#splitline{#sqrt{s}=13 TeV, 335 pb^{-1} (After scaling)}{Signal MC sample}";
   else
     ss<<"#splitline{#sqrt{s}=13 TeV, 335 pb^{-1}}{Low Luminosity Run Data}";

   TLatex title; 
   title.SetNDC();
   title.SetTextFont(42);
   title.SetTextSize(0.01); 
   title.SetTextColor(kBlack);

   TLegend *legend = new TLegend( 0.7, 0.2, 0.9, 0.28) ;
   //legend->AddEntry( h_Delta_Mass_S, "D*#mu opposite signs" , "ep") ;
   //legend->AddEntry( h_Delta_Mass_B, "D*#mu same signs" , "f") ;
   legend->SetFillColor(0);
   legend->SetBorderSize(0);
   legend->Draw() ;

   TLatex *   tex = new TLatex(0.5,0.8,"My Title");
   tex->SetNDC();
   tex->SetTextSize(0.2);
   tex->Draw();

   const char *outname3 = outname2.c_str();
   string outname40 = outname2+"(";
   const char *outname4 = outname40.c_str();
   string outname50 = outname2+")";
   const char *outname5 = outname50.c_str();


   TCanvas *c_Delta_Mass_SF_SB;
   TCanvas *c_Delta_Mass_SF;
   TCanvas *c_Delta_Time_SF;
   TCanvas *c_Delta_Mass_OF_SB;
   TCanvas *c_Delta_Mass_OF;
   TCanvas *c_Delta_Time_OF;
   TCanvas *c_Correlation_All;

   TCanvas *c_Mass_Difference_SF_SB[5];
   TCanvas *c_Mass_Difference_SF[5];
   TCanvas *c_Mass_Difference_OF_SB[5];
   TCanvas *c_Mass_Difference_OF[5];

   TCanvas *c_Time_Difference_SF;
   TCanvas *c_Time_Difference_OF;
   TCanvas *c_Correlation;
   TCanvas *c_Significance;

   c_Delta_Mass_SF_SB = new TCanvas("c_Delta_Mass_SF_SB", "Delta Mass of Same Flavor (SB)", 1600, 1200);
   c_Delta_Mass_SF_SB->Divide(4,4);
   for(int i=0;i<15;i++){
     c_Delta_Mass_SF_SB->cd(i+1);
     h_Delta_Mass_SF_S[i]->Draw("HIST");
     h_Delta_Mass_SF_B[i]->Draw("SAME,HIST");
     h_Delta_Mass_SF_S[i]->SetLineColor(color[1]);
     h_Delta_Mass_SF_S[i]->SetFillColor(kWhite);
     h_Delta_Mass_SF_S[i]->SetStats(0);
     h_Delta_Mass_SF_B[i]->SetStats(0);
     h_Delta_Mass_SF_B[i]->SetLineColor(kBlack);
     h_Delta_Mass_SF_B[i]->SetLineStyle(2);
     h_Delta_Mass_SF_B[i]->SetFillColor(kWhite);
     l.DrawLatex(0.55,0.85,"ATLAS");
     p.DrawLatex(0.655,0.85,"Work in Progress");
     n1.DrawLatex(0.56,0.785,ss.str().c_str());
   }
   c_Delta_Mass_SF_SB->SetTicks();
   //c_Delta_Mass_SF_SB->Print(outname4,"pdf");
  
   c_Delta_Mass_SF = new TCanvas("c_Delta_Mass_SF", "Delta Mass of Same Flavor", 1600, 1200);
   c_Delta_Mass_SF->Divide(4,4);
   for(int i=0;i<15;i++){
     c_Delta_Mass_SF->cd(i+1);
     h_Delta_Mass_SF[i]->Draw("EP");
     Function_Delta_Mass_SF[i]->Draw("SAME");
     Function_Delta_Mass_SF[i]->SetLineColor(kBlue);
     h_Delta_Mass_SF[i]->SetMarkerStyle(8);
     h_Delta_Mass_SF[i]->SetMarkerSize(0.7);
     Function_Delta_Mass_SF_S[i]->Draw("SAME");
     Function_Delta_Mass_SF_S[i]->SetLineColor(kRed);
     Function_Delta_Mass_SF_S[i]->SetLineStyle(2);
     Function_Delta_Mass_SF_B[i]->Draw("SAME");
     Function_Delta_Mass_SF_B[i]->SetLineColor(kBlack);
     Function_Delta_Mass_SF_B[i]->SetLineStyle(2);
     gStyle->SetOptStat("e");
     gStyle->SetOptFit(1111);
     //l.DrawLatex(0.55,0.85,"ATLAS");
     //p.DrawLatex(0.655,0.85,"Work in Progress");
     //n1.DrawLatex(0.56,0.785,ss.str().c_str());
   }
   c_Delta_Mass_SF->SetTicks();
   //c_Delta_Mass_SF->Print(outname3,"pdf");

   c_Delta_Mass_OF_SB = new TCanvas("c_Delta_Mass_OF_SB", "Delta Mass of Opposite Flavor (SB)", 1600, 1200);
   c_Delta_Mass_OF_SB->Divide(4,4);
   for(int i=0;i<15;i++){
     c_Delta_Mass_OF_SB->cd(i+1);
     h_Delta_Mass_OF_S[i]->Draw("HIST");
     h_Delta_Mass_OF_B[i]->Draw("SAME,HIST");
     h_Delta_Mass_OF_S[i]->SetLineColor(color[1]);
     h_Delta_Mass_OF_S[i]->SetFillColor(kWhite);
     h_Delta_Mass_OF_S[i]->SetStats(0);
     h_Delta_Mass_OF_B[i]->SetStats(0);
     h_Delta_Mass_OF_B[i]->SetLineColor(kBlack);
     h_Delta_Mass_OF_B[i]->SetLineStyle(2);
     h_Delta_Mass_OF_B[i]->SetFillColor(kWhite);
     l.DrawLatex(0.55,0.85,"ATLAS");
     p.DrawLatex(0.655,0.85,"Work in Progress");
     n1.DrawLatex(0.56,0.785,ss.str().c_str());
   }
   c_Delta_Mass_OF_SB->SetTicks();
   //c_Delta_Mass_OF_SB->Print(outname3,"pdf");
  
   c_Delta_Mass_OF = new TCanvas("c_Delta_Mass_OF", "Delta Mass of Opposite Flavor", 1600, 1200);
   c_Delta_Mass_OF->Divide(4,4);
   for(int i=0;i<15;i++){
     c_Delta_Mass_OF->cd(i+1);
     h_Delta_Mass_OF[i]->Draw("EP");
     Function_Delta_Mass_OF[i]->Draw("SAME");
     Function_Delta_Mass_OF[i]->SetLineColor(kBlue);
     h_Delta_Mass_OF[i]->SetMarkerStyle(8);
     h_Delta_Mass_OF[i]->SetMarkerSize(0.7);
     Function_Delta_Mass_OF_S[i]->Draw("SAME");
     Function_Delta_Mass_OF_S[i]->SetLineColor(kRed);
     Function_Delta_Mass_OF_S[i]->SetLineStyle(2);
     Function_Delta_Mass_OF_B[i]->Draw("SAME");
     Function_Delta_Mass_OF_B[i]->SetLineColor(kBlack);
     Function_Delta_Mass_OF_B[i]->SetLineStyle(2);
     gStyle->SetOptStat("e");
     gStyle->SetOptFit(1111);
     //l.DrawLatex(0.55,0.85,"ATLAS");
     //p.DrawLatex(0.655,0.85,"Work in Progress");
     //n1.DrawLatex(0.56,0.785,ss.str().c_str());
   }
   c_Delta_Mass_OF->SetTicks();
   //c_Delta_Mass_OF->Print(outname3,"pdf");


   for(int i=0;i<5;i++){
     std::stringstream ss1;
     ss1<<"c_Mass_Difference_SF_SB_Bin"<<i+1;
     std::stringstream ss2;
     ss2<<"Mass Difference of Same Flavor (SB) - Bin"<<i+1;
     c_Mass_Difference_SF_SB[i] = new TCanvas(ss1.str().c_str(), ss2.str().c_str(), 1600, 1200);
     title.DrawLatex(0.3,0.98,ss2.str().c_str());
     c_Mass_Difference_SF_SB[i]->Divide(2,2);
     for(int j=0;j<4;j++){
       c_Mass_Difference_SF_SB[i]->cd(j+1);
       h_Mass_Difference_SF_S[i][j]->Draw("HIST");
       h_Mass_Difference_SF_B[i][j]->Draw("SAME,HIST");
       h_Mass_Difference_SF_S[i][j]->SetLineColor(color[1]);
       h_Mass_Difference_SF_S[i][j]->SetFillColor(kWhite);
       h_Mass_Difference_SF_S[i][j]->SetStats(0);
       h_Mass_Difference_SF_B[i][j]->SetStats(0);
       h_Mass_Difference_SF_B[i][j]->SetLineColor(kBlack);
       h_Mass_Difference_SF_B[i][j]->SetLineStyle(2);
       h_Mass_Difference_SF_B[i][j]->SetFillColor(kWhite);
       l.DrawLatex(0.55,0.85,"ATLAS");
       p.DrawLatex(0.655,0.85,"Work in Progress");
       n1.DrawLatex(0.56,0.785,ss.str().c_str());
     }
     c_Mass_Difference_SF_SB[i]->SetTicks();
     //c_Mass_Difference_SF_SB[i]->Print(outname3,"pdf");
   }

   for(int i=0;i<5;i++){
     std::stringstream ss1;
     ss1<<"c_Mass_Difference_SF_Bin"<<i+1;
     std::stringstream ss2;
     ss2<<"Mass Difference of Same Flavor - Bin"<<i+1;
     c_Mass_Difference_SF[i] = new TCanvas(ss1.str().c_str(), ss2.str().c_str(), 1600, 1200);
     title.DrawLatex(0.3,0.98,ss2.str().c_str());
     c_Mass_Difference_SF[i]->Divide(2,2);
     for(int j=0;j<4;j++){
       c_Mass_Difference_SF[i]->cd(j+1);
       h_Mass_Difference_SF[i][j]->Draw("EP");
       Function_Mass_Difference_SF[i][j]->Draw("SAME");
       Function_Mass_Difference_SF[i][j]->SetLineColor(kBlue);
       h_Mass_Difference_SF[i][j]->SetMarkerStyle(8);
       h_Mass_Difference_SF[i][j]->SetMarkerSize(0.7);
       Function_Mass_Difference_SF_S[i][j]->Draw("SAME");
       Function_Mass_Difference_SF_S[i][j]->SetLineColor(kRed);
       Function_Mass_Difference_SF_S[i][j]->SetLineStyle(2);
       Function_Mass_Difference_SF_B[i][j]->Draw("SAME");
       Function_Mass_Difference_SF_B[i][j]->SetLineColor(kBlack);
       Function_Mass_Difference_SF_B[i][j]->SetLineStyle(2);
       gStyle->SetOptStat("e");
       gStyle->SetOptFit(1111);
       //l.DrawLatex(0.55,0.85,"ATLAS");
       //p.DrawLatex(0.655,0.85,"Work in Progress");
       //n1.DrawLatex(0.56,0.785,ss.str().c_str());
     }
     c_Mass_Difference_SF[i]->SetTicks();
     //c_Mass_Difference_SF[i]->Print(outname3,"pdf");
   }

   for(int i=0;i<5;i++){
     std::stringstream ss1;
     ss1<<"c_Mass_Difference_OF_SB_Bin"<<i+1;
     std::stringstream ss2;
     ss2<<"Mass Difference of Opposite Flavor (SB) - Bin"<<i+1;
     c_Mass_Difference_OF_SB[i] = new TCanvas(ss1.str().c_str(), ss2.str().c_str(), 1600, 1200);
     title.DrawLatex(0.3,0.98,ss2.str().c_str());
     c_Mass_Difference_OF_SB[i]->Divide(2,2);
     for(int j=0;j<4;j++){
       c_Mass_Difference_OF_SB[i]->cd(j+1);
       h_Mass_Difference_OF_S[i][j]->Draw("HIST");
       h_Mass_Difference_OF_B[i][j]->Draw("SAME,HIST");
       h_Mass_Difference_OF_S[i][j]->SetLineColor(color[1]);
       h_Mass_Difference_OF_S[i][j]->SetFillColor(kWhite);
       h_Mass_Difference_OF_S[i][j]->SetStats(0);
       h_Mass_Difference_OF_B[i][j]->SetStats(0);
       h_Mass_Difference_OF_B[i][j]->SetLineColor(kBlack);
       h_Mass_Difference_OF_B[i][j]->SetLineStyle(2);
       h_Mass_Difference_OF_B[i][j]->SetFillColor(kWhite);
       l.DrawLatex(0.55,0.85,"ATLAS");
       p.DrawLatex(0.655,0.85,"Work in Progress");
       n1.DrawLatex(0.56,0.785,ss.str().c_str());
     }
     c_Mass_Difference_OF_SB[i]->SetTicks();
     //c_Mass_Difference_OF_SB[i]->Print(outname3,"pdf");
   }

   for(int i=0;i<5;i++){
     std::stringstream ss1;
     ss1<<"c_Mass_Difference_OF_Bin"<<i+1;
     std::stringstream ss2;
     ss2<<"Mass Difference of Opposite Flavor - Bin"<<i+1;
     c_Mass_Difference_OF[i] = new TCanvas(ss1.str().c_str(), ss2.str().c_str(), 1600, 1200);
     title.DrawLatex(0.3,0.98,ss2.str().c_str());
     c_Mass_Difference_OF[i]->Divide(2,2);
     for(int j=0;j<4;j++){
       c_Mass_Difference_OF[i]->cd(j+1);
       h_Mass_Difference_OF[i][j]->Draw("EP");
       Function_Mass_Difference_OF[i][j]->Draw("SAME");
       Function_Mass_Difference_OF[i][j]->SetLineColor(kBlue);
       h_Mass_Difference_OF[i][j]->SetMarkerStyle(8);
       h_Mass_Difference_OF[i][j]->SetMarkerSize(0.7);
       Function_Mass_Difference_OF_S[i][j]->Draw("SAME");
       Function_Mass_Difference_OF_S[i][j]->SetLineColor(kRed);
       Function_Mass_Difference_OF_S[i][j]->SetLineStyle(2);
       Function_Mass_Difference_OF_B[i][j]->Draw("SAME");
       Function_Mass_Difference_OF_B[i][j]->SetLineColor(kBlack);
       Function_Mass_Difference_OF_B[i][j]->SetLineStyle(2);
       gStyle->SetOptStat("e");
       gStyle->SetOptFit(1111);
       //l.DrawLatex(0.55,0.85,"ATLAS");
       //p.DrawLatex(0.655,0.85,"Work in Progress");
       //n1.DrawLatex(0.56,0.785,ss.str().c_str());
     }
     c_Mass_Difference_OF[i]->SetTicks();
     //c_Mass_Difference_OF[i]->Print(outname3,"pdf");
   }


   c_Delta_Time_SF = new TCanvas("c_Delta_Time_SF", "Delta Time of Same Flavor", 1500, 900);
   h_Delta_Time_SF->Draw("HIST");
   c_Delta_Time_SF->SetLogy();
   //c_Delta_Time_SF->Print(outname3,"pdf");
   
   c_Delta_Time_OF = new TCanvas("c_Delta_Time_OF", "Delta Time of Opposite Flavor", 1500, 900);
   h_Delta_Time_OF->Draw("HIST");
   c_Delta_Time_OF->SetLogy();
   //c_Delta_Time_OF->Print(outname3,"pdf");

   c_Correlation_All = new TCanvas("c_Correlation_All", "Correlation like Belle", 1500, 900);
   h_Correlation_All->Draw("EP");
   h_Correlation_All->SetStats(0);
   h_Correlation_All->SetMarkerStyle(8);
   //c_Correlation_All->Print(outname3,"pdf");

   c_Time_Difference_SF = new TCanvas("c_Time_Difference_SF", "Time Difference of Same Flavor", 1500, 900);
   title.DrawLatex(0.3,0.98,"Time Difference of Same Flavor");
   c_Time_Difference_SF->Divide(3,2);
   for(int i=0;i<5;i++){
     c_Time_Difference_SF->cd(i+1);
     h_Time_Difference_SF[i]->Draw("HIST");
     c_Time_Difference_SF->SetLogy();
   }
   //c_Time_Difference_SF->Print(outname3,"pdf");
   
   c_Time_Difference_OF = new TCanvas("c_Time_Difference_OF", "Time Difference of Same Flavor", 1500, 900);
   title.DrawLatex(0.3,0.98,"Time Difference of Opposite Flavor");
   c_Time_Difference_OF->Divide(3,2);
   for(int i=0;i<5;i++){
     c_Time_Difference_OF->cd(i+1);
     c_Time_Difference_OF->SetLogy();
     h_Time_Difference_OF[i]->Draw("HIST");
   }
   //c_Time_Difference_OF->Print(outname3,"pdf");

   c_Correlation = new TCanvas("c_Correlation", "Correlation", 1500, 900);
   title.DrawLatex(0.3,0.98,"Correlation");
   c_Correlation->Divide(3,2);
   for(int i=0;i<5;i++){
     c_Correlation->cd(i+1);
     h_Correlation[i]->Draw("EP");
     h_Correlation[i]->SetStats(0);
     h_Correlation[i]->SetMarkerStyle(8);
   }
   //c_Correlation->Print(outname3,"pdf");

   c_Significance = new TCanvas("c_Significance", "Significance", 900, 675);
   title.DrawLatex(0.3,0.98,"Significance");
   h_Significance->Draw("EP");
   h_Significance->SetStats(0);
   h_Significance->SetMarkerStyle(8);
   h_Significance->SetLineColor(kBlack);
   TF1 *f_Limit=new TF1("2","2",-1,11);
   f_Limit->SetLineStyle(2);
   f_Limit->SetLineColor(kRed);
   f_Limit->Draw("SAME");
   TF1 *f_SM=new TF1("SM","fabs(-3*cos(0.5065*x)+cos(3*0.5065*x))",-1,11);
   f_SM->SetLineColor(kBlack);
   f_SM->Draw("SAME");
   //c_Significance->Print(outname5,"pdf");
   
   TFile *file = new TFile(outname1.c_str(), "RECREATE");

   // Decay mode : B0 -> D* mu nu
   for(int i=0;i<15;i++){
     h_Delta_Mass_SF_S[i]->Write();
     h_Delta_Mass_SF_B[i]->Write();
     h_Delta_Mass_SF[i]->Write();
     h_Delta_Mass_OF_S[i]->Write();
     h_Delta_Mass_OF_B[i]->Write();
     h_Delta_Mass_OF[i]->Write();
     Function_Delta_Mass_SF_S[i]->Write();
     Function_Delta_Mass_SF_B[i]->Write();
     Function_Delta_Mass_SF[i]->Write();
     Function_Delta_Mass_OF_S[i]->Write();
     Function_Delta_Mass_OF_B[i]->Write();
     Function_Delta_Mass_OF[i]->Write();
   }
   for(int i=0;i<5;i++){
     for(int j=0;j<4;j++){
       h_Mass_Difference_SF_S[i][j]->Write();
       h_Mass_Difference_SF_B[i][j]->Write();
       h_Mass_Difference_SF[i][j]->Write();
       h_Mass_Difference_OF_S[i][j]->Write();
       h_Mass_Difference_OF_B[i][j]->Write();
       h_Mass_Difference_OF[i][j]->Write();
       Function_Mass_Difference_SF_S[i][j]->Write();
       Function_Mass_Difference_SF_B[i][j]->Write();
       Function_Mass_Difference_SF[i][j]->Write();
       Function_Mass_Difference_OF_S[i][j]->Write();
       Function_Mass_Difference_OF_B[i][j]->Write();
       Function_Mass_Difference_OF[i][j]->Write();
     }
   }

   // Correlation in MC sample
   h_Delta_Time_SF->Write();
   h_Delta_Time_OF->Write();
   h_Correlation_All->Write();
   for(int i=0;i<5;i++){
     h_Time_Difference_SF[i]->Write();
     h_Time_Difference_OF[i]->Write();
     h_Correlation[i]->Write();
   }
   h_Significance->Write();

   c_Delta_Mass_SF_SB->Write();
   c_Delta_Mass_SF->Write();
   c_Delta_Time_SF->Write();
   c_Delta_Mass_OF_SB->Write();
   c_Delta_Mass_OF->Write();
   c_Delta_Time_OF->Write();
   c_Correlation_All->Write();

   for(int i=0;i<5;i++){
     c_Mass_Difference_SF_SB[i]->Write();
     c_Mass_Difference_SF[i]->Write();
     c_Mass_Difference_OF_SB[i]->Write();
     c_Mass_Difference_OF[i]->Write();
   }

   c_Time_Difference_SF->Write();
   c_Time_Difference_OF->Write();
   c_Correlation->Write();
   c_Significance->Write();

   file->Close();


   c_Delta_Mass_SF_SB->Print(outname4, "Title:Delta Mass Same Flavor (SB)");
   c_Delta_Mass_SF->Print(outname3, "Title:Delta Mass Same Flavor");
   c_Delta_Time_SF->Print(outname3, "Title:Delta Time Same Flavor");

   c_Delta_Mass_OF_SB->Print(outname3, "Title:Delta Mass Opposite Flavor (SB)");
   c_Delta_Mass_OF->Print(outname3, "Title:Delta Mass Opposite Flavor");
   c_Delta_Time_OF->Print(outname3, "Title:Delta Time Opposite Flavor");

   c_Correlation_All->Print(outname3, "Title:Correlation like Belle");

   /*
   for(int i=0;i<5;i++){
     if(i==0){
       std::stringstream ss1;
       ss1<<"Title:Mass Difference of Opposite Flavor (SB) Bin"<<i+1;
       c_Mass_Difference_OF_SB[i]->Print(outname4, ss1.str().c_str());
     } else {
       std::stringstream ss1;
       ss1<<"Title:Mass Difference of Opposite Flavor (SB) Bin"<<i+1;
       c_Mass_Difference_OF_SB[i]->Print(outname3, ss1.str().c_str());
     }
     std::stringstream ss2;
     ss2<<"Title:Mass Difference of Opposite Flavor Bin"<<i+1;
     c_Mass_Difference_OF[i]->Print(outname3, ss2.str().c_str());
   }
   c_Time_Difference_OF->Print(outname3, "Title:Time Difference Opposite Flavor");

   for(int i=0;i<5;i++){
     std::stringstream ss1;
     ss1<<"Title:Mass Difference of Same Flavor (SB) Bin"<<i+1;
     c_Mass_Difference_SF_SB[i]->Print(outname3, ss1.str().c_str());
     std::stringstream ss2;
     ss2<<"Title:Mass Difference of Same Flavor Bin"<<i+1;
     c_Mass_Difference_SF[i]->Print(outname3, ss2.str().c_str());
   }
   c_Time_Difference_SF->Print(outname3, "Title:Time Difference Same Flavor");

   c_Correlation->Print(outname3, "Title:Correlation");
   */

   c_Significance->Print(outname5, "Title:Significance");

}

