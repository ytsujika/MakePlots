//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 21 18:02:02 2023 by ROOT version 6.24/08
// from TTree B/B
// found on file: /home/ytsujika/AnalysisBphys/run/Rootfile/BPHY22-1.root
//////////////////////////////////////////////////////////

#ifndef BTreeAnalize_h
#define BTreeAnalize_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class BTreeAnalize {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

// Definition of histogram

   // Decay mode : B0 -> D* mu nu
   TH1F *h_B_mass_Dstar[7];
   TH1F *h_D0_mass[5];
   TH1F *h_Delta_Mass_S;
   TH1F *h_Delta_Mass_B;
   TH1F *h_Delta_Mass;
   TH1F *h_D0_Mass_Pass;
   TH1F *h_Dstar_Mass_Pass;
   TH1F *h_D0_pT_Pass;
   TH1F *h_Dstar_pT_Pass;
   TH1F *h_Delta_Mass_Chi2_S[6];
   TH1F *h_Delta_Mass_Chi2_B[6];
   TH1F *h_Delta_Mass_Chi2[6];
   TH1F *h_Mass_Difference_S[10];
   TH1F *h_Mass_Difference_B[10];
   TH1F *h_Mass_Difference[10];
   TH1F *h_DeltaM_wrt_DeltaT_S[20];
   TH1F *h_DeltaM_wrt_DeltaT_B[20];
   TH1F *h_DeltaM_wrt_DeltaT[20];
   // Decay mode : B0 -> D mu nu
   TH1F *h_B_mass_D[7];
   TH1F *h_B_mass_Chi2[5];
   TH1F *h_D_mass_Select_S[6];
   TH1F *h_D_mass_Select_B[6];
   TH1F *h_D_mass_Select[6];
   TH1F *h_D_Mass_S;
   TH1F *h_D_Mass_B;
   TH1F *h_D_Mass;
   TH1F *h_D_Mass_wrt_Time_S[10];
   TH1F *h_D_Mass_wrt_Time_B[10];
   TH1F *h_D_Mass_wrt_Time[10];

   // Correlation in MC sample
   TH1F *h_Delta_Time_SF;
   TH1F *h_Delta_Time_OF;
   TH2F *h_B_Flavor;
   TH2F *h_B_Flavor_E;
   TH2F *h_B_Flavor_wrt_Time[15];
   TH2F *h_B_Flavor_E_wrt_Time[15];
   TH1F *h_Time_Difference_SF[15];
   TH1F *h_Time_Difference_OF[15];
   TH1F *h_Correlation;
   TH1F *h_Correlation_wrt_Time[15];
   TH1F *h_Significance;

   // Declaration of leaf types
   Int_t           event_num;
   Int_t           jet_num;
   vector<bool>    *isMatch;
   vector<bool>    *isPassed;
   vector<bool>    *isPassedTight;
   Int_t           TriggerPass;
   Int_t           TriggerType;
   Int_t           n_B;
   Int_t           n_B_Pass;
   Int_t           n_B_Match;
   Int_t           n_B_Match_Pass;
   Int_t           n_B_Truth;
   Int_t           n_Dstar;
   Int_t           n_Dstar_Pass;
   Int_t           n_Dstar_Pass_Tight;
   Int_t           n_Dstar_Match;
   Int_t           n_Dstar_Match_Pass;
   Int_t           n_Dstar_Truth;
   Int_t           n_D;
   Int_t           n_D_Pass;
   Int_t           n_D_Pass_Tight;
   Int_t           n_D_Match;
   Int_t           n_D_Match_Pass;
   Int_t           n_D_Truth;
   vector<int>     *DecayType;
   vector<int>     *B_flavor;
   vector<float>   *B_nPart;
   vector<float>   *B_mass;
   vector<float>   *B_mass_err;
   vector<float>   *B_eta;
   vector<float>   *B_phi;
   vector<float>   *B_pT;
   vector<float>   *B_chi2;
   vector<float>   *B_nDoF;
   vector<float>   *B_x;
   vector<float>   *B_y;
   vector<float>   *B_z;
   vector<float>   *MuPiPiK_mass;
   vector<float>   *MuPiPiK_pT;
   vector<float>   *MuPiPiK_eta;
   vector<float>   *MuPiPiK_phi;
   vector<float>   *PiD0_mass;
   vector<float>   *D0Pi_mass;
   vector<float>   *D0Pi_pT;
   vector<float>   *D0Pi_eta;
   vector<float>   *D0Pi_phi;
   vector<float>   *PV_x;
   vector<float>   *PV_y;
   vector<float>   *PV_z;
   vector<float>   *RPV_x;
   vector<float>   *RPV_y;
   vector<float>   *RPV_z;
   vector<float>   *LxyMaxSumPt2;
   vector<float>   *LxyErrMaxSumPt2;
   vector<float>   *A0MaxSumPt2;
   vector<float>   *A0ErrMaxSumPt2;
   vector<float>   *A0xyMaxSumPt2;
   vector<float>   *A0xyErrMaxSumPt2;
   vector<float>   *Z0MaxSumPt2;
   vector<float>   *Z0ErrMaxSumPt2;
   vector<float>   *LxyMinA0;
   vector<float>   *LxyErrMinA0;
   vector<float>   *A0MinA0;
   vector<float>   *A0ErrMinA0;
   vector<float>   *A0xyMinA0;
   vector<float>   *A0xyErrMinA0;
   vector<float>   *Z0MinA0;
   vector<float>   *Z0ErrMinA0;
   vector<float>   *LxyMinZ0;
   vector<float>   *LxyErrMinZ0;
   vector<float>   *A0MinZ0;
   vector<float>   *A0ErrMinZ0;
   vector<float>   *A0xyMinZ0;
   vector<float>   *A0xyErrMinZ0;
   vector<float>   *Z0MinZ0;
   vector<float>   *Z0ErrMinZ0;
   vector<float>   *TauInvMassPVMaxSumPt2;
   vector<float>   *TauErrInvMassPVMaxSumPt2;
   vector<float>   *TauConstMassPVMaxSumPt2;
   vector<float>   *TauErrConstMassPVMaxSumPt2;
   vector<float>   *TauInvMassPVMinA0;
   vector<float>   *TauErrInvMassPVMinA0;
   vector<float>   *TauConstMassPVMinA0;
   vector<float>   *TauErrConstMassPVMinA0;
   vector<float>   *TauInvMassPVMinZ0;
   vector<float>   *TauErrInvMassPVMinZ0;
   vector<float>   *TauConstMassPVMinZ0;
   vector<float>   *TauErrConstMassPVMinZ0;
   vector<float>   *TauInvMassPVMinZ0BA;
   vector<float>   *TauErrInvMassPVMinZ0BA;
   vector<float>   *TauConstMassPVMinZ0BA;
   vector<float>   *TauErrConstMassPVMinZ0BA;
   vector<int>     *n_MuPi;
   vector<float>   *MuPi_mass;
   vector<float>   *MuPi_pT;
   vector<float>   *MuPi_eta;
   vector<float>   *MuPi_phi;
   vector<float>   *MuPi_chi2;
   vector<float>   *MuPi_nDoF;
   vector<float>   *MuPiAft_mass;
   vector<float>   *MuPiLxyMaxSumPt2;
   vector<double>  *Pi_s_mass;
   vector<double>  *Pi_s_pT;
   vector<double>  *Pi_s_charge;
   vector<double>  *Pi_s_eta;
   vector<double>  *Pi_s_phi;
   vector<double>  *Pi_s_theta_star;
   vector<float>   *Pi_s_d0;
   vector<float>   *Pi_s_z0;
   vector<float>   *Pi_s_qOverP;
   vector<int>     *n_Mu;
   vector<int>     *Mu_quality;
   vector<double>  *Mu_mass;
   vector<double>  *Mu_pT;
   vector<double>  *Mu_charge;
   vector<double>  *Mu_eta;
   vector<double>  *Mu_phi;
   vector<float>   *Mu_d0;
   vector<float>   *Mu_z0;
   vector<float>   *Mu_qOverP;
   vector<float>   *Mu_chi2;
   vector<float>   *Mu_nDoF;
   vector<float>   *Mu_Chi2_B;
   vector<float>   *Mu_nDoF_B;
   vector<float>   *D0_mass;
   vector<float>   *D0_mass_err;
   vector<float>   *D0_pT;
   vector<float>   *D0_pT_err;
   vector<float>   *D0_Lxy;
   vector<float>   *D0_Lxy_err;
   vector<float>   *D0_Tau;
   vector<float>   *D0_Tau_err;
   vector<float>   *D0_chi2;
   vector<float>   *D0_nDoF;
   vector<bool>    *D0_flag;
   vector<float>   *KPi_mass;
   vector<float>   *KPi_pT;
   vector<float>   *KPi_eta;
   vector<float>   *KPi_phi;
   vector<double>  *Pi_mass;
   vector<double>  *Pi_pT;
   vector<double>  *Pi_charge;
   vector<double>  *Pi_eta;
   vector<double>  *Pi_phi;
   vector<double>  *Pi_theta_star;
   vector<float>   *Pi_d0;
   vector<float>   *Pi_z0;
   vector<float>   *Pi_qOverP;
   vector<double>  *K_mass;
   vector<double>  *K_pT;
   vector<double>  *K_charge;
   vector<double>  *K_eta;
   vector<double>  *K_phi;
   vector<double>  *K_theta_star;
   vector<float>   *K_d0;
   vector<float>   *K_z0;
   vector<float>   *K_qOverP;
   vector<double>  *Pi_s_truth_dr;
   vector<double>  *Pi_s_truth_eta;
   vector<double>  *Pi_s_truth_phi;
   vector<double>  *Pi_s_truth_pT;
   vector<double>  *Pi_s_truth_m;
   vector<double>  *Pi_s_truth_charge;
   vector<int>     *Pi_s_truth_id;
   vector<vector<double> > *Pi_s_truth_parents_eta;
   vector<vector<double> > *Pi_s_truth_parents_phi;
   vector<vector<double> > *Pi_s_truth_parents_pT;
   vector<vector<double> > *Pi_s_truth_parents_m;
   vector<vector<double> > *Pi_s_truth_parents_charge;
   vector<vector<int> > *Pi_s_truth_parents_id;
   vector<vector<double> > *Pi_s_truth_siblings_eta;
   vector<vector<double> > *Pi_s_truth_siblings_phi;
   vector<vector<double> > *Pi_s_truth_siblings_pT;
   vector<vector<double> > *Pi_s_truth_siblings_m;
   vector<vector<double> > *Pi_s_truth_siblings_charge;
   vector<vector<int> > *Pi_s_truth_siblings_id;
   vector<double>  *Pi_truth_dr;
   vector<double>  *Pi_truth_eta;
   vector<double>  *Pi_truth_phi;
   vector<double>  *Pi_truth_pT;
   vector<double>  *Pi_truth_m;
   vector<double>  *Pi_truth_charge;
   vector<int>     *Pi_truth_id;
   vector<vector<double> > *Pi_truth_parents_eta;
   vector<vector<double> > *Pi_truth_parents_phi;
   vector<vector<double> > *Pi_truth_parents_pT;
   vector<vector<double> > *Pi_truth_parents_m;
   vector<vector<double> > *Pi_truth_parents_charge;
   vector<vector<int> > *Pi_truth_parents_id;
   vector<vector<double> > *Pi_truth_siblings_eta;
   vector<vector<double> > *Pi_truth_siblings_phi;
   vector<vector<double> > *Pi_truth_siblings_pT;
   vector<vector<double> > *Pi_truth_siblings_m;
   vector<vector<double> > *Pi_truth_siblings_charge;
   vector<vector<int> > *Pi_truth_siblings_id;
   vector<double>  *K_truth_dr;
   vector<double>  *K_truth_eta;
   vector<double>  *K_truth_phi;
   vector<double>  *K_truth_pT;
   vector<double>  *K_truth_m;
   vector<double>  *K_truth_charge;
   vector<int>     *K_truth_id;
   vector<vector<double> > *K_truth_parents_eta;
   vector<vector<double> > *K_truth_parents_phi;
   vector<vector<double> > *K_truth_parents_pT;
   vector<vector<double> > *K_truth_parents_m;
   vector<vector<double> > *K_truth_parents_charge;
   vector<vector<int> > *K_truth_parents_id;
   vector<vector<double> > *K_truth_siblings_eta;
   vector<vector<double> > *K_truth_siblings_phi;
   vector<vector<double> > *K_truth_siblings_pT;
   vector<vector<double> > *K_truth_siblings_m;
   vector<vector<double> > *K_truth_siblings_charge;
   vector<vector<int> > *K_truth_siblings_id;

   // List of branches
   TBranch        *b_event_num;   //!
   TBranch        *b_jet_num;   //!
   TBranch        *b_isMatch;   //!
   TBranch        *b_isPassed;   //!
   TBranch        *b_isPassedTight;   //!
   TBranch        *b_TriggerPass;   //!
   TBranch        *b_TriggerType;   //!
   TBranch        *b_n_B;   //!
   TBranch        *b_n_B_Pass;   //!
   TBranch        *b_n_B_Match;   //!
   TBranch        *b_n_B_Match_Pass;   //!
   TBranch        *b_n_B_Truth;   //!
   TBranch        *b_n_Dstar;   //!
   TBranch        *b_n_Dstar_Pass;   //!
   TBranch        *b_n_Dstar_Pass_Tight;   //!
   TBranch        *b_n_Dstar_Match;   //!
   TBranch        *b_n_Dstar_Match_Pass;   //!
   TBranch        *b_n_Dstar_Truth;   //!
   TBranch        *b_n_D;   //!
   TBranch        *b_n_D_Pass;   //!
   TBranch        *b_n_D_Pass_Tight;   //!
   TBranch        *b_n_D_Match;   //!
   TBranch        *b_n_D_Match_Pass;   //!
   TBranch        *b_n_D_Truth;   //!
   TBranch        *b_DecayType;   //!
   TBranch        *b_B_flavor;   //!
   TBranch        *b_B_nPart;   //!
   TBranch        *b_B_mass;   //!
   TBranch        *b_B_mass_err;   //!
   TBranch        *b_B_eta;   //!
   TBranch        *b_B_phi;   //!
   TBranch        *b_B_pT;   //!
   TBranch        *b_B_chi2;   //!
   TBranch        *b_B_nDoF;   //!
   TBranch        *b_B_x;   //!
   TBranch        *b_B_y;   //!
   TBranch        *b_B_z;   //!
   TBranch        *b_MuPiPiK_mass;   //!
   TBranch        *b_MuPiPiK_pT;   //!
   TBranch        *b_MuPiPiK_eta;   //!
   TBranch        *b_MuPiPiK_phi;   //!
   TBranch        *b_PiD0_mass;   //!
   TBranch        *b_D0Pi_mass;   //!
   TBranch        *b_D0Pi_pT;   //!
   TBranch        *b_D0Pi_eta;   //!
   TBranch        *b_D0Pi_phi;   //!
   TBranch        *b_PV_x;   //!
   TBranch        *b_PV_y;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_RPV_x;   //!
   TBranch        *b_RPV_y;   //!
   TBranch        *b_RPV_z;   //!
   TBranch        *b_LxyMaxSumPt2;   //!
   TBranch        *b_LxyErrMaxSumPt2;   //!
   TBranch        *b_A0MaxSumPt2;   //!
   TBranch        *b_A0ErrMaxSumPt2;   //!
   TBranch        *b_A0xyMaxSumPt2;   //!
   TBranch        *b_A0xyErrMaxSumPt2;   //!
   TBranch        *b_Z0MaxSumPt2;   //!
   TBranch        *b_Z0ErrMaxSumPt2;   //!
   TBranch        *b_LxyMinA0;   //!
   TBranch        *b_LxyErrMinA0;   //!
   TBranch        *b_A0MinA0;   //!
   TBranch        *b_A0ErrMinA0;   //!
   TBranch        *b_A0xyMinA0;   //!
   TBranch        *b_A0xyErrMinA0;   //!
   TBranch        *b_Z0MinA0;   //!
   TBranch        *b_Z0ErrMinA0;   //!
   TBranch        *b_LxyMinZ0;   //!
   TBranch        *b_LxyErrMinZ0;   //!
   TBranch        *b_A0MinZ0;   //!
   TBranch        *b_A0ErrMinZ0;   //!
   TBranch        *b_A0xyMinZ0;   //!
   TBranch        *b_A0xyErrMinZ0;   //!
   TBranch        *b_Z0MinZ0;   //!
   TBranch        *b_Z0ErrMinZ0;   //!
   TBranch        *b_TauInvMassPVMaxSumPt2;   //!
   TBranch        *b_TauErrInvMassPVMaxSumPt2;   //!
   TBranch        *b_TauConstMassPVMaxSumPt2;   //!
   TBranch        *b_TauErrConstMassPVMaxSumPt2;   //!
   TBranch        *b_TauInvMassPVMinA0;   //!
   TBranch        *b_TauErrInvMassPVMinA0;   //!
   TBranch        *b_TauConstMassPVMinA0;   //!
   TBranch        *b_TauErrConstMassPVMinA0;   //!
   TBranch        *b_TauInvMassPVMinZ0;   //!
   TBranch        *b_TauErrInvMassPVMinZ0;   //!
   TBranch        *b_TauConstMassPVMinZ0;   //!
   TBranch        *b_TauErrConstMassPVMinZ0;   //!
   TBranch        *b_TauInvMassPVMinZ0BA;   //!
   TBranch        *b_TauErrInvMassPVMinZ0BA;   //!
   TBranch        *b_TauConstMassPVMinZ0BA;   //!
   TBranch        *b_TauErrConstMassPVMinZ0BA;   //!
   TBranch        *b_n_MuPi;   //!
   TBranch        *b_MuPi_mass;   //!
   TBranch        *b_MuPi_pT;   //!
   TBranch        *b_MuPi_eta;   //!
   TBranch        *b_MuPi_phi;   //!
   TBranch        *b_MuPi_chi2;   //!
   TBranch        *b_MuPi_nDoF;   //!
   TBranch        *b_MuPiAft_mass;   //!
   TBranch        *b_MuPiLxyMaxSumPt2;   //!
   TBranch        *b_Pi_s_mass;   //!
   TBranch        *b_Pi_s_pT;   //!
   TBranch        *b_Pi_s_charge;   //!
   TBranch        *b_Pi_s_eta;   //!
   TBranch        *b_Pi_s_phi;   //!
   TBranch        *b_Pi_s_theta_star;   //!
   TBranch        *b_Pi_s_d0;   //!
   TBranch        *b_Pi_s_z0;   //!
   TBranch        *b_Pi_s_qOverP;   //!
   TBranch        *b_n_Mu;   //!
   TBranch        *b_Mu_quality;   //!
   TBranch        *b_Mu_mass;   //!
   TBranch        *b_Mu_pT;   //!
   TBranch        *b_Mu_charge;   //!
   TBranch        *b_Mu_eta;   //!
   TBranch        *b_Mu_phi;   //!
   TBranch        *b_Mu_d0;   //!
   TBranch        *b_Mu_z0;   //!
   TBranch        *b_Mu_qOverP;   //!
   TBranch        *b_Mu_chi2;   //!
   TBranch        *b_Mu_nDoF;   //!
   TBranch        *b_Mu_Chi2_B;   //!
   TBranch        *b_Mu_nDoF_B;   //!
   TBranch        *b_D0_mass;   //!
   TBranch        *b_D0_mass_err;   //!
   TBranch        *b_D0_pT;   //!
   TBranch        *b_D0_pT_err;   //!
   TBranch        *b_D0_Lxy;   //!
   TBranch        *b_D0_Lxy_err;   //!
   TBranch        *b_D0_Tau;   //!
   TBranch        *b_D0_Tau_err;   //!
   TBranch        *b_D0_chi2;   //!
   TBranch        *b_D0_nDoF;   //!
   TBranch        *b_D0_flag;   //!
   TBranch        *b_KPi_mass;   //!
   TBranch        *b_KPi_pT;   //!
   TBranch        *b_KPi_eta;   //!
   TBranch        *b_KPi_phi;   //!
   TBranch        *b_Pi_mass;   //!
   TBranch        *b_Pi_pT;   //!
   TBranch        *b_Pi_charge;   //!
   TBranch        *b_Pi_eta;   //!
   TBranch        *b_Pi_phi;   //!
   TBranch        *b_Pi_theta_star;   //!
   TBranch        *b_Pi_d0;   //!
   TBranch        *b_Pi_z0;   //!
   TBranch        *b_Pi_qOverP;   //!
   TBranch        *b_K_mass;   //!
   TBranch        *b_K_pT;   //!
   TBranch        *b_K_charge;   //!
   TBranch        *b_K_eta;   //!
   TBranch        *b_K_phi;   //!
   TBranch        *b_K_theta_star;   //!
   TBranch        *b_K_d0;   //!
   TBranch        *b_K_z0;   //!
   TBranch        *b_K_qOverP;   //!
   TBranch        *b_Pi_s_truth_dr;   //!
   TBranch        *b_Pi_s_truth_eta;   //!
   TBranch        *b_Pi_s_truth_phi;   //!
   TBranch        *b_Pi_s_truth_pT;   //!
   TBranch        *b_Pi_s_truth_m;   //!
   TBranch        *b_Pi_s_truth_charge;   //!
   TBranch        *b_Pi_s_truth_id;   //!
   TBranch        *b_Pi_s_truth_parents_eta;   //!
   TBranch        *b_Pi_s_truth_parents_phi;   //!
   TBranch        *b_Pi_s_truth_parents_pT;   //!
   TBranch        *b_Pi_s_truth_parents_m;   //!
   TBranch        *b_Pi_s_truth_parents_charge;   //!
   TBranch        *b_Pi_s_truth_parents_id;   //!
   TBranch        *b_Pi_s_truth_siblings_eta;   //!
   TBranch        *b_Pi_s_truth_siblings_phi;   //!
   TBranch        *b_Pi_s_truth_siblings_pT;   //!
   TBranch        *b_Pi_s_truth_siblings_m;   //!
   TBranch        *b_Pi_s_truth_siblings_charge;   //!
   TBranch        *b_Pi_s_truth_siblings_id;   //!
   TBranch        *b_Pi_truth_dr;   //!
   TBranch        *b_Pi_truth_eta;   //!
   TBranch        *b_Pi_truth_phi;   //!
   TBranch        *b_Pi_truth_pT;   //!
   TBranch        *b_Pi_truth_m;   //!
   TBranch        *b_Pi_truth_charge;   //!
   TBranch        *b_Pi_truth_id;   //!
   TBranch        *b_Pi_truth_parents_eta;   //!
   TBranch        *b_Pi_truth_parents_phi;   //!
   TBranch        *b_Pi_truth_parents_pT;   //!
   TBranch        *b_Pi_truth_parents_m;   //!
   TBranch        *b_Pi_truth_parents_charge;   //!
   TBranch        *b_Pi_truth_parents_id;   //!
   TBranch        *b_Pi_truth_siblings_eta;   //!
   TBranch        *b_Pi_truth_siblings_phi;   //!
   TBranch        *b_Pi_truth_siblings_pT;   //!
   TBranch        *b_Pi_truth_siblings_m;   //!
   TBranch        *b_Pi_truth_siblings_charge;   //!
   TBranch        *b_Pi_truth_siblings_id;   //!
   TBranch        *b_K_truth_dr;   //!
   TBranch        *b_K_truth_eta;   //!
   TBranch        *b_K_truth_phi;   //!
   TBranch        *b_K_truth_pT;   //!
   TBranch        *b_K_truth_m;   //!
   TBranch        *b_K_truth_charge;   //!
   TBranch        *b_K_truth_id;   //!
   TBranch        *b_K_truth_parents_eta;   //!
   TBranch        *b_K_truth_parents_phi;   //!
   TBranch        *b_K_truth_parents_pT;   //!
   TBranch        *b_K_truth_parents_m;   //!
   TBranch        *b_K_truth_parents_charge;   //!
   TBranch        *b_K_truth_parents_id;   //!
   TBranch        *b_K_truth_siblings_eta;   //!
   TBranch        *b_K_truth_siblings_phi;   //!
   TBranch        *b_K_truth_siblings_pT;   //!
   TBranch        *b_K_truth_siblings_m;   //!
   TBranch        *b_K_truth_siblings_charge;   //!
   TBranch        *b_K_truth_siblings_id;   //!

   BTreeAnalize(const int& isMC, const string& infname, const string& outfname, TTree *tree=0);
   virtual ~BTreeAnalize();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(const int& isMC, const string& outfname);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual Double_t FitFunction(double x, double par0, double par1, double par2);
};

#endif

#ifdef BTreeAnalize_cxx
BTreeAnalize::BTreeAnalize(const int& isMC, const string& infname, const string& outfname, TTree *tree) : fChain(0) 
{
   string infile(infname);
  if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(infile.c_str());
      if (!f || !f->IsOpen()) {
         f = new TFile(infile.c_str());
      }
      f->GetObject("B",tree);

   }
   Init(tree);
   Loop(isMC, outfname);

   /*
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/home/ytsujika/AnalysisBphys/run/Rootfile/BPHY22-0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/home/ytsujika/AnalysisBphys/run/Rootfile/BPHY22-0.root");
      }
      f->GetObject("B",tree);

   }
   Init(tree);
   */

}

BTreeAnalize::~BTreeAnalize()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t BTreeAnalize::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t BTreeAnalize::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void BTreeAnalize::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   isMatch = 0;
   isPassed = 0;
   isPassedTight = 0;
   DecayType = 0;
   B_flavor = 0;
   B_nPart = 0;
   B_mass = 0;
   B_mass_err = 0;
   B_eta = 0;
   B_phi = 0;
   B_pT = 0;
   B_chi2 = 0;
   B_nDoF = 0;
   B_x = 0;
   B_y = 0;
   B_z = 0;
   MuPiPiK_mass = 0;
   MuPiPiK_pT = 0;
   MuPiPiK_eta = 0;
   MuPiPiK_phi = 0;
   PiD0_mass = 0;
   D0Pi_mass = 0;
   D0Pi_pT = 0;
   D0Pi_eta = 0;
   D0Pi_phi = 0;
   PV_x = 0;
   PV_y = 0;
   PV_z = 0;
   RPV_x = 0;
   RPV_y = 0;
   RPV_z = 0;
   LxyMaxSumPt2 = 0;
   LxyErrMaxSumPt2 = 0;
   A0MaxSumPt2 = 0;
   A0ErrMaxSumPt2 = 0;
   A0xyMaxSumPt2 = 0;
   A0xyErrMaxSumPt2 = 0;
   Z0MaxSumPt2 = 0;
   Z0ErrMaxSumPt2 = 0;
   LxyMinA0 = 0;
   LxyErrMinA0 = 0;
   A0MinA0 = 0;
   A0ErrMinA0 = 0;
   A0xyMinA0 = 0;
   A0xyErrMinA0 = 0;
   Z0MinA0 = 0;
   Z0ErrMinA0 = 0;
   LxyMinZ0 = 0;
   LxyErrMinZ0 = 0;
   A0MinZ0 = 0;
   A0ErrMinZ0 = 0;
   A0xyMinZ0 = 0;
   A0xyErrMinZ0 = 0;
   Z0MinZ0 = 0;
   Z0ErrMinZ0 = 0;
   TauInvMassPVMaxSumPt2 = 0;
   TauErrInvMassPVMaxSumPt2 = 0;
   TauConstMassPVMaxSumPt2 = 0;
   TauErrConstMassPVMaxSumPt2 = 0;
   TauInvMassPVMinA0 = 0;
   TauErrInvMassPVMinA0 = 0;
   TauConstMassPVMinA0 = 0;
   TauErrConstMassPVMinA0 = 0;
   TauInvMassPVMinZ0 = 0;
   TauErrInvMassPVMinZ0 = 0;
   TauConstMassPVMinZ0 = 0;
   TauErrConstMassPVMinZ0 = 0;
   TauInvMassPVMinZ0BA = 0;
   TauErrInvMassPVMinZ0BA = 0;
   TauConstMassPVMinZ0BA = 0;
   TauErrConstMassPVMinZ0BA = 0;
   n_MuPi = 0;
   MuPi_mass = 0;
   MuPi_pT = 0;
   MuPi_eta = 0;
   MuPi_phi = 0;
   MuPi_chi2 = 0;
   MuPi_nDoF = 0;
   MuPiAft_mass = 0;
   MuPiLxyMaxSumPt2 = 0;
   Pi_s_mass = 0;
   Pi_s_pT = 0;
   Pi_s_charge = 0;
   Pi_s_eta = 0;
   Pi_s_phi = 0;
   Pi_s_theta_star = 0;
   Pi_s_d0 = 0;
   Pi_s_z0 = 0;
   Pi_s_qOverP = 0;
   n_Mu = 0;
   Mu_quality = 0;
   Mu_mass = 0;
   Mu_pT = 0;
   Mu_charge = 0;
   Mu_eta = 0;
   Mu_phi = 0;
   Mu_d0 = 0;
   Mu_z0 = 0;
   Mu_qOverP = 0;
   Mu_chi2 = 0;
   Mu_nDoF = 0;
   Mu_Chi2_B = 0;
   Mu_nDoF_B = 0;
   D0_mass = 0;
   D0_mass_err = 0;
   D0_pT = 0;
   D0_pT_err = 0;
   D0_Lxy = 0;
   D0_Lxy_err = 0;
   D0_Tau = 0;
   D0_Tau_err = 0;
   D0_chi2 = 0;
   D0_nDoF = 0;
   D0_flag = 0;
   KPi_mass = 0;
   KPi_pT = 0;
   KPi_eta = 0;
   KPi_phi = 0;
   Pi_mass = 0;
   Pi_pT = 0;
   Pi_charge = 0;
   Pi_eta = 0;
   Pi_phi = 0;
   Pi_theta_star = 0;
   Pi_d0 = 0;
   Pi_z0 = 0;
   Pi_qOverP = 0;
   K_mass = 0;
   K_pT = 0;
   K_charge = 0;
   K_eta = 0;
   K_phi = 0;
   K_theta_star = 0;
   K_d0 = 0;
   K_z0 = 0;
   K_qOverP = 0;
   Pi_s_truth_dr = 0;
   Pi_s_truth_eta = 0;
   Pi_s_truth_phi = 0;
   Pi_s_truth_pT = 0;
   Pi_s_truth_m = 0;
   Pi_s_truth_charge = 0;
   Pi_s_truth_id = 0;
   Pi_s_truth_parents_eta = 0;
   Pi_s_truth_parents_phi = 0;
   Pi_s_truth_parents_pT = 0;
   Pi_s_truth_parents_m = 0;
   Pi_s_truth_parents_charge = 0;
   Pi_s_truth_parents_id = 0;
   Pi_s_truth_siblings_eta = 0;
   Pi_s_truth_siblings_phi = 0;
   Pi_s_truth_siblings_pT = 0;
   Pi_s_truth_siblings_m = 0;
   Pi_s_truth_siblings_charge = 0;
   Pi_s_truth_siblings_id = 0;
   Pi_truth_dr = 0;
   Pi_truth_eta = 0;
   Pi_truth_phi = 0;
   Pi_truth_pT = 0;
   Pi_truth_m = 0;
   Pi_truth_charge = 0;
   Pi_truth_id = 0;
   Pi_truth_parents_eta = 0;
   Pi_truth_parents_phi = 0;
   Pi_truth_parents_pT = 0;
   Pi_truth_parents_m = 0;
   Pi_truth_parents_charge = 0;
   Pi_truth_parents_id = 0;
   Pi_truth_siblings_eta = 0;
   Pi_truth_siblings_phi = 0;
   Pi_truth_siblings_pT = 0;
   Pi_truth_siblings_m = 0;
   Pi_truth_siblings_charge = 0;
   Pi_truth_siblings_id = 0;
   K_truth_dr = 0;
   K_truth_eta = 0;
   K_truth_phi = 0;
   K_truth_pT = 0;
   K_truth_m = 0;
   K_truth_charge = 0;
   K_truth_id = 0;
   K_truth_parents_eta = 0;
   K_truth_parents_phi = 0;
   K_truth_parents_pT = 0;
   K_truth_parents_m = 0;
   K_truth_parents_charge = 0;
   K_truth_parents_id = 0;
   K_truth_siblings_eta = 0;
   K_truth_siblings_phi = 0;
   K_truth_siblings_pT = 0;
   K_truth_siblings_m = 0;
   K_truth_siblings_charge = 0;
   K_truth_siblings_id = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event_num", &event_num, &b_event_num);
   fChain->SetBranchAddress("jet_num", &jet_num, &b_jet_num);
   fChain->SetBranchAddress("isMatch", &isMatch, &b_isMatch);
   fChain->SetBranchAddress("isPassed", &isPassed, &b_isPassed);
   fChain->SetBranchAddress("isPassedTight", &isPassedTight, &b_isPassedTight);
   fChain->SetBranchAddress("TriggerPass", &TriggerPass, &b_TriggerPass);
   fChain->SetBranchAddress("TriggerType", &TriggerType, &b_TriggerType);
   fChain->SetBranchAddress("n_B", &n_B, &b_n_B);
   fChain->SetBranchAddress("n_B_Pass", &n_B_Pass, &b_n_B_Pass);
   fChain->SetBranchAddress("n_B_Match", &n_B_Match, &b_n_B_Match);
   fChain->SetBranchAddress("n_B_Match_Pass", &n_B_Match_Pass, &b_n_B_Match_Pass);
   fChain->SetBranchAddress("n_B_Truth", &n_B_Truth, &b_n_B_Truth);
   fChain->SetBranchAddress("n_Dstar", &n_Dstar, &b_n_Dstar);
   fChain->SetBranchAddress("n_Dstar_Pass", &n_Dstar_Pass, &b_n_Dstar_Pass);
   fChain->SetBranchAddress("n_Dstar_Pass_Tight", &n_Dstar_Pass_Tight, &b_n_Dstar_Pass_Tight);
   fChain->SetBranchAddress("n_Dstar_Match", &n_Dstar_Match, &b_n_Dstar_Match);
   fChain->SetBranchAddress("n_Dstar_Match_Pass", &n_Dstar_Match_Pass, &b_n_Dstar_Match_Pass);
   fChain->SetBranchAddress("n_Dstar_Truth", &n_Dstar_Truth, &b_n_Dstar_Truth);
   fChain->SetBranchAddress("n_D", &n_D, &b_n_D);
   fChain->SetBranchAddress("n_D_Pass", &n_D_Pass, &b_n_D_Pass);
   fChain->SetBranchAddress("n_D_Pass_Tight", &n_D_Pass_Tight, &b_n_D_Pass_Tight);
   fChain->SetBranchAddress("n_D_Match", &n_D_Match, &b_n_D_Match);
   fChain->SetBranchAddress("n_D_Match_Pass", &n_D_Match_Pass, &b_n_D_Match_Pass);
   fChain->SetBranchAddress("n_D_Truth", &n_D_Truth, &b_n_D_Truth);
   fChain->SetBranchAddress("DecayType", &DecayType, &b_DecayType);
   fChain->SetBranchAddress("B_flavor", &B_flavor, &b_B_flavor);
   fChain->SetBranchAddress("B_nPart", &B_nPart, &b_B_nPart);
   fChain->SetBranchAddress("B_mass", &B_mass, &b_B_mass);
   fChain->SetBranchAddress("B_mass_err", &B_mass_err, &b_B_mass_err);
   fChain->SetBranchAddress("B_eta", &B_eta, &b_B_eta);
   fChain->SetBranchAddress("B_phi", &B_phi, &b_B_phi);
   fChain->SetBranchAddress("B_pT", &B_pT, &b_B_pT);
   fChain->SetBranchAddress("B_chi2", &B_chi2, &b_B_chi2);
   fChain->SetBranchAddress("B_nDoF", &B_nDoF, &b_B_nDoF);
   fChain->SetBranchAddress("B_x", &B_x, &b_B_x);
   fChain->SetBranchAddress("B_y", &B_y, &b_B_y);
   fChain->SetBranchAddress("B_z", &B_z, &b_B_z);
   fChain->SetBranchAddress("MuPiPiK_mass", &MuPiPiK_mass, &b_MuPiPiK_mass);
   fChain->SetBranchAddress("MuPiPiK_pT", &MuPiPiK_pT, &b_MuPiPiK_pT);
   fChain->SetBranchAddress("MuPiPiK_eta", &MuPiPiK_eta, &b_MuPiPiK_eta);
   fChain->SetBranchAddress("MuPiPiK_phi", &MuPiPiK_phi, &b_MuPiPiK_phi);
   fChain->SetBranchAddress("PiD0_mass", &PiD0_mass, &b_PiD0_mass);
   fChain->SetBranchAddress("D0Pi_mass", &D0Pi_mass, &b_D0Pi_mass);
   fChain->SetBranchAddress("D0Pi_pT", &D0Pi_pT, &b_D0Pi_pT);
   fChain->SetBranchAddress("D0Pi_eta", &D0Pi_eta, &b_D0Pi_eta);
   fChain->SetBranchAddress("D0Pi_phi", &D0Pi_phi, &b_D0Pi_phi);
   fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
   fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("RPV_x", &RPV_x, &b_RPV_x);
   fChain->SetBranchAddress("RPV_y", &RPV_y, &b_RPV_y);
   fChain->SetBranchAddress("RPV_z", &RPV_z, &b_RPV_z);
   fChain->SetBranchAddress("LxyMaxSumPt2", &LxyMaxSumPt2, &b_LxyMaxSumPt2);
   fChain->SetBranchAddress("LxyErrMaxSumPt2", &LxyErrMaxSumPt2, &b_LxyErrMaxSumPt2);
   fChain->SetBranchAddress("A0MaxSumPt2", &A0MaxSumPt2, &b_A0MaxSumPt2);
   fChain->SetBranchAddress("A0ErrMaxSumPt2", &A0ErrMaxSumPt2, &b_A0ErrMaxSumPt2);
   fChain->SetBranchAddress("A0xyMaxSumPt2", &A0xyMaxSumPt2, &b_A0xyMaxSumPt2);
   fChain->SetBranchAddress("A0xyErrMaxSumPt2", &A0xyErrMaxSumPt2, &b_A0xyErrMaxSumPt2);
   fChain->SetBranchAddress("Z0MaxSumPt2", &Z0MaxSumPt2, &b_Z0MaxSumPt2);
   fChain->SetBranchAddress("Z0ErrMaxSumPt2", &Z0ErrMaxSumPt2, &b_Z0ErrMaxSumPt2);
   fChain->SetBranchAddress("LxyMinA0", &LxyMinA0, &b_LxyMinA0);
   fChain->SetBranchAddress("LxyErrMinA0", &LxyErrMinA0, &b_LxyErrMinA0);
   fChain->SetBranchAddress("A0MinA0", &A0MinA0, &b_A0MinA0);
   fChain->SetBranchAddress("A0ErrMinA0", &A0ErrMinA0, &b_A0ErrMinA0);
   fChain->SetBranchAddress("A0xyMinA0", &A0xyMinA0, &b_A0xyMinA0);
   fChain->SetBranchAddress("A0xyErrMinA0", &A0xyErrMinA0, &b_A0xyErrMinA0);
   fChain->SetBranchAddress("Z0MinA0", &Z0MinA0, &b_Z0MinA0);
   fChain->SetBranchAddress("Z0ErrMinA0", &Z0ErrMinA0, &b_Z0ErrMinA0);
   fChain->SetBranchAddress("LxyMinZ0", &LxyMinZ0, &b_LxyMinZ0);
   fChain->SetBranchAddress("LxyErrMinZ0", &LxyErrMinZ0, &b_LxyErrMinZ0);
   fChain->SetBranchAddress("A0MinZ0", &A0MinZ0, &b_A0MinZ0);
   fChain->SetBranchAddress("A0ErrMinZ0", &A0ErrMinZ0, &b_A0ErrMinZ0);
   fChain->SetBranchAddress("A0xyMinZ0", &A0xyMinZ0, &b_A0xyMinZ0);
   fChain->SetBranchAddress("A0xyErrMinZ0", &A0xyErrMinZ0, &b_A0xyErrMinZ0);
   fChain->SetBranchAddress("Z0MinZ0", &Z0MinZ0, &b_Z0MinZ0);
   fChain->SetBranchAddress("Z0ErrMinZ0", &Z0ErrMinZ0, &b_Z0ErrMinZ0);
   fChain->SetBranchAddress("TauInvMassPVMaxSumPt2", &TauInvMassPVMaxSumPt2, &b_TauInvMassPVMaxSumPt2);
   fChain->SetBranchAddress("TauErrInvMassPVMaxSumPt2", &TauErrInvMassPVMaxSumPt2, &b_TauErrInvMassPVMaxSumPt2);
   fChain->SetBranchAddress("TauConstMassPVMaxSumPt2", &TauConstMassPVMaxSumPt2, &b_TauConstMassPVMaxSumPt2);
   fChain->SetBranchAddress("TauErrConstMassPVMaxSumPt2", &TauErrConstMassPVMaxSumPt2, &b_TauErrConstMassPVMaxSumPt2);
   fChain->SetBranchAddress("TauInvMassPVMinA0", &TauInvMassPVMinA0, &b_TauInvMassPVMinA0);
   fChain->SetBranchAddress("TauErrInvMassPVMinA0", &TauErrInvMassPVMinA0, &b_TauErrInvMassPVMinA0);
   fChain->SetBranchAddress("TauConstMassPVMinA0", &TauConstMassPVMinA0, &b_TauConstMassPVMinA0);
   fChain->SetBranchAddress("TauErrConstMassPVMinA0", &TauErrConstMassPVMinA0, &b_TauErrConstMassPVMinA0);
   fChain->SetBranchAddress("TauInvMassPVMinZ0", &TauInvMassPVMinZ0, &b_TauInvMassPVMinZ0);
   fChain->SetBranchAddress("TauErrInvMassPVMinZ0", &TauErrInvMassPVMinZ0, &b_TauErrInvMassPVMinZ0);
   fChain->SetBranchAddress("TauConstMassPVMinZ0", &TauConstMassPVMinZ0, &b_TauConstMassPVMinZ0);
   fChain->SetBranchAddress("TauErrConstMassPVMinZ0", &TauErrConstMassPVMinZ0, &b_TauErrConstMassPVMinZ0);
   fChain->SetBranchAddress("TauInvMassPVMinZ0BA", &TauInvMassPVMinZ0BA, &b_TauInvMassPVMinZ0BA);
   fChain->SetBranchAddress("TauErrInvMassPVMinZ0BA", &TauErrInvMassPVMinZ0BA, &b_TauErrInvMassPVMinZ0BA);
   fChain->SetBranchAddress("TauConstMassPVMinZ0BA", &TauConstMassPVMinZ0BA, &b_TauConstMassPVMinZ0BA);
   fChain->SetBranchAddress("TauErrConstMassPVMinZ0BA", &TauErrConstMassPVMinZ0BA, &b_TauErrConstMassPVMinZ0BA);
   fChain->SetBranchAddress("n_MuPi", &n_MuPi, &b_n_MuPi);
   fChain->SetBranchAddress("MuPi_mass", &MuPi_mass, &b_MuPi_mass);
   fChain->SetBranchAddress("MuPi_pT", &MuPi_pT, &b_MuPi_pT);
   fChain->SetBranchAddress("MuPi_eta", &MuPi_eta, &b_MuPi_eta);
   fChain->SetBranchAddress("MuPi_phi", &MuPi_phi, &b_MuPi_phi);
   fChain->SetBranchAddress("MuPi_chi2", &MuPi_chi2, &b_MuPi_chi2);
   fChain->SetBranchAddress("MuPi_nDoF", &MuPi_nDoF, &b_MuPi_nDoF);
   fChain->SetBranchAddress("MuPiAft_mass", &MuPiAft_mass, &b_MuPiAft_mass);
   fChain->SetBranchAddress("MuPiLxyMaxSumPt2", &MuPiLxyMaxSumPt2, &b_MuPiLxyMaxSumPt2);
   fChain->SetBranchAddress("Pi_s_mass", &Pi_s_mass, &b_Pi_s_mass);
   fChain->SetBranchAddress("Pi_s_pT", &Pi_s_pT, &b_Pi_s_pT);
   fChain->SetBranchAddress("Pi_s_charge", &Pi_s_charge, &b_Pi_s_charge);
   fChain->SetBranchAddress("Pi_s_eta", &Pi_s_eta, &b_Pi_s_eta);
   fChain->SetBranchAddress("Pi_s_phi", &Pi_s_phi, &b_Pi_s_phi);
   fChain->SetBranchAddress("Pi_s_theta_star", &Pi_s_theta_star, &b_Pi_s_theta_star);
   fChain->SetBranchAddress("Pi_s_d0", &Pi_s_d0, &b_Pi_s_d0);
   fChain->SetBranchAddress("Pi_s_z0", &Pi_s_z0, &b_Pi_s_z0);
   fChain->SetBranchAddress("Pi_s_qOverP", &Pi_s_qOverP, &b_Pi_s_qOverP);
   fChain->SetBranchAddress("n_Mu", &n_Mu, &b_n_Mu);
   fChain->SetBranchAddress("Mu_quality", &Mu_quality, &b_Mu_quality);
   fChain->SetBranchAddress("Mu_mass", &Mu_mass, &b_Mu_mass);
   fChain->SetBranchAddress("Mu_pT", &Mu_pT, &b_Mu_pT);
   fChain->SetBranchAddress("Mu_charge", &Mu_charge, &b_Mu_charge);
   fChain->SetBranchAddress("Mu_eta", &Mu_eta, &b_Mu_eta);
   fChain->SetBranchAddress("Mu_phi", &Mu_phi, &b_Mu_phi);
   fChain->SetBranchAddress("Mu_d0", &Mu_d0, &b_Mu_d0);
   fChain->SetBranchAddress("Mu_z0", &Mu_z0, &b_Mu_z0);
   fChain->SetBranchAddress("Mu_qOverP", &Mu_qOverP, &b_Mu_qOverP);
   fChain->SetBranchAddress("Mu_chi2", &Mu_chi2, &b_Mu_chi2);
   fChain->SetBranchAddress("Mu_nDoF", &Mu_nDoF, &b_Mu_nDoF);
   fChain->SetBranchAddress("Mu_Chi2_B", &Mu_Chi2_B, &b_Mu_Chi2_B);
   fChain->SetBranchAddress("Mu_nDoF_B", &Mu_nDoF_B, &b_Mu_nDoF_B);
   fChain->SetBranchAddress("D0_mass", &D0_mass, &b_D0_mass);
   fChain->SetBranchAddress("D0_mass_err", &D0_mass_err, &b_D0_mass_err);
   fChain->SetBranchAddress("D0_pT", &D0_pT, &b_D0_pT);
   fChain->SetBranchAddress("D0_pT_err", &D0_pT_err, &b_D0_pT_err);
   fChain->SetBranchAddress("D0_Lxy", &D0_Lxy, &b_D0_Lxy);
   fChain->SetBranchAddress("D0_Lxy_err", &D0_Lxy_err, &b_D0_Lxy_err);
   fChain->SetBranchAddress("D0_Tau", &D0_Tau, &b_D0_Tau);
   fChain->SetBranchAddress("D0_Tau_err", &D0_Tau_err, &b_D0_Tau_err);
   fChain->SetBranchAddress("D0_chi2", &D0_chi2, &b_D0_chi2);
   fChain->SetBranchAddress("D0_nDoF", &D0_nDoF, &b_D0_nDoF);
   fChain->SetBranchAddress("D0_flag", &D0_flag, &b_D0_flag);
   fChain->SetBranchAddress("KPi_mass", &KPi_mass, &b_KPi_mass);
   fChain->SetBranchAddress("KPi_pT", &KPi_pT, &b_KPi_pT);
   fChain->SetBranchAddress("KPi_eta", &KPi_eta, &b_KPi_eta);
   fChain->SetBranchAddress("KPi_phi", &KPi_phi, &b_KPi_phi);
   fChain->SetBranchAddress("Pi_mass", &Pi_mass, &b_Pi_mass);
   fChain->SetBranchAddress("Pi_pT", &Pi_pT, &b_Pi_pT);
   fChain->SetBranchAddress("Pi_charge", &Pi_charge, &b_Pi_charge);
   fChain->SetBranchAddress("Pi_eta", &Pi_eta, &b_Pi_eta);
   fChain->SetBranchAddress("Pi_phi", &Pi_phi, &b_Pi_phi);
   fChain->SetBranchAddress("Pi_theta_star", &Pi_theta_star, &b_Pi_theta_star);
   fChain->SetBranchAddress("Pi_d0", &Pi_d0, &b_Pi_d0);
   fChain->SetBranchAddress("Pi_z0", &Pi_z0, &b_Pi_z0);
   fChain->SetBranchAddress("Pi_qOverP", &Pi_qOverP, &b_Pi_qOverP);
   fChain->SetBranchAddress("K_mass", &K_mass, &b_K_mass);
   fChain->SetBranchAddress("K_pT", &K_pT, &b_K_pT);
   fChain->SetBranchAddress("K_charge", &K_charge, &b_K_charge);
   fChain->SetBranchAddress("K_eta", &K_eta, &b_K_eta);
   fChain->SetBranchAddress("K_phi", &K_phi, &b_K_phi);
   fChain->SetBranchAddress("K_theta_star", &K_theta_star, &b_K_theta_star);
   fChain->SetBranchAddress("K_d0", &K_d0, &b_K_d0);
   fChain->SetBranchAddress("K_z0", &K_z0, &b_K_z0);
   fChain->SetBranchAddress("K_qOverP", &K_qOverP, &b_K_qOverP);
   fChain->SetBranchAddress("Pi_s_truth_dr", &Pi_s_truth_dr, &b_Pi_s_truth_dr);
   fChain->SetBranchAddress("Pi_s_truth_eta", &Pi_s_truth_eta, &b_Pi_s_truth_eta);
   fChain->SetBranchAddress("Pi_s_truth_phi", &Pi_s_truth_phi, &b_Pi_s_truth_phi);
   fChain->SetBranchAddress("Pi_s_truth_pT", &Pi_s_truth_pT, &b_Pi_s_truth_pT);
   fChain->SetBranchAddress("Pi_s_truth_m", &Pi_s_truth_m, &b_Pi_s_truth_m);
   fChain->SetBranchAddress("Pi_s_truth_charge", &Pi_s_truth_charge, &b_Pi_s_truth_charge);
   fChain->SetBranchAddress("Pi_s_truth_id", &Pi_s_truth_id, &b_Pi_s_truth_id);
   fChain->SetBranchAddress("Pi_s_truth_parents_eta", &Pi_s_truth_parents_eta, &b_Pi_s_truth_parents_eta);
   fChain->SetBranchAddress("Pi_s_truth_parents_phi", &Pi_s_truth_parents_phi, &b_Pi_s_truth_parents_phi);
   fChain->SetBranchAddress("Pi_s_truth_parents_pT", &Pi_s_truth_parents_pT, &b_Pi_s_truth_parents_pT);
   fChain->SetBranchAddress("Pi_s_truth_parents_m", &Pi_s_truth_parents_m, &b_Pi_s_truth_parents_m);
   fChain->SetBranchAddress("Pi_s_truth_parents_charge", &Pi_s_truth_parents_charge, &b_Pi_s_truth_parents_charge);
   fChain->SetBranchAddress("Pi_s_truth_parents_id", &Pi_s_truth_parents_id, &b_Pi_s_truth_parents_id);
   fChain->SetBranchAddress("Pi_s_truth_siblings_eta", &Pi_s_truth_siblings_eta, &b_Pi_s_truth_siblings_eta);
   fChain->SetBranchAddress("Pi_s_truth_siblings_phi", &Pi_s_truth_siblings_phi, &b_Pi_s_truth_siblings_phi);
   fChain->SetBranchAddress("Pi_s_truth_siblings_pT", &Pi_s_truth_siblings_pT, &b_Pi_s_truth_siblings_pT);
   fChain->SetBranchAddress("Pi_s_truth_siblings_m", &Pi_s_truth_siblings_m, &b_Pi_s_truth_siblings_m);
   fChain->SetBranchAddress("Pi_s_truth_siblings_charge", &Pi_s_truth_siblings_charge, &b_Pi_s_truth_siblings_charge);
   fChain->SetBranchAddress("Pi_s_truth_siblings_id", &Pi_s_truth_siblings_id, &b_Pi_s_truth_siblings_id);
   fChain->SetBranchAddress("Pi_truth_dr", &Pi_truth_dr, &b_Pi_truth_dr);
   fChain->SetBranchAddress("Pi_truth_eta", &Pi_truth_eta, &b_Pi_truth_eta);
   fChain->SetBranchAddress("Pi_truth_phi", &Pi_truth_phi, &b_Pi_truth_phi);
   fChain->SetBranchAddress("Pi_truth_pT", &Pi_truth_pT, &b_Pi_truth_pT);
   fChain->SetBranchAddress("Pi_truth_m", &Pi_truth_m, &b_Pi_truth_m);
   fChain->SetBranchAddress("Pi_truth_charge", &Pi_truth_charge, &b_Pi_truth_charge);
   fChain->SetBranchAddress("Pi_truth_id", &Pi_truth_id, &b_Pi_truth_id);
   fChain->SetBranchAddress("Pi_truth_parents_eta", &Pi_truth_parents_eta, &b_Pi_truth_parents_eta);
   fChain->SetBranchAddress("Pi_truth_parents_phi", &Pi_truth_parents_phi, &b_Pi_truth_parents_phi);
   fChain->SetBranchAddress("Pi_truth_parents_pT", &Pi_truth_parents_pT, &b_Pi_truth_parents_pT);
   fChain->SetBranchAddress("Pi_truth_parents_m", &Pi_truth_parents_m, &b_Pi_truth_parents_m);
   fChain->SetBranchAddress("Pi_truth_parents_charge", &Pi_truth_parents_charge, &b_Pi_truth_parents_charge);
   fChain->SetBranchAddress("Pi_truth_parents_id", &Pi_truth_parents_id, &b_Pi_truth_parents_id);
   fChain->SetBranchAddress("Pi_truth_siblings_eta", &Pi_truth_siblings_eta, &b_Pi_truth_siblings_eta);
   fChain->SetBranchAddress("Pi_truth_siblings_phi", &Pi_truth_siblings_phi, &b_Pi_truth_siblings_phi);
   fChain->SetBranchAddress("Pi_truth_siblings_pT", &Pi_truth_siblings_pT, &b_Pi_truth_siblings_pT);
   fChain->SetBranchAddress("Pi_truth_siblings_m", &Pi_truth_siblings_m, &b_Pi_truth_siblings_m);
   fChain->SetBranchAddress("Pi_truth_siblings_charge", &Pi_truth_siblings_charge, &b_Pi_truth_siblings_charge);
   fChain->SetBranchAddress("Pi_truth_siblings_id", &Pi_truth_siblings_id, &b_Pi_truth_siblings_id);
   fChain->SetBranchAddress("K_truth_dr", &K_truth_dr, &b_K_truth_dr);
   fChain->SetBranchAddress("K_truth_eta", &K_truth_eta, &b_K_truth_eta);
   fChain->SetBranchAddress("K_truth_phi", &K_truth_phi, &b_K_truth_phi);
   fChain->SetBranchAddress("K_truth_pT", &K_truth_pT, &b_K_truth_pT);
   fChain->SetBranchAddress("K_truth_m", &K_truth_m, &b_K_truth_m);
   fChain->SetBranchAddress("K_truth_charge", &K_truth_charge, &b_K_truth_charge);
   fChain->SetBranchAddress("K_truth_id", &K_truth_id, &b_K_truth_id);
   fChain->SetBranchAddress("K_truth_parents_eta", &K_truth_parents_eta, &b_K_truth_parents_eta);
   fChain->SetBranchAddress("K_truth_parents_phi", &K_truth_parents_phi, &b_K_truth_parents_phi);
   fChain->SetBranchAddress("K_truth_parents_pT", &K_truth_parents_pT, &b_K_truth_parents_pT);
   fChain->SetBranchAddress("K_truth_parents_m", &K_truth_parents_m, &b_K_truth_parents_m);
   fChain->SetBranchAddress("K_truth_parents_charge", &K_truth_parents_charge, &b_K_truth_parents_charge);
   fChain->SetBranchAddress("K_truth_parents_id", &K_truth_parents_id, &b_K_truth_parents_id);
   fChain->SetBranchAddress("K_truth_siblings_eta", &K_truth_siblings_eta, &b_K_truth_siblings_eta);
   fChain->SetBranchAddress("K_truth_siblings_phi", &K_truth_siblings_phi, &b_K_truth_siblings_phi);
   fChain->SetBranchAddress("K_truth_siblings_pT", &K_truth_siblings_pT, &b_K_truth_siblings_pT);
   fChain->SetBranchAddress("K_truth_siblings_m", &K_truth_siblings_m, &b_K_truth_siblings_m);
   fChain->SetBranchAddress("K_truth_siblings_charge", &K_truth_siblings_charge, &b_K_truth_siblings_charge);
   fChain->SetBranchAddress("K_truth_siblings_id", &K_truth_siblings_id, &b_K_truth_siblings_id);
   Notify();
}

Bool_t BTreeAnalize::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void BTreeAnalize::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t BTreeAnalize::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

// Define a function with three parameters.
Double_t  BTreeAnalize::FitFunction(double x, double par0, double par1, double par2) {
  double arg = 0;
  if (par2!=0) arg = TMath::Abs((x - par1)/par2);
  double FitVal = par0*TMath::Exp(-0.5*TMath::Power(arg, 1.+1./(1+0.5*arg)));
  return FitVal;
}

// Define a function with three parameters.
/*Double_t  BTreeAnalize::FitFunction(double x, double par0, double par1, double par2) {
  double arg = 0;
  if (par[2]!=0) arg = TMath::Abs((x[0] - par[1])/par[2]);
  double FitVal = par[0]*TMath::Exp(-0.5*TMath::Power(arg, 1.+1./(1+0.5*arg)));
  return FitVal;
  }*/

#endif // #ifdef BTreeAnalize_cxx
