#define MakePlots_B_cxx
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void MakePlots_B(const string& inname, const string& outname1, const string& outname2)
{

   string infile(inname);
   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(infile.c_str());
   if (!f || !f->IsOpen()) {
     f = new TFile(infile.c_str());
   }

   TH1::SetDefaultSumw2();

   // Definition of histogram
   // Correlation in MC sample
   TH1F *h_Distance;
   TH1F *h_B_Distance;
   TH1F *h_n_Truth[7];
   TH2F *h_B_Flavor;
   TH2F *h_B_Flavor_E;
   TH2F *h_B_Flavor_wrt_Time[15];
   TH2F *h_B_Flavor_E_wrt_Time[15];
   TH1F *h_Delta_Time_SF;
   TH1F *h_Delta_Time_OF;
   TH1F *h_Correlation;
   TH1F *h_Significance;
   TH1F *h_NoDelta_Time_SF;
   TH1F *h_NoDelta_Time_OF;
   TH1F *h_NoCorrelation;
   TH1F *h_NoSignificance;

   // Correlation in MC sample
   h_Distance=(TH1F*)f->Get("h_Distance");
   h_Distance->SetXTitle("s^{2}");
   h_Distance->SetYTitle("Entries / 0.5");

   h_B_Distance=(TH1F*)f->Get("h_B_Distance");

   for(int i=0;i<7;i++){
     std::stringstream ss1;
     ss1<<"h_n_Truth"<<std::setw(1)<<i;
     h_n_Truth[i]=(TH1F*)f->Get(ss1.str().c_str());
     h_n_Truth[i]->SetXTitle("#(B^{0})");
     h_n_Truth[i]->SetYTitle("Events");
   }

   h_B_Flavor=(TH2F*)f->Get("h_B_Flavor");
   h_B_Flavor->SetXTitle("B Flavor1");
   h_B_Flavor->SetYTitle("B Flavor2");

   h_B_Flavor_E=(TH2F*)f->Get("h_B_Flavor_E");
   h_B_Flavor_E->SetXTitle("B Flavor1");
   h_B_Flavor_E->SetYTitle("B Flavor2");

   for(int i=0;i<15;i++){
     std::stringstream ss1;
     ss1<<"h_B_Flavor_wrt_Time"<<std::setw(2)<<std::setfill('0')<<i;
     h_B_Flavor_wrt_Time[i]=(TH2F*)f->Get(ss1.str().c_str());
     h_B_Flavor_wrt_Time[i]->SetXTitle("B Flavor1");
     h_B_Flavor_wrt_Time[i]->SetYTitle("B Flavor2");

     std::stringstream ss2;
     ss2<<"h_B_Flavor_E_wrt_Time"<<std::setw(2)<<std::setfill('0')<<i;
     h_B_Flavor_E_wrt_Time[i]=(TH2F*)f->Get(ss2.str().c_str());
     h_B_Flavor_E_wrt_Time[i]->SetXTitle("B Flavor1");
     h_B_Flavor_E_wrt_Time[i]->SetYTitle("B Flavor2");
   }

   h_Delta_Time_SF=(TH1F*)f->Get("h_Delta_Time_SF");
   h_Delta_Time_SF->SetXTitle("#Deltat [ps]");
   h_Delta_Time_SF->SetYTitle("Entries / 0.5 ps");

   h_Delta_Time_OF=(TH1F*)f->Get("h_Delta_Time_OF");
   h_Delta_Time_OF->SetXTitle("#Deltat [ps]");
   h_Delta_Time_OF->SetYTitle("Entries / 0.5 ps");

   h_Correlation=(TH1F*)f->Get("h_Correlation");
   h_Correlation->SetXTitle("#Deltat [ps]");
   h_Correlation->SetYTitle("Correlation / 0.5 ps");

   h_Significance=(TH1F*)f->Get("h_Significance");
   h_Significance->SetXTitle("#Deltat [ps]");
   h_Significance->SetYTitle("|S| / 0.5 ps");

   h_NoDelta_Time_SF=(TH1F*)f->Get("h_NoDelta_Time_SF");
   h_NoDelta_Time_SF->SetXTitle("#Deltat [ps]");
   h_NoDelta_Time_SF->SetYTitle("Entries / 0.5 ps");

   h_NoDelta_Time_OF=(TH1F*)f->Get("h_NoDelta_Time_OF");
   h_NoDelta_Time_OF->SetXTitle("#Deltat [ps]");
   h_NoDelta_Time_OF->SetYTitle("Entries / 0.5 ps");

   h_NoCorrelation=(TH1F*)f->Get("h_NoCorrelation");
   h_NoCorrelation->SetXTitle("#Deltat [ps]");
   h_NoCorrelation->SetYTitle("Correlation / 0.5 ps");

   h_NoSignificance=(TH1F*)f->Get("h_NoSignificance");
   h_NoSignificance->SetXTitle("#Deltat [ps]");
   h_NoSignificance->SetYTitle("|S| / 0.5 ps");


   int color[8]=
     {400, 632, 600, 800, 432, 616, 416, 0}; //Yel, Red, Cya, Blu, Ora, Gre, Mag, Gre, Whi

   TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
   l.SetNDC();
   l.SetTextFont(72);
   l.SetTextColor(kBlack);
   l.SetTextSize(0.04); 
   TLatex p; 
   p.SetNDC();
   p.SetTextFont(42);
   p.SetTextSize(0.04); 
   p.SetTextColor(kBlack);
   TLatex n1; 
   n1.SetNDC();
   n1.SetTextFont(42);
   n1.SetTextSize(0.032); 
   n1.SetTextColor(kBlack);
   std::stringstream ss;
   ss<<"#splitline{#sqrt{s}=13 TeV, Signal MC sample}{(Truth_Information, After Scaling)}";
   TLatex n2;
   n2.SetNDC();
   n2.SetTextFont(42);
   n2.SetTextSize(0.032); 
   n2.SetTextColor(kBlack);

   TLatex title; 
   title.SetNDC();
   title.SetTextFont(42);
   title.SetTextSize(0.01); 
   title.SetTextColor(kBlack);

   const char *outname3 = outname2.c_str();
   string outname40 = outname2+"(";
   const char *outname4 = outname40.c_str();
   string outname50 = outname2+")";
   const char *outname5 = outname50.c_str();

   TCanvas *c_Distance;
   TCanvas *c_B_Distance;

   TCanvas *c_Truth;
   TCanvas *c_Truth_HighPt;

   TCanvas *c_Flavor;
   TCanvas *c_Flavor_wrt_Time;
   TCanvas *c_Flavor_E_wrt_Time;

   TCanvas *c_Delta_Time_SF;
   TCanvas *c_Delta_Time_OF;
   TCanvas *c_Correlation;
   TCanvas *c_Significance;

   TCanvas *c_NoDelta_Time_SF;
   TCanvas *c_NoDelta_Time_OF;
   TCanvas *c_NoCorrelation;
   TCanvas *c_NoSignificance;


   c_Distance = new TCanvas("c_Distance", "Proper Distance", 1500, 900);
   h_Distance->Draw("HIST");
   h_Distance->SetStats(0);
   c_Distance->SetLogy();
   h_Distance->SetMarkerStyle(8);
   l.DrawLatex(0.60,0.85,"ATLAS");
   p.DrawLatex(0.705,0.85,"Work in Progress");
   n1.DrawLatex(0.61,0.785,ss.str().c_str());

   c_B_Distance = new TCanvas("c_B_Distance", "B0 Distance", 1500, 900);
   h_B_Distance->Draw("HIST");
   h_B_Distance->SetStats(0);
   c_B_Distance->SetLogy();
   h_B_Distance->SetMarkerStyle(8);
   l.DrawLatex(0.60,0.85,"ATLAS");
   p.DrawLatex(0.705,0.85,"Work in Progress");
   n1.DrawLatex(0.61,0.785,ss.str().c_str());

   c_Truth = new TCanvas("c_Truth", "", 800, 600);
   h_n_Truth[0]->Draw("HIST");
   h_n_Truth[0]->SetMinimum(1);
   h_n_Truth[0]->SetLineColor(kBlack);
   h_n_Truth[0]->SetStats(0);
   h_n_Truth[1]->Draw("SAME,HIST");
   h_n_Truth[1]->SetLineColor(kGreen);
   h_n_Truth[1]->SetStats(0);
   h_n_Truth[2]->Draw("SAME,HIST");
   h_n_Truth[2]->SetLineColor(kBlue);
   h_n_Truth[2]->SetStats(0);
   h_n_Truth[3]->Draw("SAME,HIST");
   h_n_Truth[3]->SetLineColor(kViolet);
   h_n_Truth[3]->SetStats(0);
   gPad->SetLogy(1);
   l.DrawLatex(0.60,0.85,"ATLAS");
   p.DrawLatex(0.705,0.85,"Work in Progress");
   n1.DrawLatex(0.61,0.785,ss.str().c_str());
   TLegend *legend1 = new TLegend( 0.55, 0.45, 0.85, 0.6) ;
   legend1->AddEntry( h_n_Truth[0], "Truth B^{0}(B^{0}->D*#mu#nu)" , "f") ;
   legend1->AddEntry( h_n_Truth[1], "Truth B^{0}(B^{0}->D*#mu#nu,D*->D^{0}#pi)" , "f") ;
   legend1->AddEntry( h_n_Truth[2], "Truth B^{0}(B^{0}->D*#mu#nu,D*->D^{0}#pi,D^{0}->K#pi/K3#pi)" , "f") ;
   legend1->AddEntry( h_n_Truth[3], "Truth B^{0}(B^{0}->D*#mu#nu,D*->D^{0}#pi,D^{0}->K3#pi)" , "f") ;
   legend1->SetFillColor(0);
   legend1->SetBorderSize(0);
   legend1->Draw() ;

   c_Truth_HighPt = new TCanvas("c_Truth_HighPt", "", 800, 600);
   h_n_Truth[2]->Draw("HIST");
   h_n_Truth[2]->SetMinimum(1);
   h_n_Truth[2]->SetStats(0);
   h_n_Truth[4]->Draw("SAME,HIST");
   h_n_Truth[4]->SetLineColor(kGreen);
   h_n_Truth[4]->SetStats(0);
   h_n_Truth[5]->Draw("SAME,HIST");
   h_n_Truth[5]->SetLineColor(kMagenta);
   h_n_Truth[5]->SetStats(0);
   h_n_Truth[6]->Draw("SAME,HIST");
   h_n_Truth[6]->SetLineColor(kRed);
   h_n_Truth[6]->SetStats(0);
   gPad->SetLogy(1);
   l.DrawLatex(0.60,0.85,"ATLAS");
   p.DrawLatex(0.705,0.85,"Work in Progress");
   n1.DrawLatex(0.61,0.785,ss.str().c_str());
   TLegend *legend2 = new TLegend( 0.52, 0.4, 0.87, 0.6) ;
   legend2->AddEntry( h_n_Truth[2], "Truth B^{0}(B^{0}->D*#mu#nu,D*->D^{0}#pi,D^{0}->K#pi/K3#pi)" , "f") ;
   legend2->AddEntry( h_n_Truth[4], "Truth High Pt B^{0}" , "f") ;
   legend2->AddEntry( h_n_Truth[5], "Truth B^{0} matching Rec. B^{0}" , "f") ;
   legend2->AddEntry( h_n_Truth[6], "Truth B^{0} matching Rec. B^{0} with RCC and sig. #Deltam" , "f") ;
   legend2->SetFillColor(0);
   legend2->SetBorderSize(0);
   legend2->Draw() ;

   c_Flavor = new TCanvas("c_Flavor", "", 900, 675);
   c_Flavor->Divide(2,1);
   c_Flavor->cd(1);
   h_B_Flavor->Draw("colz");
   h_B_Flavor->SetStats(0);
   c_Flavor->cd(2);
   h_B_Flavor_E->Draw("colz");
   h_B_Flavor_E->SetStats(0);
   
   c_Flavor_wrt_Time = new TCanvas("c_Flavor_wrt_Time", "", 1500, 900);
   c_Flavor_wrt_Time->Divide(3,3);
   for(int i=0;i<9;i++){
     c_Flavor_wrt_Time->cd(i+1);
     h_B_Flavor_wrt_Time[i]->Draw("colz");
     h_B_Flavor_wrt_Time[i]->SetStats(0);
   }
   
   c_Flavor_E_wrt_Time = new TCanvas("c_Flavor_E_wrt_Time", "", 1500, 900);
   c_Flavor_E_wrt_Time->Divide(3,3);
   for(int i=0;i<9;i++){
     c_Flavor_E_wrt_Time->cd(i+1);
     h_B_Flavor_E_wrt_Time[i]->Draw("colz");
     h_B_Flavor_E_wrt_Time[i]->SetStats(0);
   }
   
   c_Delta_Time_SF = new TCanvas("c_Delta_Time_SF", "Delta Time of Same Flavor", 1500, 900);
   h_Delta_Time_SF->Draw("HIST");
   h_Delta_Time_SF->SetStats(0);
   c_Delta_Time_SF->SetLogy();
   l.DrawLatex(0.62,0.85,"ATLAS");
   p.DrawLatex(0.725,0.85,"Work in Progress");
   n1.DrawLatex(0.63,0.785,ss.str().c_str());
   n2.DrawLatex(0.63,0.73,"Same Flavor Combination");
   
   c_Delta_Time_OF = new TCanvas("c_Delta_Time_OF", "Delta Time of Opposite Flavor", 1500, 900);
   h_Delta_Time_OF->Draw("HIST");
   h_Delta_Time_OF->SetStats(0);
   c_Delta_Time_OF->SetLogy();
   l.DrawLatex(0.62,0.85,"ATLAS");
   p.DrawLatex(0.725,0.85,"Work in Progress");
   n1.DrawLatex(0.63,0.785,ss.str().c_str());
   n2.DrawLatex(0.63,0.73,"Opposite Flavor Combination");

   c_Correlation = new TCanvas("c_Correlation", "Correlation", 1500, 900);
   h_Correlation->Draw("EP");
   h_Correlation->SetStats(0);
   h_Correlation->SetMarkerStyle(8);
   TF1 *f_Correlation_Limit1=new TF1("1","1",-1,11);
   f_Correlation_Limit1->SetLineStyle(2);
   f_Correlation_Limit1->SetLineColor(kBlack);
   f_Correlation_Limit1->Draw("SAME");
   TF1 *f_Correlation_Limit2=new TF1("-1","-1",-1,11);
   f_Correlation_Limit2->SetLineStyle(2);
   f_Correlation_Limit2->SetLineColor(kBlack);
   f_Correlation_Limit2->Draw("SAME");
   l.DrawLatex(0.17,0.80,"ATLAS");
   p.DrawLatex(0.275,0.80,"Work in Progress");
   n1.DrawLatex(0.18,0.735,ss.str().c_str());
   c_Correlation->SetTicks();

   c_Significance = new TCanvas("c_Significance", "Significance", 900, 675);
   title.DrawLatex(0.3,0.98,"Significance");
   h_Significance->Draw("EP");
   h_Significance->SetStats(0);
   h_Significance->SetMarkerStyle(8);
   h_Significance->SetLineColor(kBlack);
   TF1 *f_Limit=new TF1("2","2",-1,11);
   f_Limit->SetLineStyle(2);
   f_Limit->SetLineColor(kRed);
   f_Limit->Draw("SAME");
   TF1 *f_SM=new TF1("SM","fabs(-3*cos(0.5065*x)+cos(3*0.5065*x))",-1,11);
   f_SM->SetLineColor(kBlack);
   f_SM->Draw("SAME");
   l.DrawLatex(0.55,0.80,"ATLAS");
   p.DrawLatex(0.655,0.80,"Work in Progress");
   n1.DrawLatex(0.56,0.735,ss.str().c_str());
   c_Significance->SetTicks();
   
   c_NoDelta_Time_SF = new TCanvas("c_NoDelta_Time_SF", "Delta Time of Same Flavor", 1500, 900);
   h_NoDelta_Time_SF->Draw("HIST");
   h_NoDelta_Time_SF->SetStats(0);
   c_NoDelta_Time_SF->SetLogy();
   l.DrawLatex(0.62,0.85,"ATLAS");
   p.DrawLatex(0.725,0.85,"Work in Progress");
   n1.DrawLatex(0.63,0.785,ss.str().c_str());
   n2.DrawLatex(0.63,0.73,"Same Flavor Combination");
   
   c_NoDelta_Time_OF = new TCanvas("c_NoDelta_Time_OF", "Delta Time of Opposite Flavor", 1500, 900);
   h_NoDelta_Time_OF->Draw("HIST");
   h_NoDelta_Time_OF->SetStats(0);
   c_NoDelta_Time_OF->SetLogy();
   l.DrawLatex(0.62,0.85,"ATLAS");
   p.DrawLatex(0.725,0.85,"Work in Progress");
   n1.DrawLatex(0.63,0.785,ss.str().c_str());
   n2.DrawLatex(0.63,0.73,"Opposite Flavor Combination");

   c_NoCorrelation = new TCanvas("c_NoCorrelation", "Correlation", 1500, 900);
   h_NoCorrelation->Draw("EP");
   h_NoCorrelation->SetStats(0);
   h_NoCorrelation->SetMarkerStyle(8);
   f_Correlation_Limit1->SetLineStyle(2);
   f_Correlation_Limit1->SetLineColor(kBlack);
   f_Correlation_Limit1->Draw("SAME");
   f_Correlation_Limit2->SetLineStyle(2);
   f_Correlation_Limit2->SetLineColor(kBlack);
   f_Correlation_Limit2->Draw("SAME");
   l.DrawLatex(0.17,0.80,"ATLAS");
   p.DrawLatex(0.275,0.80,"Work in Progress");
   n1.DrawLatex(0.18,0.735,ss.str().c_str());
   c_NoCorrelation->SetTicks();

   c_NoSignificance = new TCanvas("c_NoSignificance", "Significance", 900, 675);
   title.DrawLatex(0.3,0.98,"Significance");
   h_NoSignificance->Draw("EP");
   h_NoSignificance->SetStats(0);
   h_NoSignificance->SetMarkerStyle(8);
   h_NoSignificance->SetLineColor(kBlack);
   f_Limit->SetLineStyle(2);
   f_Limit->SetLineColor(kRed);
   f_Limit->Draw("SAME");
   f_SM->SetLineColor(kBlack);
   f_SM->Draw("SAME");
   l.DrawLatex(0.55,0.80,"ATLAS");
   p.DrawLatex(0.655,0.80,"Work in Progress");
   n1.DrawLatex(0.56,0.735,ss.str().c_str());
   c_NoSignificance->SetTicks();
   
   TFile *file = new TFile(outname1.c_str(), "RECREATE");


   h_Distance->Write();
   h_B_Distance->Write();
   for(int i=0;i<6;i++)
     h_n_Truth[i]->Write();
   h_B_Flavor->Write();
   h_B_Flavor_E->Write();
   for(int i=0;i<15;i++){
     h_B_Flavor_wrt_Time[i]->Write();
     h_B_Flavor_E_wrt_Time[i]->Write();
   }
   h_Delta_Time_SF->Write();
   h_Delta_Time_OF->Write();
   h_Correlation->Write();
   h_Significance->Write();
   h_NoDelta_Time_SF->Write();
   h_NoDelta_Time_OF->Write();
   h_NoCorrelation->Write();
   h_NoSignificance->Write();

   c_Distance->Write();
   c_B_Distance->Write();
   c_Truth->Write();
   c_Truth_HighPt->Write();
   c_Flavor->Write();
   c_Flavor_wrt_Time->Write();
   c_Flavor_E_wrt_Time->Write();
   c_Delta_Time_SF->Write();
   c_Delta_Time_OF->Write();
   c_Correlation->Write();
   c_Significance->Write();
   c_NoDelta_Time_SF->Write();
   c_NoDelta_Time_OF->Write();
   c_NoCorrelation->Write();
   c_NoSignificance->Write();

   file->Close();


   c_Distance->Print(outname4, "Title:Proper Distance");
   c_B_Distance->Print(outname3, "Title:B0 Distance");
   c_Truth->Print(outname3, "Title:Truth B0 of D* mode");
   c_Truth_HighPt->Print(outname3, "Title:Truth high pT B0 of D* mode");

   c_Flavor->Print(outname3, "Title:Flavor Combination of Truth B0");
   c_Flavor_wrt_Time->Print(outname3, "Title:Flavor Combination of Truth B0 with respect to Decay Time");
   c_Flavor_E_wrt_Time->Print(outname3, "Title:Flavor Combination of Truth B0 with respect to Decay Time (No)");

   c_Delta_Time_SF->Print(outname3, "Title:Delta Time Same Flavor");
   c_Delta_Time_OF->Print(outname3, "Title:Delta Time Opposite Flavor");
   c_Correlation->Print(outname3, "Title:Correlation");
   c_Significance->Print(outname3, "Title:Significance");

   c_NoDelta_Time_SF->Print(outname3, "Title:Delta Time Same Flavor (No Correlation)");
   c_NoDelta_Time_OF->Print(outname3, "Title:Delta Time Opposite Flavor (No Correlation)");
   c_NoCorrelation->Print(outname3, "Title:Correlation (No Correlation)");
   c_NoSignificance->Print(outname5, "Title:Significance (No Correlation)");


}

