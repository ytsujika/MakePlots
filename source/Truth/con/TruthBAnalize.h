//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Mar  1 14:30:03 2024 by ROOT version 6.18/04
// from TTree Truth_B/Truth_B
// found on file: /home/ytsujika/AnalysisBphys/run/run1_B/Run34687508/job.000001/produced_rootfiles/myfile.root.000001
//////////////////////////////////////////////////////////

#ifndef TruthBAnalize_h
#define TruthBAnalize_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class TruthBAnalize {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

// Definition of histogram

   // Correlation in MC sample
   TH1F *h_Distance;
   TH1F *h_n_Truth[6];
   TH2F *h_B_Flavor;
   TH2F *h_B_Flavor_E;
   TH2F *h_B_Flavor_wrt_Time[15];
   TH2F *h_B_Flavor_E_wrt_Time[15];
   TH1F *h_Delta_Time_SF;
   TH1F *h_Delta_Time_OF;
   TH1F *h_Correlation;
   TH1F *h_Significance;
   TH1F *h_NoDelta_Time_SF;
   TH1F *h_NoDelta_Time_OF;
   TH1F *h_NoCorrelation;
   TH1F *h_NoSignificance;

   // Declaration of leaf types
   Int_t           n_B;
   Int_t           n_B_Pass;
   Int_t           n_B_Match;
   Int_t           n_B_Match_Pass;
   Int_t           n_B_Truth;
   Int_t           n_Dstar;
   Int_t           n_Dstar_Pass;
   Int_t           n_Dstar_Match;
   Int_t           n_Dstar_Match_Pass;
   Int_t           n_Dstar_Truth;
   Int_t           n_Dstar_Truth_HighPt;
   Int_t           n_B_Dstar_Truth;
   Int_t           n_B_Dstar_D0Pi_Truth;
   Int_t           n_B_D_Truth;
   Int_t           n_B_D_s_Truth;
   Int_t           n_B_Lambda_c_Truth;
   vector<bool>    *isHighPt;
   vector<int>     *Flavor;
   vector<float>   *DecayTime;
   vector<float>   *DeltaT;
   vector<bool>    *hasMuon;
   vector<int>     *DecayType;
   vector<int>     *pdgID;
   vector<int>     *parent_num;
   vector<vector<int> > *parent_pdgID;
   vector<int>     *track_num;
   vector<vector<int> > *track_pdgID;
   vector<vector<int> > *child_num;
   vector<vector<vector<int> > > *child_pdgID;
   vector<double>  *Truth_x;
   vector<double>  *Truth_y;
   vector<double>  *Truth_z;
   vector<double>  *Truth_eta;
   vector<double>  *Truth_phi;
   vector<double>  *B_pT;
   vector<double>  *B_eta;
   vector<double>  *B_phi;
   vector<double>  *Dstar_pT;
   vector<double>  *Dstar_eta;
   vector<double>  *Dstar_phi;
   vector<double>  *Dstar_charge;
   vector<double>  *D0_mass;
   vector<double>  *D0_pT;
   vector<double>  *D0_eta;
   vector<double>  *D0_phi;
   vector<double>  *Mu_pT;
   vector<double>  *Mu_eta;
   vector<double>  *Mu_phi;
   vector<double>  *Mu_charge;
   vector<double>  *Pi_s_mass;
   vector<double>  *Pi_s_pT;
   vector<double>  *Pi_s_eta;
   vector<double>  *Pi_s_phi;
   vector<double>  *Pi_s_charge;
   vector<double>  *Pi_s_pdgID;
   vector<double>  *D0_child_num;
   vector<double>  *D0_charged_child_num;
   vector<double>  *D0_reco_mass;
   vector<double>  *D0_reco_pT;
   vector<vector<double> > *D0_child_mass;
   vector<vector<double> > *D0_child_pT;
   vector<vector<double> > *D0_child_eta;
   vector<vector<double> > *D0_child_phi;
   vector<vector<double> > *D0_child_charge;
   vector<vector<double> > *D0_child_pdgID;
   vector<bool>    *isMatch;
   vector<float>   *Min_d;
   vector<float>   *Min_dx;
   vector<float>   *Min_dy;
   vector<float>   *Min_dz;
   vector<float>   *Reco_x;
   vector<float>   *Reco_y;
   vector<float>   *Reco_z;
   vector<float>   *Reco_eta;
   vector<float>   *Reco_phi;
   vector<bool>    *Reco_isPassed;
   vector<float>   *Reco_B_mass;
   vector<float>   *Reco_B_eta;
   vector<float>   *Reco_B_phi;
   vector<float>   *Reco_B_pT;
   vector<int>     *Reco_B_flavor;
   vector<float>   *Reco_Dstar_mass;
   vector<float>   *Reco_Dstar_eta;
   vector<float>   *Reco_Dstar_phi;
   vector<float>   *Reco_Dstar_pT;
   vector<int>     *Reco_Dstar_charge;
   vector<float>   *Reco_D0_mass;
   vector<float>   *Reco_D0_eta;
   vector<float>   *Reco_D0_phi;
   vector<float>   *Reco_D0_pT;
   vector<float>   *Reco_Mu_eta;
   vector<float>   *Reco_Mu_phi;
   vector<float>   *Reco_Mu_pT;
   vector<float>   *Reco_Mu_charge;
   vector<float>   *Reco_Pi_s_pT;
   vector<float>   *Reco_Pi_s_eta;
   vector<float>   *Reco_Pi_s_phi;
   vector<float>   *Reco_Pi_s_charge;
   vector<float>   *Reco_Pi_pT;
   vector<float>   *Reco_Pi_eta;
   vector<float>   *Reco_Pi_phi;
   vector<float>   *Reco_Pi_charge;
   vector<float>   *Reco_K_pT;
   vector<float>   *Reco_K_eta;
   vector<float>   *Reco_K_phi;
   vector<float>   *Reco_K_charge;
   vector<bool>    *isMatch_Pass;
   vector<float>   *Min_d_Pass;
   vector<float>   *Min_dx_Pass;
   vector<float>   *Min_dy_Pass;
   vector<float>   *Min_dz_Pass;
   vector<float>   *Reco_x_Pass;
   vector<float>   *Reco_y_Pass;
   vector<float>   *Reco_z_Pass;
   vector<float>   *Reco_eta_Pass;
   vector<float>   *Reco_phi_Pass;
   vector<float>   *Reco_B_mass_Pass;
   vector<float>   *Reco_B_eta_Pass;
   vector<float>   *Reco_B_phi_Pass;
   vector<float>   *Reco_B_pT_Pass;
   vector<int>     *Reco_B_flavor_Pass;
   vector<float>   *Reco_Dstar_mass_Pass;
   vector<float>   *Reco_Dstar_eta_Pass;
   vector<float>   *Reco_Dstar_phi_Pass;
   vector<float>   *Reco_Dstar_pT_Pass;
   vector<int>     *Reco_Dstar_charge_Pass;
   vector<float>   *Reco_D0_mass_Pass;
   vector<float>   *Reco_D0_eta_Pass;
   vector<float>   *Reco_D0_phi_Pass;
   vector<float>   *Reco_D0_pT_Pass;
   vector<int>     *Reco_D0_charge_Pass;
   vector<float>   *Reco_Mu_eta_Pass;
   vector<float>   *Reco_Mu_phi_Pass;
   vector<float>   *Reco_Mu_pT_Pass;
   vector<float>   *Reco_Mu_charge_Pass;
   vector<float>   *Reco_Pi_s_pT_Pass;
   vector<float>   *Reco_Pi_s_eta_Pass;
   vector<float>   *Reco_Pi_s_phi_Pass;
   vector<float>   *Reco_Pi_s_charge_Pass;
   vector<float>   *Reco_Pi_pT_Pass;
   vector<float>   *Reco_Pi_eta_Pass;
   vector<float>   *Reco_Pi_phi_Pass;
   vector<float>   *Reco_Pi_charge_Pass;
   vector<float>   *Reco_K_pT_Pass;
   vector<float>   *Reco_K_eta_Pass;
   vector<float>   *Reco_K_phi_Pass;
   vector<float>   *Reco_K_charge_Pass;

   // List of branches
   TBranch        *b_n_B;   //!
   TBranch        *b_n_B_Pass;   //!
   TBranch        *b_n_B_Match;   //!
   TBranch        *b_n_B_Match_Pass;   //!
   TBranch        *b_n_B_Truth;   //!
   TBranch        *b_n_Dstar;   //!
   TBranch        *b_n_Dstar_Pass;   //!
   TBranch        *b_n_Dstar_Match;   //!
   TBranch        *b_n_Dstar_Match_Pass;   //!
   TBranch        *b_n_Dstar_Truth;   //!
   TBranch        *b_n_Dstar_Truth_HighPt;   //!
   TBranch        *b_n_B_Dstar_Truth;   //!
   TBranch        *b_n_B_Dstar_D0Pi_Truth;   //!
   TBranch        *b_n_B_D_Truth;   //!
   TBranch        *b_n_B_D_s_Truth;   //!
   TBranch        *b_n_B_Lambda_c_Truth;   //!
   TBranch        *b_isHighPt;   //!
   TBranch        *b_Flavor;   //!
   TBranch        *b_DecayTime;   //!
   TBranch        *b_DeltaT;   //!
   TBranch        *b_hasMuon;   //!
   TBranch        *b_DecayType;   //!
   TBranch        *b_pdgID;   //!
   TBranch        *b_parent_num;   //!
   TBranch        *b_parent_pdgID;   //!
   TBranch        *b_track_num;   //!
   TBranch        *b_track_pdgID;   //!
   TBranch        *b_child_num;   //!
   TBranch        *b_child_pdgID;   //!
   TBranch        *b_Truth_x;   //!
   TBranch        *b_Truth_y;   //!
   TBranch        *b_Truth_z;   //!
   TBranch        *b_Truth_eta;   //!
   TBranch        *b_Truth_phi;   //!
   TBranch        *b_B_pT;   //!
   TBranch        *b_B_eta;   //!
   TBranch        *b_B_phi;   //!
   TBranch        *b_Dstar_pT;   //!
   TBranch        *b_Dstar_eta;   //!
   TBranch        *b_Dstar_phi;   //!
   TBranch        *b_Dstar_charge;   //!
   TBranch        *b_D0_mass;   //!
   TBranch        *b_D0_pT;   //!
   TBranch        *b_D0_eta;   //!
   TBranch        *b_D0_phi;   //!
   TBranch        *b_Mu_pT;   //!
   TBranch        *b_Mu_eta;   //!
   TBranch        *b_Mu_phi;   //!
   TBranch        *b_Mu_charge;   //!
   TBranch        *b_Pi_s_mass;   //!
   TBranch        *b_Pi_s_pT;   //!
   TBranch        *b_Pi_s_eta;   //!
   TBranch        *b_Pi_s_phi;   //!
   TBranch        *b_Pi_s_charge;   //!
   TBranch        *b_Pi_s_pdgID;   //!
   TBranch        *b_D0_child_num;   //!
   TBranch        *b_D0_charged_child_num;   //!
   TBranch        *b_D0_reco_mass;   //!
   TBranch        *b_D0_reco_pT;   //!
   TBranch        *b_D0_child_mass;   //!
   TBranch        *b_D0_child_pT;   //!
   TBranch        *b_D0_child_eta;   //!
   TBranch        *b_D0_child_phi;   //!
   TBranch        *b_D0_child_charge;   //!
   TBranch        *b_D0_child_pdgID;   //!
   TBranch        *b_isMatch;   //!
   TBranch        *b_Min_d;   //!
   TBranch        *b_Min_dx;   //!
   TBranch        *b_Min_dy;   //!
   TBranch        *b_Min_dz;   //!
   TBranch        *b_Reco_x;   //!
   TBranch        *b_Reco_y;   //!
   TBranch        *b_Reco_z;   //!
   TBranch        *b_Reco_eta;   //!
   TBranch        *b_Reco_phi;   //!
   TBranch        *b_Reco_isPassed;   //!
   TBranch        *b_Reco_B_mass;   //!
   TBranch        *b_Reco_B_eta;   //!
   TBranch        *b_Reco_B_phi;   //!
   TBranch        *b_Reco_B_pT;   //!
   TBranch        *b_Reco_B_flavor;   //!
   TBranch        *b_Reco_Dstar_mass;   //!
   TBranch        *b_Reco_Dstar_eta;   //!
   TBranch        *b_Reco_Dstar_phi;   //!
   TBranch        *b_Reco_Dstar_pT;   //!
   TBranch        *b_Reco_Dstar_charge;   //!
   TBranch        *b_Reco_D0_mass;   //!
   TBranch        *b_Reco_D0_eta;   //!
   TBranch        *b_Reco_D0_phi;   //!
   TBranch        *b_Reco_D0_pT;   //!
   TBranch        *b_Reco_Mu_eta;   //!
   TBranch        *b_Reco_Mu_phi;   //!
   TBranch        *b_Reco_Mu_pT;   //!
   TBranch        *b_Reco_Mu_charge;   //!
   TBranch        *b_Reco_Pi_s_pT;   //!
   TBranch        *b_Reco_Pi_s_eta;   //!
   TBranch        *b_Reco_Pi_s_phi;   //!
   TBranch        *b_Reco_Pi_s_charge;   //!
   TBranch        *b_Reco_Pi_pT;   //!
   TBranch        *b_Reco_Pi_eta;   //!
   TBranch        *b_Reco_Pi_phi;   //!
   TBranch        *b_Reco_Pi_charge;   //!
   TBranch        *b_Reco_K_pT;   //!
   TBranch        *b_Reco_K_eta;   //!
   TBranch        *b_Reco_K_phi;   //!
   TBranch        *b_Reco_K_charge;   //!
   TBranch        *b_isMatch_Pass;   //!
   TBranch        *b_Min_d_Pass;   //!
   TBranch        *b_Min_dx_Pass;   //!
   TBranch        *b_Min_dy_Pass;   //!
   TBranch        *b_Min_dz_Pass;   //!
   TBranch        *b_Reco_x_Pass;   //!
   TBranch        *b_Reco_y_Pass;   //!
   TBranch        *b_Reco_z_Pass;   //!
   TBranch        *b_Reco_eta_Pass;   //!
   TBranch        *b_Reco_phi_Pass;   //!
   TBranch        *b_Reco_B_mass_Pass;   //!
   TBranch        *b_Reco_B_eta_Pass;   //!
   TBranch        *b_Reco_B_phi_Pass;   //!
   TBranch        *b_Reco_B_pT_Pass;   //!
   TBranch        *b_Reco_B_flavor_Pass;   //!
   TBranch        *b_Reco_Dstar_mass_Pass;   //!
   TBranch        *b_Reco_Dstar_eta_Pass;   //!
   TBranch        *b_Reco_Dstar_phi_Pass;   //!
   TBranch        *b_Reco_Dstar_pT_Pass;   //!
   TBranch        *b_Reco_Dstar_charge_Pass;   //!
   TBranch        *b_Reco_D0_mass_Pass;   //!
   TBranch        *b_Reco_D0_eta_Pass;   //!
   TBranch        *b_Reco_D0_phi_Pass;   //!
   TBranch        *b_Reco_D0_pT_Pass;   //!
   TBranch        *b_Reco_D0_charge_Pass;   //!
   TBranch        *b_Reco_Mu_eta_Pass;   //!
   TBranch        *b_Reco_Mu_phi_Pass;   //!
   TBranch        *b_Reco_Mu_pT_Pass;   //!
   TBranch        *b_Reco_Mu_charge_Pass;   //!
   TBranch        *b_Reco_Pi_s_pT_Pass;   //!
   TBranch        *b_Reco_Pi_s_eta_Pass;   //!
   TBranch        *b_Reco_Pi_s_phi_Pass;   //!
   TBranch        *b_Reco_Pi_s_charge_Pass;   //!
   TBranch        *b_Reco_Pi_pT_Pass;   //!
   TBranch        *b_Reco_Pi_eta_Pass;   //!
   TBranch        *b_Reco_Pi_phi_Pass;   //!
   TBranch        *b_Reco_Pi_charge_Pass;   //!
   TBranch        *b_Reco_K_pT_Pass;   //!
   TBranch        *b_Reco_K_eta_Pass;   //!
   TBranch        *b_Reco_K_phi_Pass;   //!
   TBranch        *b_Reco_K_charge_Pass;   //!

   TruthBAnalize(const int& isAdd, const string& infname, const string& outfname, const float& width1, TTree *tree=0);
   virtual ~TruthBAnalize();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(const int& isAdd, const string& outfname, const float& width1);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TruthBAnalize_cxx
TruthBAnalize::TruthBAnalize(const int& isAdd, const string& infname, const string& outfname, const float& width1, TTree *tree) : fChain(0) 
{
   string infile(infname);
  if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(infile.c_str());
      if (!f || !f->IsOpen()) {
         f = new TFile(infile.c_str());
      }
      f->GetObject("Truth_B",tree);

   }
   Init(tree);
   Loop(isAdd, outfname, width1);

   /*
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/home/ytsujika/AnalysisBphys/run/Rootfile/BPHY22-0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/home/ytsujika/AnalysisBphys/run/Rootfile/BPHY22-0.root");
      }
      f->GetObject("Truth_B",tree);

   }
   Init(tree);
   */
}

TruthBAnalize::~TruthBAnalize()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TruthBAnalize::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TruthBAnalize::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TruthBAnalize::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   isHighPt = 0;
   Flavor = 0;
   DecayTime = 0;
   DeltaT = 0;
   hasMuon = 0;
   DecayType = 0;
   pdgID = 0;
   parent_num = 0;
   parent_pdgID = 0;
   track_num = 0;
   track_pdgID = 0;
   child_num = 0;
   child_pdgID = 0;
   Truth_x = 0;
   Truth_y = 0;
   Truth_z = 0;
   Truth_eta = 0;
   Truth_phi = 0;
   B_pT = 0;
   B_eta = 0;
   B_phi = 0;
   Dstar_pT = 0;
   Dstar_eta = 0;
   Dstar_phi = 0;
   Dstar_charge = 0;
   D0_mass = 0;
   D0_pT = 0;
   D0_eta = 0;
   D0_phi = 0;
   Mu_pT = 0;
   Mu_eta = 0;
   Mu_phi = 0;
   Mu_charge = 0;
   Pi_s_mass = 0;
   Pi_s_pT = 0;
   Pi_s_eta = 0;
   Pi_s_phi = 0;
   Pi_s_charge = 0;
   Pi_s_pdgID = 0;
   D0_child_num = 0;
   D0_charged_child_num = 0;
   D0_reco_mass = 0;
   D0_reco_pT = 0;
   D0_child_mass = 0;
   D0_child_pT = 0;
   D0_child_eta = 0;
   D0_child_phi = 0;
   D0_child_charge = 0;
   D0_child_pdgID = 0;
   isMatch = 0;
   Min_d = 0;
   Min_dx = 0;
   Min_dy = 0;
   Min_dz = 0;
   Reco_x = 0;
   Reco_y = 0;
   Reco_z = 0;
   Reco_eta = 0;
   Reco_phi = 0;
   Reco_isPassed = 0;
   Reco_B_mass = 0;
   Reco_B_eta = 0;
   Reco_B_phi = 0;
   Reco_B_pT = 0;
   Reco_B_flavor = 0;
   Reco_Dstar_mass = 0;
   Reco_Dstar_eta = 0;
   Reco_Dstar_phi = 0;
   Reco_Dstar_pT = 0;
   Reco_Dstar_charge = 0;
   Reco_D0_mass = 0;
   Reco_D0_eta = 0;
   Reco_D0_phi = 0;
   Reco_D0_pT = 0;
   Reco_Mu_eta = 0;
   Reco_Mu_phi = 0;
   Reco_Mu_pT = 0;
   Reco_Mu_charge = 0;
   Reco_Pi_s_pT = 0;
   Reco_Pi_s_eta = 0;
   Reco_Pi_s_phi = 0;
   Reco_Pi_s_charge = 0;
   Reco_Pi_pT = 0;
   Reco_Pi_eta = 0;
   Reco_Pi_phi = 0;
   Reco_Pi_charge = 0;
   Reco_K_pT = 0;
   Reco_K_eta = 0;
   Reco_K_phi = 0;
   Reco_K_charge = 0;
   isMatch_Pass = 0;
   Min_d_Pass = 0;
   Min_dx_Pass = 0;
   Min_dy_Pass = 0;
   Min_dz_Pass = 0;
   Reco_x_Pass = 0;
   Reco_y_Pass = 0;
   Reco_z_Pass = 0;
   Reco_eta_Pass = 0;
   Reco_phi_Pass = 0;
   Reco_B_mass_Pass = 0;
   Reco_B_eta_Pass = 0;
   Reco_B_phi_Pass = 0;
   Reco_B_pT_Pass = 0;
   Reco_B_flavor_Pass = 0;
   Reco_Dstar_mass_Pass = 0;
   Reco_Dstar_eta_Pass = 0;
   Reco_Dstar_phi_Pass = 0;
   Reco_Dstar_pT_Pass = 0;
   Reco_Dstar_charge_Pass = 0;
   Reco_D0_mass_Pass = 0;
   Reco_D0_eta_Pass = 0;
   Reco_D0_phi_Pass = 0;
   Reco_D0_pT_Pass = 0;
   Reco_D0_charge_Pass = 0;
   Reco_Mu_eta_Pass = 0;
   Reco_Mu_phi_Pass = 0;
   Reco_Mu_pT_Pass = 0;
   Reco_Mu_charge_Pass = 0;
   Reco_Pi_s_pT_Pass = 0;
   Reco_Pi_s_eta_Pass = 0;
   Reco_Pi_s_phi_Pass = 0;
   Reco_Pi_s_charge_Pass = 0;
   Reco_Pi_pT_Pass = 0;
   Reco_Pi_eta_Pass = 0;
   Reco_Pi_phi_Pass = 0;
   Reco_Pi_charge_Pass = 0;
   Reco_K_pT_Pass = 0;
   Reco_K_eta_Pass = 0;
   Reco_K_phi_Pass = 0;
   Reco_K_charge_Pass = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("n_B", &n_B, &b_n_B);
   fChain->SetBranchAddress("n_B_Pass", &n_B_Pass, &b_n_B_Pass);
   fChain->SetBranchAddress("n_B_Match", &n_B_Match, &b_n_B_Match);
   fChain->SetBranchAddress("n_B_Match_Pass", &n_B_Match_Pass, &b_n_B_Match_Pass);
   fChain->SetBranchAddress("n_B_Truth", &n_B_Truth, &b_n_B_Truth);
   fChain->SetBranchAddress("n_Dstar", &n_Dstar, &b_n_Dstar);
   fChain->SetBranchAddress("n_Dstar_Pass", &n_Dstar_Pass, &b_n_Dstar_Pass);
   fChain->SetBranchAddress("n_Dstar_Match", &n_Dstar_Match, &b_n_Dstar_Match);
   fChain->SetBranchAddress("n_Dstar_Match_Pass", &n_Dstar_Match_Pass, &b_n_Dstar_Match_Pass);
   fChain->SetBranchAddress("n_Dstar_Truth", &n_Dstar_Truth, &b_n_Dstar_Truth);
   fChain->SetBranchAddress("n_Dstar_Truth_HighPt", &n_Dstar_Truth_HighPt, &b_n_Dstar_Truth_HighPt);
   fChain->SetBranchAddress("n_B_Dstar_Truth", &n_B_Dstar_Truth, &b_n_B_Dstar_Truth);
   fChain->SetBranchAddress("n_B_Dstar_D0Pi_Truth", &n_B_Dstar_D0Pi_Truth, &b_n_B_Dstar_D0Pi_Truth);
   fChain->SetBranchAddress("n_B_D_Truth", &n_B_D_Truth, &b_n_B_D_Truth);
   fChain->SetBranchAddress("n_B_D_s_Truth", &n_B_D_s_Truth, &b_n_B_D_s_Truth);
   fChain->SetBranchAddress("n_B_Lambda_c_Truth", &n_B_Lambda_c_Truth, &b_n_B_Lambda_c_Truth);
   fChain->SetBranchAddress("isHighPt", &isHighPt, &b_isHighPt);
   fChain->SetBranchAddress("Flavor", &Flavor, &b_Flavor);
   fChain->SetBranchAddress("DecayTime", &DecayTime, &b_DecayTime);
   fChain->SetBranchAddress("DeltaT", &DeltaT, &b_DeltaT);
   fChain->SetBranchAddress("hasMuon", &hasMuon, &b_hasMuon);
   fChain->SetBranchAddress("DecayType", &DecayType, &b_DecayType);
   fChain->SetBranchAddress("pdgID", &pdgID, &b_pdgID);
   fChain->SetBranchAddress("parent_num", &parent_num, &b_parent_num);
   fChain->SetBranchAddress("parent_pdgID", &parent_pdgID, &b_parent_pdgID);
   fChain->SetBranchAddress("track_num", &track_num, &b_track_num);
   fChain->SetBranchAddress("track_pdgID", &track_pdgID, &b_track_pdgID);
   fChain->SetBranchAddress("child_num", &child_num, &b_child_num);
   fChain->SetBranchAddress("child_pdgID", &child_pdgID, &b_child_pdgID);
   fChain->SetBranchAddress("Truth_x", &Truth_x, &b_Truth_x);
   fChain->SetBranchAddress("Truth_y", &Truth_y, &b_Truth_y);
   fChain->SetBranchAddress("Truth_z", &Truth_z, &b_Truth_z);
   fChain->SetBranchAddress("Truth_eta", &Truth_eta, &b_Truth_eta);
   fChain->SetBranchAddress("Truth_phi", &Truth_phi, &b_Truth_phi);
   fChain->SetBranchAddress("B_pT", &B_pT, &b_B_pT);
   fChain->SetBranchAddress("B_eta", &B_eta, &b_B_eta);
   fChain->SetBranchAddress("B_phi", &B_phi, &b_B_phi);
   fChain->SetBranchAddress("Dstar_pT", &Dstar_pT, &b_Dstar_pT);
   fChain->SetBranchAddress("Dstar_eta", &Dstar_eta, &b_Dstar_eta);
   fChain->SetBranchAddress("Dstar_phi", &Dstar_phi, &b_Dstar_phi);
   fChain->SetBranchAddress("Dstar_charge", &Dstar_charge, &b_Dstar_charge);
   fChain->SetBranchAddress("D0_mass", &D0_mass, &b_D0_mass);
   fChain->SetBranchAddress("D0_pT", &D0_pT, &b_D0_pT);
   fChain->SetBranchAddress("D0_eta", &D0_eta, &b_D0_eta);
   fChain->SetBranchAddress("D0_phi", &D0_phi, &b_D0_phi);
   fChain->SetBranchAddress("Mu_pT", &Mu_pT, &b_Mu_pT);
   fChain->SetBranchAddress("Mu_eta", &Mu_eta, &b_Mu_eta);
   fChain->SetBranchAddress("Mu_phi", &Mu_phi, &b_Mu_phi);
   fChain->SetBranchAddress("Mu_charge", &Mu_charge, &b_Mu_charge);
   fChain->SetBranchAddress("Pi_s_mass", &Pi_s_mass, &b_Pi_s_mass);
   fChain->SetBranchAddress("Pi_s_pT", &Pi_s_pT, &b_Pi_s_pT);
   fChain->SetBranchAddress("Pi_s_eta", &Pi_s_eta, &b_Pi_s_eta);
   fChain->SetBranchAddress("Pi_s_phi", &Pi_s_phi, &b_Pi_s_phi);
   fChain->SetBranchAddress("Pi_s_charge", &Pi_s_charge, &b_Pi_s_charge);
   fChain->SetBranchAddress("Pi_s_pdgID", &Pi_s_pdgID, &b_Pi_s_pdgID);
   fChain->SetBranchAddress("D0_child_num", &D0_child_num, &b_D0_child_num);
   fChain->SetBranchAddress("D0_charged_child_num", &D0_charged_child_num, &b_D0_charged_child_num);
   fChain->SetBranchAddress("D0_reco_mass", &D0_reco_mass, &b_D0_reco_mass);
   fChain->SetBranchAddress("D0_reco_pT", &D0_reco_pT, &b_D0_reco_pT);
   fChain->SetBranchAddress("D0_child_mass", &D0_child_mass, &b_D0_child_mass);
   fChain->SetBranchAddress("D0_child_pT", &D0_child_pT, &b_D0_child_pT);
   fChain->SetBranchAddress("D0_child_eta", &D0_child_eta, &b_D0_child_eta);
   fChain->SetBranchAddress("D0_child_phi", &D0_child_phi, &b_D0_child_phi);
   fChain->SetBranchAddress("D0_child_charge", &D0_child_charge, &b_D0_child_charge);
   fChain->SetBranchAddress("D0_child_pdgID", &D0_child_pdgID, &b_D0_child_pdgID);
   fChain->SetBranchAddress("isMatch", &isMatch, &b_isMatch);
   fChain->SetBranchAddress("Min_d", &Min_d, &b_Min_d);
   fChain->SetBranchAddress("Min_dx", &Min_dx, &b_Min_dx);
   fChain->SetBranchAddress("Min_dy", &Min_dy, &b_Min_dy);
   fChain->SetBranchAddress("Min_dz", &Min_dz, &b_Min_dz);
   fChain->SetBranchAddress("Reco_x", &Reco_x, &b_Reco_x);
   fChain->SetBranchAddress("Reco_y", &Reco_y, &b_Reco_y);
   fChain->SetBranchAddress("Reco_z", &Reco_z, &b_Reco_z);
   fChain->SetBranchAddress("Reco_eta", &Reco_eta, &b_Reco_eta);
   fChain->SetBranchAddress("Reco_phi", &Reco_phi, &b_Reco_phi);
   fChain->SetBranchAddress("Reco_isPassed", &Reco_isPassed, &b_Reco_isPassed);
   fChain->SetBranchAddress("Reco_B_mass", &Reco_B_mass, &b_Reco_B_mass);
   fChain->SetBranchAddress("Reco_B_eta", &Reco_B_eta, &b_Reco_B_eta);
   fChain->SetBranchAddress("Reco_B_phi", &Reco_B_phi, &b_Reco_B_phi);
   fChain->SetBranchAddress("Reco_B_pT", &Reco_B_pT, &b_Reco_B_pT);
   fChain->SetBranchAddress("Reco_B_flavor", &Reco_B_flavor, &b_Reco_B_flavor);
   fChain->SetBranchAddress("Reco_Dstar_mass", &Reco_Dstar_mass, &b_Reco_Dstar_mass);
   fChain->SetBranchAddress("Reco_Dstar_eta", &Reco_Dstar_eta, &b_Reco_Dstar_eta);
   fChain->SetBranchAddress("Reco_Dstar_phi", &Reco_Dstar_phi, &b_Reco_Dstar_phi);
   fChain->SetBranchAddress("Reco_Dstar_pT", &Reco_Dstar_pT, &b_Reco_Dstar_pT);
   fChain->SetBranchAddress("Reco_Dstar_charge", &Reco_Dstar_charge, &b_Reco_Dstar_charge);
   fChain->SetBranchAddress("Reco_D0_mass", &Reco_D0_mass, &b_Reco_D0_mass);
   fChain->SetBranchAddress("Reco_D0_eta", &Reco_D0_eta, &b_Reco_D0_eta);
   fChain->SetBranchAddress("Reco_D0_phi", &Reco_D0_phi, &b_Reco_D0_phi);
   fChain->SetBranchAddress("Reco_D0_pT", &Reco_D0_pT, &b_Reco_D0_pT);
   fChain->SetBranchAddress("Reco_Mu_eta", &Reco_Mu_eta, &b_Reco_Mu_eta);
   fChain->SetBranchAddress("Reco_Mu_phi", &Reco_Mu_phi, &b_Reco_Mu_phi);
   fChain->SetBranchAddress("Reco_Mu_pT", &Reco_Mu_pT, &b_Reco_Mu_pT);
   fChain->SetBranchAddress("Reco_Mu_charge", &Reco_Mu_charge, &b_Reco_Mu_charge);
   fChain->SetBranchAddress("Reco_Pi_s_pT", &Reco_Pi_s_pT, &b_Reco_Pi_s_pT);
   fChain->SetBranchAddress("Reco_Pi_s_eta", &Reco_Pi_s_eta, &b_Reco_Pi_s_eta);
   fChain->SetBranchAddress("Reco_Pi_s_phi", &Reco_Pi_s_phi, &b_Reco_Pi_s_phi);
   fChain->SetBranchAddress("Reco_Pi_s_charge", &Reco_Pi_s_charge, &b_Reco_Pi_s_charge);
   fChain->SetBranchAddress("Reco_Pi_pT", &Reco_Pi_pT, &b_Reco_Pi_pT);
   fChain->SetBranchAddress("Reco_Pi_eta", &Reco_Pi_eta, &b_Reco_Pi_eta);
   fChain->SetBranchAddress("Reco_Pi_phi", &Reco_Pi_phi, &b_Reco_Pi_phi);
   fChain->SetBranchAddress("Reco_Pi_charge", &Reco_Pi_charge, &b_Reco_Pi_charge);
   fChain->SetBranchAddress("Reco_K_pT", &Reco_K_pT, &b_Reco_K_pT);
   fChain->SetBranchAddress("Reco_K_eta", &Reco_K_eta, &b_Reco_K_eta);
   fChain->SetBranchAddress("Reco_K_phi", &Reco_K_phi, &b_Reco_K_phi);
   fChain->SetBranchAddress("Reco_K_charge", &Reco_K_charge, &b_Reco_K_charge);
   fChain->SetBranchAddress("isMatch_Pass", &isMatch_Pass, &b_isMatch_Pass);
   fChain->SetBranchAddress("Min_d_Pass", &Min_d_Pass, &b_Min_d_Pass);
   fChain->SetBranchAddress("Min_dx_Pass", &Min_dx_Pass, &b_Min_dx_Pass);
   fChain->SetBranchAddress("Min_dy_Pass", &Min_dy_Pass, &b_Min_dy_Pass);
   fChain->SetBranchAddress("Min_dz_Pass", &Min_dz_Pass, &b_Min_dz_Pass);
   fChain->SetBranchAddress("Reco_x_Pass", &Reco_x_Pass, &b_Reco_x_Pass);
   fChain->SetBranchAddress("Reco_y_Pass", &Reco_y_Pass, &b_Reco_y_Pass);
   fChain->SetBranchAddress("Reco_z_Pass", &Reco_z_Pass, &b_Reco_z_Pass);
   fChain->SetBranchAddress("Reco_eta_Pass", &Reco_eta_Pass, &b_Reco_eta_Pass);
   fChain->SetBranchAddress("Reco_phi_Pass", &Reco_phi_Pass, &b_Reco_phi_Pass);
   fChain->SetBranchAddress("Reco_B_mass_Pass", &Reco_B_mass_Pass, &b_Reco_B_mass_Pass);
   fChain->SetBranchAddress("Reco_B_eta_Pass", &Reco_B_eta_Pass, &b_Reco_B_eta_Pass);
   fChain->SetBranchAddress("Reco_B_phi_Pass", &Reco_B_phi_Pass, &b_Reco_B_phi_Pass);
   fChain->SetBranchAddress("Reco_B_pT_Pass", &Reco_B_pT_Pass, &b_Reco_B_pT_Pass);
   fChain->SetBranchAddress("Reco_B_flavor_Pass", &Reco_B_flavor_Pass, &b_Reco_B_flavor_Pass);
   fChain->SetBranchAddress("Reco_Dstar_mass_Pass", &Reco_Dstar_mass_Pass, &b_Reco_Dstar_mass_Pass);
   fChain->SetBranchAddress("Reco_Dstar_eta_Pass", &Reco_Dstar_eta_Pass, &b_Reco_Dstar_eta_Pass);
   fChain->SetBranchAddress("Reco_Dstar_phi_Pass", &Reco_Dstar_phi_Pass, &b_Reco_Dstar_phi_Pass);
   fChain->SetBranchAddress("Reco_Dstar_pT_Pass", &Reco_Dstar_pT_Pass, &b_Reco_Dstar_pT_Pass);
   fChain->SetBranchAddress("Reco_Dstar_charge_Pass", &Reco_Dstar_charge_Pass, &b_Reco_Dstar_charge_Pass);
   fChain->SetBranchAddress("Reco_D0_mass_Pass", &Reco_D0_mass_Pass, &b_Reco_D0_mass_Pass);
   fChain->SetBranchAddress("Reco_D0_eta_Pass", &Reco_D0_eta_Pass, &b_Reco_D0_eta_Pass);
   fChain->SetBranchAddress("Reco_D0_phi_Pass", &Reco_D0_phi_Pass, &b_Reco_D0_phi_Pass);
   fChain->SetBranchAddress("Reco_D0_pT_Pass", &Reco_D0_pT_Pass, &b_Reco_D0_pT_Pass);
   fChain->SetBranchAddress("Reco_D0_charge_Pass", &Reco_D0_charge_Pass, &b_Reco_D0_charge_Pass);
   fChain->SetBranchAddress("Reco_Mu_eta_Pass", &Reco_Mu_eta_Pass, &b_Reco_Mu_eta_Pass);
   fChain->SetBranchAddress("Reco_Mu_phi_Pass", &Reco_Mu_phi_Pass, &b_Reco_Mu_phi_Pass);
   fChain->SetBranchAddress("Reco_Mu_pT_Pass", &Reco_Mu_pT_Pass, &b_Reco_Mu_pT_Pass);
   fChain->SetBranchAddress("Reco_Mu_charge_Pass", &Reco_Mu_charge_Pass, &b_Reco_Mu_charge_Pass);
   fChain->SetBranchAddress("Reco_Pi_s_pT_Pass", &Reco_Pi_s_pT_Pass, &b_Reco_Pi_s_pT_Pass);
   fChain->SetBranchAddress("Reco_Pi_s_eta_Pass", &Reco_Pi_s_eta_Pass, &b_Reco_Pi_s_eta_Pass);
   fChain->SetBranchAddress("Reco_Pi_s_phi_Pass", &Reco_Pi_s_phi_Pass, &b_Reco_Pi_s_phi_Pass);
   fChain->SetBranchAddress("Reco_Pi_s_charge_Pass", &Reco_Pi_s_charge_Pass, &b_Reco_Pi_s_charge_Pass);
   fChain->SetBranchAddress("Reco_Pi_pT_Pass", &Reco_Pi_pT_Pass, &b_Reco_Pi_pT_Pass);
   fChain->SetBranchAddress("Reco_Pi_eta_Pass", &Reco_Pi_eta_Pass, &b_Reco_Pi_eta_Pass);
   fChain->SetBranchAddress("Reco_Pi_phi_Pass", &Reco_Pi_phi_Pass, &b_Reco_Pi_phi_Pass);
   fChain->SetBranchAddress("Reco_Pi_charge_Pass", &Reco_Pi_charge_Pass, &b_Reco_Pi_charge_Pass);
   fChain->SetBranchAddress("Reco_K_pT_Pass", &Reco_K_pT_Pass, &b_Reco_K_pT_Pass);
   fChain->SetBranchAddress("Reco_K_eta_Pass", &Reco_K_eta_Pass, &b_Reco_K_eta_Pass);
   fChain->SetBranchAddress("Reco_K_phi_Pass", &Reco_K_phi_Pass, &b_Reco_K_phi_Pass);
   fChain->SetBranchAddress("Reco_K_charge_Pass", &Reco_K_charge_Pass, &b_Reco_K_charge_Pass);
   Notify();
}

Bool_t TruthBAnalize::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TruthBAnalize::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TruthBAnalize::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TruthBAnalize_cxx
