#define TruthBAnalize_cxx
#include "TruthBAnalize.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void TruthBAnalize::Loop(const int& isAdd, const string& outname, const float& width1)
{
//   In a ROOT session, you can do:
//      root> .L TruthBAnalize.C
//      root> TruthBAnalize t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   TH1::SetDefaultSumw2();

   Float_t Time_bin_end = 15.*width1+0.25;
   Float_t Significance_bin_start = 0.25;
   Float_t Significance_bin_end = Significance_bin_start + width1*7.;

   // Correlation in MC sample
   h_Distance=new TH1F("h_Distance", "", 40, -10, 10);
   h_Distance->SetXTitle("s^{2}");
   h_Distance->SetYTitle("Entries / 0.5");

   h_B_Distance=new TH1F("h_B_Distance", "", 100, 0, 5);
   h_B_Distance->SetXTitle("dR [mm]");
   h_B_Distance->SetYTitle("Entries / 0.1 mm");

   for(int i=0;i<7;i++){
     std::stringstream ss1;
     ss1<<"h_n_Truth"<<std::setw(1)<<i;
     h_n_Truth[i]=new TH1F(ss1.str().c_str(), "", 4, 1, 5);
     h_n_Truth[i]->SetXTitle("#(B^{0})");
     h_n_Truth[i]->SetYTitle("Events");
   }

   h_B_Flavor=new TH2F("h_B_Flavor", "", 4, -2, 2, 4, -2, 2);
   h_B_Flavor->SetXTitle("B Flavor1");
   h_B_Flavor->SetYTitle("B Flavor2");

   h_B_Flavor_E=new TH2F("h_B_Flavor_E", "", 4, -2, 2, 4, -2, 2);
   h_B_Flavor_E->SetXTitle("B Flavor1");
   h_B_Flavor_E->SetYTitle("B Flavor2");

   for(int i=0;i<15;i++){
     std::stringstream ss1;
     ss1<<"h_B_Flavor_wrt_Time"<<std::setw(2)<<std::setfill('0')<<i;
     h_B_Flavor_wrt_Time[i]=new TH2F(ss1.str().c_str(), "", 4, -2, 2, 4, -2, 2);
     h_B_Flavor_wrt_Time[i]->SetXTitle("B Flavor1");
     h_B_Flavor_wrt_Time[i]->SetYTitle("B Flavor2");

     std::stringstream ss2;
     ss2<<"h_B_Flavor_E_wrt_Time"<<std::setw(2)<<std::setfill('0')<<i;
     h_B_Flavor_E_wrt_Time[i]=new TH2F(ss2.str().c_str(), "", 4, -2, 2, 4, -2, 2);
     h_B_Flavor_E_wrt_Time[i]->SetXTitle("B Flavor1");
     h_B_Flavor_E_wrt_Time[i]->SetYTitle("B Flavor2");
   }

   h_Delta_Time_SF=new TH1F("h_Delta_Time_SF", "", 15, 0.25, Time_bin_end);
   h_Delta_Time_SF->SetXTitle("#Deltat [ps]");
   h_Delta_Time_SF->SetYTitle("Entries / 0.5 ps");

   h_Delta_Time_OF=new TH1F("h_Delta_Time_OF", "", 15, 0.25, Time_bin_end);
   h_Delta_Time_OF->SetXTitle("#Deltat [ps]");
   h_Delta_Time_OF->SetYTitle("Entries / 0.5 ps");

   h_Correlation=new TH1F("h_Correlation", "", 15, 0.25, Time_bin_end);
   h_Correlation->SetXTitle("#Deltat [ps]");
   h_Correlation->SetYTitle("Correlation / 0.5 ps");

   h_Significance=new TH1F("h_Significance", "", 7, Significance_bin_start, Significance_bin_end);
   h_Significance->SetXTitle("#Deltat [ps]");
   h_Significance->SetYTitle("|S| / 0.5 ps");

   h_NoDelta_Time_SF=new TH1F("h_NoDelta_Time_SF", "", 15, 0.25, Time_bin_end);
   h_NoDelta_Time_SF->SetXTitle("#Deltat [ps]");
   h_NoDelta_Time_SF->SetYTitle("Entries / 0.5 ps");

   h_NoDelta_Time_OF=new TH1F("h_NoDelta_Time_OF", "", 15, 0.25, Time_bin_end);
   h_NoDelta_Time_OF->SetXTitle("#Deltat [ps]");
   h_NoDelta_Time_OF->SetYTitle("Entries / 0.5 ps");

   h_NoCorrelation=new TH1F("h_NoCorrelation", "", 15, 0.25, Time_bin_end);
   h_NoCorrelation->SetXTitle("#Deltat [ps]");
   h_NoCorrelation->SetYTitle("NoCorrelation / 0.5 ps");

   h_NoSignificance=new TH1F("h_NoSignificance", "", 7, Significance_bin_start, Significance_bin_end);
   h_NoSignificance->SetXTitle("#Deltat [ps]");
   h_NoSignificance->SetYTitle("|S| / 0.5 ps");

   std::cout << "RAND_MAX = " << RAND_MAX << "\n";
   srand((unsigned int)time(NULL));

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
     if(jentry%100000==0){
       std::cout<<"JOBS....."<<(double)jentry/nentries*100<<"%....COMPLETE"<<std::endl;
     }
     Long64_t ientry = LoadTree(jentry);
     if (ientry < 0) break;
     nb = fChain->GetEntry(jentry);   nbytes += nb;
     // if (Cut(ientry) < 0) continue;

     h_n_Truth[0]->Fill(n_B_Dstar_Truth);
     h_n_Truth[1]->Fill(n_B_Dstar_D0Pi_Truth);
     h_n_Truth[2]->Fill(n_Dstar1_Truth+n_Dstar2_Truth);
     h_n_Truth[3]->Fill(n_Dstar2_Truth);
     h_n_Truth[4]->Fill(n_Dstar1_Truth_HighPt+n_Dstar2_Truth_HighPt);
     h_n_Truth[5]->Fill(n_Dstar1_Match+n_Dstar2_Match);
     h_n_Truth[6]->Fill(n_Dstar1_Match_Pass+n_Dstar2_Match_Pass);

     for(int n=0; n < n_Dstar1_Truth+n_Dstar2_Truth; n++){ // First B meson
       h_B_Distance->Fill(Min_d->at(n));
       for(int k=n+1; k < n_Dstar1_Truth+n_Dstar2_Truth; k++){ // Second B meson

	 //Bool_t Pass = (DecayType->at(n)==1&&DecayType->at(k)==1&&isHighPt->at(k)&&isHighPt->at(n));
	 Bool_t Pass = (isHighPt->at(k)&&isHighPt->at(n));
	 Pass = Pass&&(isMatch->at(k)&&isMatch->at(n));
	 //if(!(Pass)) continue; // Selection
	 
	 Float_t FirstTime = std::min(DecayTime->at(n), DecayTime->at(k));
	 Float_t SecondTime = std::max(DecayTime->at(n), DecayTime->at(k));
	 Float_t time_difference = fabs( SecondTime - FirstTime );
	  
	 Float_t dx2 = pow(Truth_x->at(n) - Truth_x->at(k), 2) + pow(Truth_y->at(n) - Truth_y->at(k), 2) + pow(Truth_z->at(n) - Truth_z->at(k), 2); // mm^2
	 Float_t dt2 = pow(time_difference, 2); //ps^2
	 Float_t distance = dx2 - 0.09 * dt2;

	 // Simulate the entanglement of B0 meson
	 Double_t Rate_SF = (1-cos(0.5065*time_difference))/2.;
	 Double_t Error_Rate_SF = sin(0.5065*time_difference)/2.*hypot(time_difference*0.0019, 0.5065*0.11);
	 Double_t Uniform = ((double)rand()+1.0)/((double)RAND_MAX+2.0);
	 Double_t Estimated_Rate_SF = Rate_SF + Error_Rate_SF*sqrt( -2.0*log(Uniform) ) * sin( 2.0*M_PI*Uniform );
	 //Bool_t isSameFlavor = ((Double_t)rand()/RAND_MAX < Estimated_Rate_SF);
	 Bool_t isSameFlavor = ((Double_t)rand()/RAND_MAX < Rate_SF);

	 Bool_t No_isSameFlavor = ( Flavor->at(n) == Flavor->at(k) );

	 Int_t bin_T = (Int_t)(FirstTime/width1);
	 Int_t bin_dT = (Int_t)((time_difference-0.25)/width1);

	 h_Distance->Fill(distance);

	 h_B_Flavor->Fill(Flavor->at(n), Flavor->at(k));
	 if(bin_dT>=0&&bin_dT<15) h_B_Flavor_wrt_Time[bin_dT]->Fill(Flavor->at(n), Flavor->at(k));

	 if(isSameFlavor) h_Delta_Time_SF->Fill(time_difference);
	 else h_Delta_Time_OF->Fill(time_difference);

	 if(No_isSameFlavor) h_NoDelta_Time_SF->Fill(time_difference);
	 else h_NoDelta_Time_OF->Fill(time_difference);

	 if(DecayTime->at(n) < DecayTime->at(k)){ // Time Order (First < Second)

	   if(isSameFlavor) { // Same flavor
	     h_B_Flavor_E->Fill(Flavor->at(n), Flavor->at(n));
	     if(bin_dT>=0&&bin_dT<15) h_B_Flavor_E_wrt_Time[bin_dT]->Fill(Flavor->at(n), Flavor->at(n));
	   } else { // Wrong flavor
	     h_B_Flavor_E->Fill(Flavor->at(n), -1*Flavor->at(n));
	     if(bin_dT>=0&&bin_dT<15) h_B_Flavor_E_wrt_Time[bin_dT]->Fill(Flavor->at(n), -1*Flavor->at(n));
	   }

	 } else { // Time Order (First >= Second)

	   if(isSameFlavor) { // Same flavor
	     h_B_Flavor_E->Fill(Flavor->at(k), Flavor->at(k));
	     if(bin_dT>=0&&bin_dT<15) h_B_Flavor_E_wrt_Time[bin_dT]->Fill(Flavor->at(k), Flavor->at(k));
	   } else { // Wrong flavor
	     h_B_Flavor_E->Fill(Flavor->at(k), -1*Flavor->at(k));
	     if(bin_dT>=0&&bin_dT<15) h_B_Flavor_E_wrt_Time[bin_dT]->Fill(Flavor->at(k), -1*Flavor->at(k));
	   }
	 } // End (Time Order)
       } // End (Second)
     } // End (First)

   } // End (Event Loop)

   Float_t ScaleFactor;
   /*
   if (isAdd==1) ScaleFactor = 0.7250;
   else if (isAdd==2) ScaleFactor = 1;
   else if (isAdd==-1) ScaleFactor = 43.6;
   //else ScaleFactor = 2.67;
   else ScaleFactor = 0.7250;
   */
   //ScaleFactor = 0.7250;
   ScaleFactor = 1;

   h_Distance->Scale(ScaleFactor);
   h_B_Distance->Scale(ScaleFactor);
   for(int i=0;i<7;i++)
     h_n_Truth[i]->Scale(ScaleFactor);
   h_B_Flavor->Scale(ScaleFactor);
   h_B_Flavor_E->Scale(ScaleFactor);
   for(int i=0;i<15;i++){
   h_B_Flavor_wrt_Time[i]->Scale(ScaleFactor);
   h_B_Flavor_E_wrt_Time[i]->Scale(ScaleFactor);
   }
   h_Delta_Time_SF->Scale(ScaleFactor);
   h_Delta_Time_OF->Scale(ScaleFactor);
   h_NoDelta_Time_SF->Scale(ScaleFactor);
   h_NoDelta_Time_OF->Scale(ScaleFactor);


   Float_t Number_SF, error_SF;
   Float_t Number_OF, error_OF;
   Float_t correlation, error_cor;

   for(int i=1;i<=15;i++){
     Number_SF = h_Delta_Time_SF->GetBinContent(i);
     if(Number_SF>=ScaleFactor)
       error_SF = h_Delta_Time_SF->GetBinError(i);
     else
       error_SF = ScaleFactor;
     Number_OF = h_Delta_Time_OF->GetBinContent(i);
     if(Number_OF>=ScaleFactor)
       error_OF = h_Delta_Time_OF->GetBinError(i);
     else
       error_OF = ScaleFactor;

     if(Number_SF+Number_OF>=ScaleFactor){
       correlation=(Number_SF-Number_OF)/(Number_SF+Number_OF);
       error_cor=sqrt(pow(2*Number_OF/pow(Number_SF+Number_OF,2)*error_SF, 2)+pow(2*Number_SF/pow(Number_SF+Number_OF,2)*error_OF, 2));
       h_Correlation->SetBinContent(i, correlation);
       h_Correlation->SetBinError(i, error_cor);
     }
   }
   
   Float_t significance = 0, error_sig = 0;
   Float_t value[2];
   Float_t error[2];
   Bool_t invalid_flag=false;
   
   for(int i=1;i<=5;i++){
     std::cout << "Statistics Bin " << i << " : ";
     value[0] = h_Correlation->GetBinContent(i);
     error[0] = h_Correlation->GetBinError(i);
     value[1] = h_Correlation->GetBinContent(3*i);
     error[1] = h_Correlation->GetBinError(3*i);

     if(error[0]<=0||error[1]<=0){
       std::cout << "\n";
       continue;
     }

     significance=fabs(3*value[0]-value[1]);
     h_Significance->SetBinContent(i, significance);
     error_sig=sqrt(pow(3*error[0], 2)+pow(error[1], 2));
     h_Significance->SetBinError(i, error_sig);

     std::cout << value[0] << " + " << error[0] << "(val0), " << value[1] << " + " << error[1] << "(val1); ";
     std::cout << significance << " + " << error_sig << "(significance)\n";
   }

   h_Significance->GetYaxis()->SetRangeUser(-1, 4.);


   //Histogram of No Correlation situation
   Float_t No_Number_SF, No_error_SF;
   Float_t No_Number_OF, No_error_OF;
   Float_t No_correlation, No_error_cor;

   for(int i=1;i<=15;i++){
     No_Number_SF = h_NoDelta_Time_SF->GetBinContent(i);
     if(No_Number_SF>=ScaleFactor)
       No_error_SF = h_NoDelta_Time_SF->GetBinError(i);
     else
       No_error_SF = ScaleFactor;
     No_Number_OF = h_NoDelta_Time_OF->GetBinContent(i);
     if(No_Number_OF>=ScaleFactor)
       No_error_OF = h_NoDelta_Time_OF->GetBinError(i);
     else
       No_error_OF = ScaleFactor;

     if(No_Number_SF+No_Number_OF>=ScaleFactor){
       No_correlation=(No_Number_SF - No_Number_OF)/(No_Number_SF + No_Number_OF);
       No_error_cor=sqrt( pow( 2*No_Number_OF/pow(No_Number_SF + No_Number_OF,2)*No_error_SF, 2)+pow( 2*No_Number_SF/pow(No_Number_SF + No_Number_OF,2)*No_error_OF, 2) );
       h_NoCorrelation->SetBinContent(i, No_correlation);
       h_NoCorrelation->SetBinError(i, No_error_cor);
     }
   }
   
   Float_t No_significance = 0, No_error_sig = 0;
   Float_t No_value[2];
   Float_t No_error[2];
   Bool_t No_invalid_flag=false;
   
   for(int i=1;i<=5;i++){
     std::cout << "Statistics Bin " << i << " (No Correlation situation) : ";
     No_value[0] = h_NoCorrelation->GetBinContent(i);
     No_error[0] = h_NoCorrelation->GetBinError(i);
     No_value[1] = h_NoCorrelation->GetBinContent(3*i);
     No_error[1] = h_NoCorrelation->GetBinError(3*i);

     if(No_error[0]<=0||No_error[1]<=0){
       std::cout << "\n";
       continue;
     }

     No_significance=fabs(3*No_value[0]-No_value[1]);
     h_NoSignificance->SetBinContent(i, No_significance);
     No_error_sig=sqrt(pow(3*No_error[0], 2)+pow(No_error[1], 2));
     h_NoSignificance->SetBinError(i, No_error_sig);

     std::cout << No_value[0] << " + " << No_error[0] << "(val0), " << No_value[1] << " + " << No_error[1] << "(val1); ";
     std::cout << No_significance << " + " << No_error_sig << "(significance)\n";
   }

   h_NoSignificance->GetYaxis()->SetRangeUser(-1, 4.);

   // Write histograms to root file
   TFile *file = new TFile(outname.c_str(), "RECREATE");

   // Correlation in MC sample
   h_Distance->Write();
   h_B_Distance->Write();
   for(int i=0;i<7;i++)
     h_n_Truth[i]->Write();
   h_B_Flavor->Write();
   h_B_Flavor_E->Write();
   for(int i=0;i<15;i++){
     h_B_Flavor_wrt_Time[i]->Write();
     h_B_Flavor_E_wrt_Time[i]->Write();
   }
   h_Delta_Time_SF->Write();
   h_Delta_Time_OF->Write();
   h_Correlation->Write();
   h_Significance->Write();
   h_NoDelta_Time_SF->Write();
   h_NoDelta_Time_OF->Write();
   h_NoCorrelation->Write();
   h_NoSignificance->Write();

   file->Close();

}
