# Setup variable
isMain=0
isTruth=1
CutFlow=0
Kine=1

isMC=1
isAdd=1

Make_histo=1
Make_plots=1

Mass_bin=0.5
Time_bin=1

cd /home/ytsujika/MakePlots/build/
source $AtlasSetup/scripts/asetup.sh
cd /home/ytsujika/MakePlots/script/

if [ "$isMain" == 1 ]; then
    # MC
    if [ "$isMC" == 1 ]; then
	if [ "$isAdd" == 1 ]; then
	    ifile_name=NonSkim_MC
	else
	    ifile_name=Main_MC
	    #ifile_name=New_MC
	fi
    # Data
    else
	if [ "$isAdd" == -1 ]; then
	    ifile_name=Main_PPref
	else
	    ifile_name=Main_Data
	    #ifile_name=New_Data
	fi
    fi

# Truth B
elif [ "$isTruth" == 1 ]; then
    if [ "$isAdd" == 1 ]; then
	ifile_name=NonSkim_MC
	ofile_name=NonSkim_Truth
    else
	ifile_name=Main_MC
	#ifile_name=New_MC
	#ifile_name=Validation_MC
	ofile_name=Truth
    fi

#Kinenematics
elif [ "$Kine" == 1 ]; then
    # MC
    if [ "$isMC" == 1 ]; then
	if [ "$isAdd" == 1 ]; then
	    ifile_name=NonSkim_MC
	    ofile_name=Kine_NonSkim
	else
	    #ifile_name=Main_MC
	    #ifile_name=New_MC
	    ifile_name=Validation_MC
	    ofile_name=Kine_MC
	fi
    # Data
    else
	if [ "$isAdd" == -1 ]; then
	    ifile_name=Main_PPref
	    ofile_name=Kine_PPref
	else
	    ifile_name=Main_Data
	    #ifile_name=New_Data
	    #ifile_name=Validation_Data
	    ofile_name=Kine_Data
	fi
    fi
fi


#Main analysis
if [ "$isMain" == 1 ]; then
    # Make histogram
    if [ "$Make_histo" == 1 ]; then
	root -l -b -q '/home/ytsujika/MakePlots/source/Main/MakeHist_B.C('$isMC', '$isAdd', "/home/ytsujika/AnalysisBphys/run/Rootfile/'$ifile_name'.root", "/home/ytsujika/MakePlots/outputs/hist/'$ifile_name'.root", '$Mass_bin', '$Time_bin')';
    fi
	    
    # Make plots
    if [ "$Make_plots" == 1 ]; then
	root -l -b -q '/home/ytsujika/MakePlots/source/Main/MakePlots_B.C('$isMC', "/home/ytsujika/MakePlots/outputs/hist/'$ifile_name'.root", "/home/ytsujika/MakePlots/outputs/plot/'$ifile_name'.root", "../outputs/file/'$ifile_name'.pdf")';
    fi

# Truth B
elif [ "$isTruth" == 1 ]; then
    # Make histogram
    if [ "$Make_histo" == 1 ]; then
	root -l -b -q '/home/ytsujika/MakePlots/source/Truth/MakeHist_B.C('$isAdd', "/home/ytsujika/AnalysisBphys/run/Rootfile/'$ifile_name'.root", "/home/ytsujika/MakePlots/outputs/hist/'$ofile_name'.root", '$Time_bin')';
    fi
	    
    # Make plots
    if [ "$Make_plots" == 1 ]; then
	root -l -b -q '/home/ytsujika/MakePlots/source/Truth/MakePlots_B.C("/home/ytsujika/MakePlots/outputs/hist/'$ofile_name'.root", "/home/ytsujika/MakePlots/outputs/plot/'$ofile_name'.root", "../outputs/file/'$ofile_name'.pdf")';
    fi

# Kinematics
elif [ "$Kine" == 1 ]; then
    # Make histogram
    if [ "$Make_histo" == 1 ]; then
	root -l -b -q '/home/ytsujika/MakePlots/source/Kine/MakeHist_B.C('$isMC', '$isAdd', "/home/ytsujika/AnalysisBphys/run/Rootfile/'$ifile_name'.root", "/home/ytsujika/MakePlots/outputs/hist/'$ofile_name'.root")';
    fi
	    
    # Make plots
    if [ "$Make_plots" == 1 ]; then
	root -l -b -q '/home/ytsujika/MakePlots/source/Kine/MakePlots_B.C('$isMC', "/home/ytsujika/MakePlots/outputs/hist/'$ofile_name'.root", "/home/ytsujika/MakePlots/outputs/plot/'$ofile_name'.root", "../outputs/file/'$ofile_name'.pdf")';
    fi

fi
